
from pathlib import Path
from typing import List, Dict, Tuple
import os
from Program.Code.utils import get_problem_types, get_problem_names, get_model_types, get_tuple_problem_names
import os


# ---------- JOBS ----------------
def load_job_info_single_mode() -> str:
    return '#!/bin/bash\n' +\
'#SBATCH --nodes=1\n' +\
'#SBATCH --ntasks-per-node=1\n' +\
'#SBATCH --mem=16G\n' +\
'#SBATCH --partition=cpu\n' +\
'#SBATCH --error=gnn.err\n' +\
'#SBATCH --out=gnn.out\n' +\
'#SBATCH --time=6:00:00\n' +\
'#SBATCH --job-name=HESOYAM\n' +\
'\n' +\
'ml PyTorch/1.13.0-foss-2022a-CUDA-11.7.0\n' +\
'pip install --user pytorch-lightning\n' +\
'pip install --user -U rich\n' +\
'pip install --user frozendict\n'


def load_job_command_single_mode(pddl_domain_file_path: Path, pddl_domain_instance_file_path: Path, output_folder_path: Path, fastdownward_pruning: bool) -> str:
    return 'srun python Program/main_terminal_gnn_data_generator.py' +\
    ' --mode single' +\
    ' --fastdownward_folder_path FastDownwardPlannerLinux' +\
    f' --pddl_domain_file_path {pddl_domain_file_path}' +\
    f' --pddl_domain_instance_file_path {pddl_domain_instance_file_path}' +\
    f' --output_folder_path {output_folder_path}' +\
    f' --fastdownward_pruning {str(fastdownward_pruning)}' +\
    ' --timer 240'


def load_job_single_mode(pddl_domain_file_path: Path, pddl_domain_instance_file_path: Path, output_folder_path: Path, fastdownward_pruning: bool) -> str:
    return load_job_info_single_mode() + load_job_command_single_mode(pddl_domain_file_path, pddl_domain_instance_file_path, output_folder_path, fastdownward_pruning)


def write_job(job_string: str, job_output_folder_path: Path, job_output_file_name: str) -> None:
    job_output_file_name = job_output_file_name + '.sh'

    path = os.path.abspath(os.path.join(job_output_folder_path, job_output_file_name))
    os.makedirs(os.path.dirname(path), exist_ok=True)

    with open(path, 'w') as f:
        f.write(job_string)


def create_jobs_single_mode_for(pddl_folder_path: Path, job_output_folder_path: Path, output_folder_path: Path, fastdownward_pruning: bool) -> None:
    pddl_domain_file_path = os.path.join(pddl_folder_path, 'domain.pddl')

    assert os.path.exists(pddl_domain_file_path), 'Pddl folder must contain domain named "domain.pddl".'

    for pddl_domain_instance_file_path in pddl_folder_path.glob('*.pddl'):
        if str(pddl_domain_instance_file_path) == str(pddl_domain_file_path):
            continue
        job_string = load_job_single_mode(pddl_domain_file_path, pddl_domain_instance_file_path, output_folder_path, fastdownward_pruning)
        job_output_file_name = 'job_' + str(os.path.basename(os.path.normpath(pddl_domain_instance_file_path))).replace('.pddl', '')
        write_job(job_string, job_output_folder_path, job_output_file_name)
            

def create_jobs_single_mode(pddl_folders_path: Path, problem_names: List[str], problem_types: List[str], fastdownward_pruning: bool):
    for problem_name in problem_names:
        for problem_type in problem_types:
            pddl_folder_path = Path(os.path.join(pddl_folders_path, problem_name, problem_type))
            job_output_folder_path = Path(os.path.join('Jobs', 'Single', problem_name, problem_type))
            output_folder_path = Path(os.path.join('Output', problem_name, problem_type))

            create_jobs_single_mode_for(pddl_folder_path, job_output_folder_path, output_folder_path, fastdownward_pruning)


def run_jobs_single_mode(jobs_folder_path: Path, problem_names: List[str], problem_types: List[str]):
    for problem_name in problem_names:
        for problem_type in problem_types:
            path = Path(os.path.join(jobs_folder_path, problem_name, problem_type))
            for job in path.glob('*.sh'):
                os.system('sbatch' + ' ' + str(job))


def create_and_run_jobs_single_mode_all():
    create_jobs_single_mode(Path('Data/Pddl'), get_problem_names(), get_problem_types(), fastdownward_pruning=False)
    run_jobs_single_mode(Path(os.path.join('Jobs', 'Single')), get_problem_names(), get_problem_types())


















def load_job_info_train_mode() -> str:
    return '#!/bin/bash\n' +\
'#SBATCH --nodes=1\n' +\
'#SBATCH --ntasks-per-node=1\n' +\
'#SBATCH --cpus-per-task=8\n' +\
'#SBATCH --mem=32G\n' +\
'#SBATCH --gres=gpu:1\n' +\
'#SBATCH --partition=amdgpu\n' +\
'#SBATCH --error=gnn.err\n' +\
'#SBATCH --out=gnn.out\n' +\
'#SBATCH --time=24:00:00\n' +\
'#SBATCH --job-name=HESOYAM\n' +\
'\n' +\
'ml PyTorch/1.13.0-foss-2022a-CUDA-11.7.0\n' +\
'pip install --user pytorch-lightning\n' +\
'pip install --user -U rich\n' +\
'pip install --user frozendict\n'


def load_job_command_train_mode(model_type: str, problem_name: str, train_dataset_folder_path: Path, validation_dataset_folder_path: Path) -> str:
    return 'srun python Program/main_terminal_gnn.py' +\
    ' --mode train' +\
    f' --model_type {model_type}' +\
    f' --problem_name {problem_name}' +\
    f' --train_dataset_folder_path {train_dataset_folder_path}' +\
    f' --validation_dataset_folder_path {validation_dataset_folder_path}' +\
    ' --accelerator gpu' +\
    ' --devices 1' +\
    ' --patience 1000' +\
    ' --loader_num_workers 8' + (' --l1_factor 0' if model_type == 'max' else '')
    

def load_job_train_mode(model_type: str, problem_name: str, train_dataset_folder_path: Path, validation_dataset_folder_path: Path) -> str:
    return load_job_info_train_mode() + load_job_command_train_mode(model_type, problem_name, train_dataset_folder_path, validation_dataset_folder_path)


def create_jobs_train_mode_for(model_type: str, problem_name: str, json_datasets_folder_path: Path, job_name: str,job_output_folder_path: Path):
    job_string = load_job_train_mode(model_type, problem_name, Path(os.path.join(json_datasets_folder_path, problem_name, 'Train')), Path(os.path.join(json_datasets_folder_path, problem_name, 'Validation')))
    job_output_file_name = 'job_' + job_name + '_' + model_type
    write_job(job_string, job_output_folder_path, job_output_file_name)


def create_jobs_train_mode(tuple_problem_names: List[Tuple[str, str]], model_types: List[str], json_datasets_folder_path: Path):
    for problem_name, problem_name_snake in tuple_problem_names:
        for model_type in model_types:
            job_output_folder_path = Path(os.path.join('Jobs', 'Train', problem_name, model_type.capitalize()))

            create_jobs_train_mode_for(model_type, problem_name, json_datasets_folder_path, problem_name_snake, job_output_folder_path)


def run_jobs_train_mode(jobs_folder_path: Path, problem_names: List[str], model_types: List[str]):
    for problem_name in problem_names:
        for model_type in model_types:
            path = Path(os.path.join(jobs_folder_path, problem_name, model_type.capitalize()))
            for job in path.glob('*.sh'):
                os.system('sbatch' + ' ' + str(job))


def create_and_run_jobs_train_mode_all():
    create_jobs_train_mode(get_tuple_problem_names(), get_model_types(), Path(os.path.join('Data', 'JsonFull')))
    run_jobs_train_mode(Path(os.path.join('Jobs', 'Train')), get_problem_names(), get_model_types())




















def load_job_info_results() -> str:
    return '#!/bin/bash\n' +\
'#SBATCH --nodes=1\n' +\
'#SBATCH --ntasks-per-node=1\n' +\
'#SBATCH --cpus-per-task=50\n' +\
'#SBATCH --partition=amd\n' +\
'#SBATCH --error=gnn.err\n' +\
'#SBATCH --out=gnn.out\n' +\
'#SBATCH --time=15:00:00\n' +\
'#SBATCH --job-name=AEZAKMI\n' +\
'\n' +\
'ml PyTorch/1.13.0-foss-2022a-CUDA-11.7.0\n' +\
'pip install --user pytorch-lightning\n' +\
'pip install --user -U rich\n' +\
'pip install --user frozendict\n'


def load_job_command_results(problem_folder_name: str, results_output_folder_path: Path, trained_models_folder_path: Path, fastdownward_pruning: bool) -> str:
    return 'srun python resultor.py' +\
    ' --mode generate' +\
    f' --problem_folder_name {problem_folder_name}' +\
    f' --results_output_folder_path {results_output_folder_path}' +\
    f' --trained_models_folder_path {trained_models_folder_path}' +\
    f' --fastdownward_pruning {str(fastdownward_pruning)}'


def load_job_results(problem_folder_name: str, results_output_folder_path: Path, trained_models_folder_path: Path, fastdownward_pruning: bool) -> str:
    return load_job_info_results() + load_job_command_results(problem_folder_name, results_output_folder_path, trained_models_folder_path, fastdownward_pruning)


def create_jobs_results_for(problem_name: str, results_output_folder_path: Path, trained_models_folder_path: Path, fastdownward_pruning: bool, job_name: str, job_output_folder_path: Path):
    job_string = load_job_results(problem_name, results_output_folder_path, trained_models_folder_path, fastdownward_pruning)
    job_output_file_name = 'job_' + job_name
    write_job(job_string, job_output_folder_path, job_output_file_name)


def create_jobs_results(tuple_problem_names: List[Tuple[str, str]], results_output_folder_path: Path, trained_models_folder_path: Path, fastdownward_pruning: bool):
    for problem_name, problem_name_snake in tuple_problem_names:
        job_output_folder_path = Path(os.path.join('Jobs', 'Results', problem_name))

        create_jobs_results_for(problem_name, results_output_folder_path, trained_models_folder_path, fastdownward_pruning, problem_name_snake, job_output_folder_path)


def run_jobs_results(jobs_folder_path: Path, problem_names: List[str]):
    for problem_name in problem_names:
        path = Path(os.path.join(jobs_folder_path, problem_name))
        for job in path.glob('*.sh'):
            os.system('sbatch' + ' ' + str(job))


def create_and_run_jobs_results_all():
    create_jobs_results(get_tuple_problem_names(), Path('ResultsFull'), Path('TrainedModelsFull'), fastdownward_pruning=False) 
    run_jobs_results(Path(os.path.join('Jobs', 'Results')), get_problem_names())


if __name__ == "__main__":
    pass