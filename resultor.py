import subprocess
import multiprocessing
from multiprocessing import Pool
import shutil
import uuid
import argparse
from types import SimpleNamespace
import os
from pathlib import Path
from typing import List, Dict, Tuple 
from Program.Code.utils import get_problem_names, get_model_types

from rich.console import Console
from rich.table import Table
import pandas as pd
from rich_tools import table_to_df, df_to_table
import subprocess
import re
from Program.Code.utils import str2bool
import numpy as np

def generate_result_gnn(model_type, model_file_path, pddl_domain_file_path, pddl_domain_instance_file_path, output_folder_path, output_file_name, fastdownward_pruning: bool):
    gnn_unique_log_file_name = uuid.uuid4().hex + '_gnn-log.txt'
    
    with open(gnn_unique_log_file_name, mode='w') as file:
        print('        Start Gnn ' + output_file_name)
        subprocess.run(f'python {os.path.abspath(os.path.join("Program", "main_terminal_gnn.py"))} --mode predict --model_type {model_type} --model_file_path {model_file_path} --fastdownward_folder_path {os.path.abspath("FastDownwardPlannerWindows") if os.name == "nt" else os.path.abspath("FastDownwardPlannerLinux")} --pddl_domain_file_path {pddl_domain_file_path} --pddl_domain_instance_file_path {pddl_domain_instance_file_path}  --output_folder_path {output_folder_path} --output_file_name {output_file_name} --fastdownward_pruning {fastdownward_pruning} --info false', shell=True, stderr=file, stdout=file)

    if os.path.exists(gnn_unique_log_file_name):
        os.remove(gnn_unique_log_file_name)

    print('        Done Gnn ' + output_file_name)


def generate_result_fastdownward(pddl_domain_file_path, pddl_domain_instance_file_path, output_folder_path, output_file_name):
    fastdownward_unique_log_file_name = uuid.uuid4().hex + '_fast-downward-log.txt'

    with open(fastdownward_unique_log_file_name, mode='w') as file:
        print('        Start Fast ' + output_file_name)
        unique_sas_file_name = uuid.uuid4().hex + '_output.sas'
        subprocess.run(f'python {os.path.abspath(os.path.join("FastDownwardPlannerWindows" if os.name == "nt" else "FastDownwardPlannerLinux", "fast-downward.py"))} --translate-time-limit 30 --plan-file {output_file_name} --sas-file {unique_sas_file_name} {pddl_domain_file_path} {pddl_domain_instance_file_path} --search "astar(lmcut())"', shell=True, stderr=file, stdout=file)

        if os.path.exists(unique_sas_file_name):
            os.remove(unique_sas_file_name)

        if os.path.exists(output_file_name):
            os.makedirs(output_folder_path, exist_ok=True)
            shutil.move(output_file_name, os.path.join(output_folder_path, output_file_name))
    
        
    if os.path.exists(fastdownward_unique_log_file_name):
        os.remove(fastdownward_unique_log_file_name)
    print('        Done Fast ' + output_file_name)


def generate_results(problem_folder_name: str, results_output_folder_path: Path, trained_models_folder_path: Path, fastdownward_pruning: bool):
    pddl_test_dataset_folder_path = Path(os.path.abspath(os.path.join('Data', 'Pddl', problem_folder_name, 'Test')))

    with Pool(processes=multiprocessing.cpu_count()) as pool:
        pddl_domain_file_path = os.path.join(pddl_test_dataset_folder_path, 'domain.pddl')
        for pddl_domain_instance_file_path in pddl_test_dataset_folder_path.glob('*.pddl'):
            if str(pddl_domain_file_path) == str(pddl_domain_instance_file_path):
                continue


            output_folder_path = Path(os.path.join(os.path.abspath(results_output_folder_path), problem_folder_name, os.path.basename(os.path.normpath(pddl_domain_instance_file_path)).replace('.pddl', '')))


            for model_type in get_model_types():
                path = Path(os.path.abspath(os.path.join(os.path.abspath(trained_models_folder_path), problem_folder_name, model_type.capitalize(), 'Version_0', 'Checkpoints')))
                model_file_path = list(path.glob('*.ckpt'))[0]
                
                output_file_name = os.path.basename(os.path.normpath(pddl_domain_instance_file_path)).replace('.pddl', '') + '_sas_plan_gnn' + '_' + model_type
                print('Submitting ' + str(output_file_name))
                pool.apply_async(generate_result_gnn, args=(model_type, model_file_path, pddl_domain_file_path, pddl_domain_instance_file_path, output_folder_path, output_file_name, fastdownward_pruning, ))
                
            
            output_file_name_fastdownward = os.path.basename(os.path.normpath(pddl_domain_instance_file_path)).replace('.pddl', '_sas_plan_fastdownward')
            if not os.path.exists(os.path.join(output_folder_path, output_file_name_fastdownward)):
                print('Submitting ' + str(output_file_name_fastdownward))
                pool.apply_async(generate_result_fastdownward, args=(pddl_domain_file_path, pddl_domain_instance_file_path, output_folder_path, output_file_name_fastdownward, ))


        pool.close()
        pool.join()


def is_predicted_plan_optimal(output_file_name_gnn: Path, output_file_name_fastdownward: Path):
    with output_file_name_gnn.open('r') as file: 
        gnn_lines = [line.strip() for line in file.readlines()]
        gnn_cost_line = gnn_lines[-1]
        gnn_cost = float(gnn_cost_line.replace('; cost = ', '').replace(' (unit cost)', ''))

    with output_file_name_fastdownward.open('r') as file: 
        fastdownward_lines = [line.strip() for line in file.readlines()]
        fastdownward_cost_line = fastdownward_lines[-1]
        fastdownward_cost = float(fastdownward_cost_line.replace('; cost = ', '').replace(' (unit cost)', ''))

    return gnn_cost == fastdownward_cost and len(gnn_lines) == len(fastdownward_lines)


def create_table(columns: List[str], rows: List[List[str]]) -> Table:
    table = Table(title="Results")

    for column in columns:
        table.add_column(column)

    for row in rows:
        table.add_row(*row, style='bright_green')

    return table


def examine_results(results_folder_path: Path):
    console = Console()

    columns = ["Domain(#)", "GNN-SUM: Optimal or Only GNN", "GNN-SUM: Suboptimal, Failed GNN or Failed Both", "GNN-MAX: Optimal or Only GNN", "GNN-MAX: Suboptimal, Failed GNN or Failed Both"]
    rows: List[List[str]] = []

    total = 0

    global_optimal_gnn_add = 0
    global_failed_only_gnn_add = 0
    global_only_gnn_add = 0
    global_suboptimal_gnn_add = 0
    global_failed_both_add = 0

    global_optimal_gnn_max = 0
    global_failed_only_gnn_max = 0
    global_only_gnn_max = 0
    global_suboptimal_gnn_max = 0
    global_failed_both_max = 0

    for problem_folder_path in os.scandir(os.path.abspath(results_folder_path)):
        optimal_gnn_add = 0
        failed_only_gnn_add = 0
        only_gnn_add = 0
        suboptimal_gnn_add = 0
        failed_both_add = 0

        optimal_gnn_max = 0
        failed_only_gnn_max = 0
        only_gnn_max = 0
        suboptimal_gnn_max = 0
        failed_both_max = 0

        for specific_problem_folder_path in os.scandir(problem_folder_path):
            plans = [plan.name for plan in os.scandir(specific_problem_folder_path)]

            total += 1

            # if any('_sas_plan_gnn_add' in plan for plan in plans):
            #     sas_plan_gnn_add_path = os.path.join(results_folder_path, problem_folder_path.name, specific_problem_folder_path.name, specific_problem_folder_path.name + '_sas_plan_gnn_add')
            #     os.remove(sas_plan_gnn_add_path)

            # if any('_sas_plan_gnn_max' in plan for plan in plans):
            #     sas_plan_gnn_max_path = os.path.join(results_folder_path, problem_folder_path.name, specific_problem_folder_path.name, specific_problem_folder_path.name + '_sas_plan_gnn_max')
            #     os.remove(sas_plan_gnn_max_path)

            if any('_sas_plan_fastdownward' in plan for plan in plans):
                sas_plan_fastdownward_path = os.path.join(os.path.abspath(results_folder_path), problem_folder_path.name, specific_problem_folder_path.name, specific_problem_folder_path.name + '_sas_plan_fastdownward')

                if any('_sas_plan_gnn_add' in plan for plan in plans):
                    sas_plan_gnn_add_path = os.path.join(os.path.abspath(results_folder_path), problem_folder_path.name, specific_problem_folder_path.name, specific_problem_folder_path.name + '_sas_plan_gnn_add')
                    if is_predicted_plan_optimal(Path(sas_plan_gnn_add_path), Path(sas_plan_fastdownward_path)):
                        optimal_gnn_add += 1
                    else:
                        suboptimal_gnn_add += 1
                else:
                    failed_only_gnn_add += 1


                if any('_sas_plan_gnn_max' in plan for plan in plans):
                    sas_plan_gnn_max_path = os.path.join(os.path.abspath(results_folder_path), problem_folder_path.name, specific_problem_folder_path.name, specific_problem_folder_path.name + '_sas_plan_gnn_max')
                    if is_predicted_plan_optimal(Path(sas_plan_gnn_max_path), Path(sas_plan_fastdownward_path)):
                        optimal_gnn_max += 1
                    else:
                        suboptimal_gnn_max += 1
                else:
                    failed_only_gnn_max += 1
            else: # fast downward didnt find anything
                if any('_sas_plan_gnn_add' in plan for plan in plans):
                    only_gnn_add += 1
                else:
                    failed_both_add += 1

                if any('_sas_plan_gnn_max' in plan for plan in plans):
                    only_gnn_max += 1
                else:
                    failed_both_max += 1

        row = []

        problem_count = len(list(os.scandir(problem_folder_path)))

        row = [
            f'{problem_folder_path.name} ({problem_count})',
            f'{optimal_gnn_add} + {only_gnn_add} ({optimal_gnn_add + only_gnn_add})',
            f'{suboptimal_gnn_add} + {failed_only_gnn_add} + {failed_both_add} ({suboptimal_gnn_add + failed_only_gnn_add + failed_both_add})',
            f'{optimal_gnn_max} + {only_gnn_max} ({optimal_gnn_max + only_gnn_max})',
            f'{suboptimal_gnn_max} + {failed_only_gnn_max} + {failed_both_max} ({suboptimal_gnn_max + failed_only_gnn_max + failed_both_max})'
        ]
       
        rows.append(row)

        global_optimal_gnn_add      += optimal_gnn_add
        global_failed_only_gnn_add  += failed_only_gnn_add
        global_only_gnn_add         += only_gnn_add
        global_suboptimal_gnn_add   += suboptimal_gnn_add
        global_failed_both_add      += failed_both_add
        
        global_optimal_gnn_max      += optimal_gnn_max
        global_failed_only_gnn_max  += failed_only_gnn_max
        global_only_gnn_max         += only_gnn_max
        global_suboptimal_gnn_max   += suboptimal_gnn_max
        global_failed_both_max      += failed_both_max

    row = [
            f'Total ({total})',
            f'{global_optimal_gnn_add} + {global_only_gnn_add} ({global_optimal_gnn_add + global_only_gnn_add} = {((global_optimal_gnn_add + global_only_gnn_add)/total)*100:.2f}%)',
            f'{global_suboptimal_gnn_add} + {global_failed_only_gnn_add} + {global_failed_both_add} ({global_suboptimal_gnn_add + global_failed_only_gnn_add + global_failed_both_add} = {((global_suboptimal_gnn_add + global_failed_only_gnn_add + global_failed_both_add)/total)*100:.2f}%)',
            f'{global_optimal_gnn_max} + {global_only_gnn_max} ({global_optimal_gnn_max + global_only_gnn_max} = {((global_optimal_gnn_max + global_only_gnn_max)/total)*100:.2f}%)',
            f'{global_suboptimal_gnn_max} + {global_failed_only_gnn_max} + {global_failed_both_max} ({global_suboptimal_gnn_max + global_failed_only_gnn_max + global_failed_both_max} = {((global_suboptimal_gnn_max + global_failed_only_gnn_max + global_failed_both_max)/total)*100:.2f}%)'
        ]
       
    rows.append(row)

    table = create_table(columns, rows)
    console.print(table)

    table_to_df(table).to_csv('_'.join(re.split('(?<=.)(?=[A-Z])', os.path.basename(os.path.normpath(os.path.abspath(results_folder_path))))).lower() + '.csv', index=False)


def read_results(results_csv_file_path: Path):
    console = Console()
    table = df_to_table(pd.read_csv(os.path.abspath(results_csv_file_path)))

    for row in table.rows:
       row.style = 'bright_green'

    console.print(table)


def parse_arguments():
    # Create starting parser to choose the mode of gnn
    mode_argument_parser = argparse.ArgumentParser(add_help=False)
    mode_argument_parser.add_argument('--mode', required=True, type=str, help='Results mode: generate, examine, read')
    mode_argument, _ = mode_argument_parser.parse_known_args()


    # Create the main parser with the knowledge of choosen mode
    parser = argparse.ArgumentParser(parents=[mode_argument_parser])

    # generate mode
    if (mode_argument.mode == 'generate'):
        parser.add_argument('--problem_folder_name', required=True, type=str, help='Name of PDDL problem folder in "Data/Pddl" with "Test" subfolder, a folder with this name will be created in results folder')
        parser.add_argument('--results_output_folder_path', required=True, type=Path, help='Full or relative path of output results folder, where folder for every problem will be created')
        parser.add_argument('--trained_models_folder_path', required=True, type=Path, help='Full or relative path of trained models')
        parser.add_argument('--fastdownward_pruning', default=False, type=str2bool, help='Whether SAS file should be pruned')
    # examine mode
    elif (mode_argument.mode == 'examine'):
        parser.add_argument('--results_folder_path', required=True, type=Path, help='Full or relative path to results folder, a csv file with that name will be created')
    elif (mode_argument.mode == 'read'):
        parser.add_argument('--results_csv_file_path', required=True, type=Path, help='Full or relative path to generated results csv file')
    else:
        raise Exception('Unknown type of mode')
    

    arguments = parser.parse_args()
    return SimpleNamespace(**arguments.__dict__)


if __name__ == '__main__':
    config = parse_arguments()

    if config.mode == 'generate':
        generate_results(config.problem_folder_name, config.results_output_folder_path, config.trained_models_folder_path, config.fastdownward_pruning)
    elif config.mode == 'examine':
        examine_results(config.results_folder_path)
    elif config.mode == 'read':
        read_results(config.results_csv_file_path)
    else:
        raise Exception('Unknown type of mode')
         