#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=16G
#SBATCH --gres=gpu:1
#SBATCH --partition=gpulong
#SBATCH --error=gnn.err
#SBATCH --out=gnn.out
#SBATCH --time=15:00:00
#SBATCH --job-name=HESOYAM

ml PyTorch/1.13.0-foss-2022a-CUDA-11.7.0
pip install --user pytorch-lightning
pip install --user -U rich
pip install --user frozendict
srun python Program/main_terminal_gnn.py --mode train --model_type max --problem_name ParkingBehind --train_dataset_folder_path Data\Json\ParkingBehind\Train --validation_dataset_folder_path Data\Json\ParkingBehind\Validation --accelerator gpu --devices 1 --patience 1000 --loader_num_workers 8