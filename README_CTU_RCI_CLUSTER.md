# RCI CLUSTER
Manual how to connect to RCI Cluster 

## Table of Contents
1. [General Info](#general-info)
2. [Interactive Job](#interactive-job)
3. [Noninteractive Job](#noninteractive-job)
4. [Jober](#jober)

## General Info
The authors acknowledge the support of the OP VVV funded project CZ.02.1.01/0.0/0.0/16_019/0000765 "Research Center for Informatics".
Read info here: https://login.rci.cvut.cz/wiki/how_to_start

## Interactive Job
1. Connect to RCI cluster. Address of the access node is: login.rci.cvut.cz (for older Intel nodes n01-n33) or login3.rci.cvut.cz (for newer AMD nodes a01-a16,g01-g12)
```
ssh username@login.rci.cvut.cz
```
2. Using (for example WinSCP) add GNN folder there
3. Set up modules
```
ml PyTorch/1.13.0-foss-2022a-CUDA-11.7.0
pip install --user pytorch-lightning
pip install --user -U rich
pip install --user frozendict
```
4. Start interactive job
```
srun -p amdgpufast --pty --gres=gpu:1 bash -i

srun -p cpufast --pty bash -i
```
5. Go into GNN - gnn.py will create folder trained_models there (in open directory)
```
cd GNN
```
6. Run your code
```
python ./code/gnn.py --mode train --model_type max --train_dataset_path ./data/train/blocks-clear/ --validation_dataset_path ./data/validation/blocks-clear/ --accelerator gpu --devices 1 --loader_num_workers 2
```

## Noninteractive Job
1. Connect to RCI cluster (https://login.rci.cvut.cz/wiki/how_to_start) Address of the access node is: login.rci.cvut.cz (for older Intel nodes n01-n33) or login3.rci.cvut.cz (for newer AMD nodes a01-a16,g01-g12) (username is your username)
```
ssh username@login.rci.cvut.cz
```
2. Using (for example WinSCP) add GNN folder there
3. Go into GNN - gnn.py will create folder trained_models there (in open directory)
```
cd GNN
```
4. Set up modules
```
ml PyTorch/1.13.0-foss-2022a-CUDA-11.7.0
pip install --user pytorch-lightning
pip install --user -U rich
pip install --user frozendict
```
5. Start noninteractive job with batch - will give you message "Submitted batch job N", where N is some number; (job.sh is your prepared batch bash script)
```
sbatch job.sh
```
6. Check your job (username is your username)
```
squeue --user username
```
7. To cancel your job (N is some number given you in step 5.)
```
scancel N
```
```
scancel --user username 
```

## Jober
1. Connect to RCI cluster (https://login.rci.cvut.cz/wiki/how_to_start) Address of the access node is: login.rci.cvut.cz (for older Intel nodes n01-n33) or login3.rci.cvut.cz (for newer AMD nodes a01-a16,g01-g12) (username is your username)
```
ssh username@login.rci.cvut.cz
```
2. Using (for example WinSCP) add GNN folder there
3. Go into GNN - gnn.py will create folder trained_models there (in open directory)
```
cd GNN
```
4. Prepare master automation function of creating and summiting jobs (create_and_run_jobs_single_mode_all, create_and_run_jobs_train_mode_all, create_and_run_jobs_results_all, etc.) in jober.py script in main function of the script
5. Run jober.py script
```
python jober.py
```