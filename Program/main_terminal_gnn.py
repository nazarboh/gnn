
import argparse
from pathlib import Path
from Code.gnn import Gnn
from Code.utils import str2bool
from Code.configs import TrainConfig, ResumeConfig, TestConfig, PredictConfig

def parse_arguments():
    # Create starting parser to choose the mode of gnn
    mode_argument_parser = argparse.ArgumentParser(add_help=False)
    mode_argument_parser.add_argument('--mode', required=True, type=str, help='GNN mode: train, resume, test')
    mode_argument, _ = mode_argument_parser.parse_known_args()

    # Create the main parser with the knowledge of choosen mode
    parser = argparse.ArgumentParser(parents=[mode_argument_parser])
    parser.add_argument('--model_type', required=True, type=str, help='add or max')

    # train mode
    if (mode_argument.mode == 'train'):
        parser.add_argument('--problem_name', required=True, type=str, help='Name of problem, a folder with this name will be created for gnn output files')
        parser.add_argument('--train_dataset_folder_path', required=True, type=Path, help='Full or relative path to training dataset folder consisting of "_cost_stete_pairs" ".txt" gnn input files')
        parser.add_argument('--validation_dataset_folder_path', required=True, type=Path, help='Full or relative path to validation dataset folder consisting of "_cost_stete_pairs" ".txt" gnn input files')
        parser.add_argument('--hidden_size', default=32, type=int, help='Number of features per object (default is 32)')
        parser.add_argument('--iterations', default=30, type=int, help='Number of convolutions (default is 30)')
        parser.add_argument('--batch_size', default=16, type=int, help='Maximum size of batches (default is 16)')
        parser.add_argument('--accelerator', default='cpu', type=str, help='Type of process unit: cpu, gpu, tpu, ipu or hpu (default is cpu)')
        parser.add_argument('--devices', default=1, type=int, help='Count of accelerators to be run on (default is 1)')
        parser.add_argument('--max_epochs', default=1000, type=int, help='Limit number of train epochs (default is 1000)')
        parser.add_argument('--learning_rate', default=0.0002, type=float, help='Learning rate of training session (default is 0.0002)')
        parser.add_argument('--l1_factor', default=0.0001, type=float, help='Strength of L1 regularization (default is 0.0001)')
        parser.add_argument('--weight_decay', default=0.0, type=float, help='Strength of weight decay regularization (default is 0)') # Weight decay is a regularization technique that is used in machine learning to reduce the complexity of a model and prevent overfitting
        parser.add_argument('--gradient_accumulation', default=1, type=int, help='Number of gradients to accumulate before step (default is 1)')
        parser.add_argument('--max_samples_per_value', default=100, type=int, help='Maximum number of states per value st per dataset (default is 100)')
        parser.add_argument('--patience', default=200, type=int, help='Patience for early stopping (default is 200)')
        parser.add_argument('--gradient_clip', default=0.01, type=float, help='Gradient clip value (default is 0.01)') # Gradient Clipping is a method where the error derivative is changed or clipped to a threshold during backward propagation through the network, and using the clipped gradients to update the weights.
        parser.add_argument('--profiler', default=None, type=str, help='Profiler type: simple, advanced or pytorch (default None)')
        parser.add_argument('--loader_num_workers', default=0, type=int, help='Number of workers in loader (default 0)')
        parser.add_argument('--info', default=True, type=str2bool, help='Whether print info (default is True)')
    # resume mode
    elif (mode_argument.mode == 'resume'):
        parser.add_argument('--model_file_path', required=True, type=Path, help='Full or relative path to the model ".ckpt" file')
        parser.add_argument('--problem_name', required=True, type=str, help='Name of problem, a folder with this name will be created for gnn output files')
        parser.add_argument('--train_dataset_folder_path', required=True, type=Path, help='Full or relative path to training dataset folder consisting of "_cost_stete_pairs" ".txt" gnn input files')
        parser.add_argument('--validation_dataset_folder_path', required=True, type=Path, help='Full or relative path to validation dataset folder consisting of "_cost_stete_pairs" ".txt" gnn input files')
        parser.add_argument('--batch_size', default=16, type=int, help='Maximum size of batches (default is 16)')
        parser.add_argument('--accelerator', default='cpu', type=str, help='Type of process unit: cpu, gpu, tpu, ipu or hpu (default is cpu)')
        parser.add_argument('--devices', default=1, type=int, help='Count of accelerators to be run on (default is 1)')
        parser.add_argument('--max_epochs', default=1000, type=int, help='Limit number of train epochs (default is 1000)')
        parser.add_argument('--gradient_accumulation', default=1, type=int, help='Number of gradients to accumulate before step (default is 1)')
        parser.add_argument('--max_samples_per_value', default=100, type=int, help='Maximum number of states per value per dataset (default is 100)')
        parser.add_argument('--patience', default=200, type=int, help='Patience for early stopping (default is 200)')
        parser.add_argument('--gradient_clip', default=0.01, type=float, help='Gradient clip value (default is 0.01)') # Gradient Clipping is a method where the error derivative is changed or clipped to a threshold during backward propagation through the network, and using the clipped gradients to update the weights.
        parser.add_argument('--profiler', default=None, type=str, help='Profiler type: simple, advanced or pytorch (default None)')
        parser.add_argument('--loader_num_workers', default=0, type=int, help='Number of workers in loader (default 0)')
        parser.add_argument('--info', default=True, type=str2bool, help='Whether print info (default is True)')
    # test mode
    elif (mode_argument.mode == 'test'):
        parser.add_argument('--model_file_path', required=True, type=Path, help='Full or relative path to the model ".ckpt" file')
        parser.add_argument('--test_dataset_folder_path', required=True, type=Path, help='Full or relative path to test dataset folder consisting of "_cost_stete_pairs" ".txt" gnn input files')
        parser.add_argument('--batch_size', default=16, type=int, help='Maximum size of batches (default is 16)')
        parser.add_argument('--accelerator', default='cpu', type=str, help='Type of process unit: cpu, gpu, tpu, ipu or hpu (default is cpu)')
        parser.add_argument('--devices', default=1, type=int, help='Count of accelerators to be run on (default is 1)')
        parser.add_argument('--max_samples_per_value', default=100, type=int, help='Maximum number of states per value per dataset (default is 100)')
        parser.add_argument('--loader_num_workers', default=0, type=int, help='Number of workers in loader (default 0)')
        parser.add_argument('--info', default=True, type=str2bool, help='Whether print info (default is True)')
    elif (mode_argument.mode == 'predict'):
        parser.add_argument('--model_file_path', required=True, type=Path, help='Full or relative path to the model ".ckpt" file')
        parser.add_argument('--fastdownward_folder_path', required=True, type=Path, help='Full or relative path to folder with the Fast Downward planner')
        parser.add_argument('--pddl_domain_file_path', required=True, type=Path, help='Full or relative path to domain pddl file')
        parser.add_argument('--pddl_domain_instance_file_path', required=True, type=Path, help='Full or relative path to domain instance pddl file')
        parser.add_argument('--step_limit', default=100, type=int, help='Max count of steps in plan, returns no plan with cost 0 if exceeded')
        parser.add_argument('--output_folder_path', default='', type=Path, help='Full or relative path to output folder (default is active folder in terminal)')
        parser.add_argument('--output_file_name', default='', type=str, help='Name of output file (default is "name of pddl instance file")')
        parser.add_argument('--fastdownward_pruning', default=False, type=str2bool, help='Whether space should be pruned (default is False)')
        parser.add_argument('--info', default=True, type=str2bool, help='Whether print info (default is True)')
        parser.add_argument('--clean_up', default=True, type=str2bool, help='Whether clean up all temporary files (default is True)')

    arguments = parser.parse_args()

    return arguments


def print_arguments(args):
    print()
    print('Gnn parameters:')

    print(f'--mode = {args.mode}')
    print(f'--model_type = {args.model_type}')

    if args.mode == 'train':
        print(f'--problem_name = {args.problem_name}')
        print(f'--train_dataset_folder_path = {args.train_dataset_folder_path}')
        print(f'--validation_dataset_folder_path = {args.validation_dataset_folder_path}')
        print(f'--hidden_size = {args.hidden_size}')
        print(f'--iterations = {args.iterations}')
        print(f'--batch_size = {args.batch_size}')
        print(f'--accelerator = {args.accelerator}')
        print(f'--devices = {args.devices}')
        print(f'--max_epochs = {args.max_epochs}')
        print(f'--learning_rate = {args.learning_rate}')
        print(f'--l1_factor = {args.l1_factor}')
        print(f'--weight_decay = {args.weight_decay}')
        print(f'--gradient_accumulation = {args.gradient_accumulation}')
        print(f'--max_samples_per_value = {args.max_samples_per_value}')
        print(f'--patience = {args.patience}')
        print(f'--gradient_clip = {args.gradient_clip}')
        print(f'--profiler = {args.profiler}')
        print(f'--loader_num_workers = {args.loader_num_workers}')
        print(f'--info = {args.info}')
    elif args.mode == 'resume':
        print(f'--model_file_path = {args.model_file_path}')
        print(f'--problem_name = {args.problem_name}')
        print(f'--train_dataset_folder_path = {args.train_dataset_folder_path}')
        print(f'--validation_dataset_folder_path = {args.validation_dataset_folder_path}')
        print(f'--batch_size = {args.batch_size}')
        print(f'--accelerator = {args.accelerator}')
        print(f'--devices = {args.devices}')
        print(f'--max_epochs = {args.max_epochs}')
        print(f'--gradient_accumulation = {args.gradient_accumulation}')
        print(f'--max_samples_per_value = {args.max_samples_per_value}')
        print(f'--patience = {args.patience}')
        print(f'--gradient_clip = {args.gradient_clip}')
        print(f'--profiler = {args.profiler}')
        print(f'--loader_num_workers = {args.loader_num_workers}')
        print(f'--info = {args.info}')
    elif args.mode == 'test':
        print(f'--model_file_path = {args.model_file_path}')
        print(f'--test_dataset_folder_path = {args.test_dataset_folder_path}')
        print(f'--batch_size = {args.batch_size}')
        print(f'--accelerator = {args.accelerator}')
        print(f'--devices = {args.devices}')
        print(f'--max_samples_per_value = {args.max_samples_per_value}')
        print(f'--loader_num_workers = {args.loader_num_workers}')
        print(f'--info = {args.info}')
    elif args.mode == 'predict':
        print(f'--model_file_path = {args.model_file_path}')
        print(f'--fastdownward_folder_path = {args.fastdownward_folder_path}')
        print(f'--pddl_domain_file_path = {args.pddl_domain_file_path}')
        print(f'--pddl_domain_instance_file_path = {args.pddl_domain_instance_file_path}')
        print(f'--step_limit = {args.step_limit}')
        print(f'--output_folder_path = {args.output_folder_path}')
        print(f'--output_file_name = {args.output_file_name}')
        print(f'--fastdownward_pruning = {args.fastdownward_pruning}')
        print(f'--info = {args.info}')
        print(f'--clean_up = {args.clean_up}')

    print()
        

if __name__ == "__main__":
    args = parse_arguments()
    if args.info: print_arguments(args)

    gnn = Gnn()
    if args.mode == 'train':
        gnn.train(TrainConfig(**args.__dict__))
    elif args.mode == 'resume':
        gnn.resume(ResumeConfig(**args.__dict__))
    elif args.mode == 'test':
        gnn.test(TestConfig(**args.__dict__))
    elif args.mode == 'predict':
        gnn.predict(PredictConfig(**args.__dict__))
    else:
        raise ValueError('Unknown type of mode, exiting...')