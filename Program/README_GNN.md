# GNN
This is tutorial how to work with GNN (Graph Neural Network) (main_gnn.py and main_terminal_gnn.py)

## Table of Contents
1. [Technologies & Packages](#technologies-and-packages)
2. [GitLab](#gitlab)
3. [Usage](#collaboration)

## Technologies and Packages
A list of technologies used within the project:
* Python [https://www.python.org/downloads/](https://www.python.org/downloads/)
* Compiled Fast Downward Planner on your platform [https://www.fast-downward.org/](https://www.fast-downward.org/)
* ```pytorch``` package is required. See info [https://pytorch.org/get-started/locally/](https://pytorch.org/get-started/locally/).
* ```pytorch-lightning``` package is required. See info [https://lightning.ai/docs/pytorch/stable/starter/installation.html](https://lightning.ai/docs/pytorch/stable/starter/installation.html).
* ```frozendict``` package is required for ```predict``` mode. See info [https://pypi.org/project/frozendict/](https://pypi.org/project/frozendict/).

## GitLab
Find the project [https://gitlab.fel.cvut.cz/nazarboh/gnn](https://gitlab.fel.cvut.cz/nazarboh/gnn).

## Usage
### Args passed through one by one
#### Usage of **train** mode :
To get help use:
```
Program/main_terminal_gnn.py --mode train --help
```

Arguments of **train** mode:
```
--mode MODE (required: train)
	Planner mode: train, resume, test, predict

--model_type TYPE (required: add or max)
	Type of GNN model: add or max

--problem_name NAME(required)
	Name of problem, a folder with this name will be created for GNN output files

--train_dataset_folder_path PATH (required)
	Full or relative path to training dataset folder consisting of "_cost_state_pairs" ".txt" GNN input files

--validation_dataset_folder_path PATH(required)
	Full or relative path to validation dataset folder consisting of "_cost_state_pairs" ".txt" GNN input files

--hidden_size NUMBER (default=32)
	Number of features per object

--iterations NUMBER (default=30)
	Number of convolutions

--batch_size NUMBER (default=16)
	Maximum size of batches

--accelerator TYPE (default="cpu")
	Type of process unit: cpu, gpu, tpu, ipu or hpu

--devices NUMBER (default=1)
	Count of accelerators to be run on

--max_epochs NUMBER (default=1000)
	Limit number of train epochs

--learning_rate FLOAT (default=0.0002)
	Learning rate of training session

--l1_factor FLOAT (default=0.0001)
	Strength of L1 regularization

--weight_decay FLOAT (default=0.0)
	Strength of weight decay regularization

--gradient_accumulation NUMBER (default=1)
	Number of gradients to accumulate before step

--max_samples_per_value NUMBER (default=100)
	Maximum number of states per value per instance in dataset

--patience NUMBER (default=200)
	Patience for early stopping

--gradient_clip FLOAT (default=0.01)
	Gradient clip value

--profiler TYPE (default=null)
	Profiler type: simple, advanced or pytorch

--loader_num_workers NUMBER (default=0)
	Number of workers in dataset loader

--info BOOLEAN (default=True)
	Whether print out info
```

Example command run of **train** mode (minimal required args):
```
python3 Program/main_terminal_gnn.py
--mode train
--model_type max
--problem_name BlocksClear
--train_dataset_folder_path Data/Json/BlocksClear/Train
--validation_dataset_folder_path Data/Json/BlocksClear/Validation
```

#### Usage of **resume** mode :
To get some help use
```
Program/main_terminal_gnn.py --mode resume --help
```

Arguments of **resume** mode:
```
--mode MODE (required: resume)
	Planner mode: train, resume, test, predict
	
--model_type TYPE (required: add or max)
	Type of GNN model: add or max

--model_file_path PATH (required)
	Full or relative path to the model ".ckpt" file

--problem_name NAME (required)
	Name of problem, a folder with this name will be created for GNN output files

--train_dataset_folder_path PATH (required)
	Full or relative path to training dataset folder consisting of "_cost_state_pairs" ".txt" GNN input files

--validation_dataset_folder_path PATH (required)
	Full or relative path to validation dataset folder consisting of "_cost_state_pairs" ".txt" GNN input files

--batch_size NUMBER (default=16)
	Maximum size of batches

--accelerator TYPE (default="cpu")
	Type of process unit: cpu, gpu, tpu, ipu or hpu

--devices NUMBER (default=1)
	Count of accelerators to be run on

--max_epochs NUMBER (default=1000)
	Limit number of train epochs

--gradient_accumulation NUMBER (default=1)
	Number of gradients to accumulate before step

--max_samples_per_value NUMBER (default=100)
	Maximum number of states per value per instance in dataset

--patience NUMBER (default=200)
	Patience for early stopping

--gradient_clip FLOAT (default=0.01)
	Gradient clip value

--profiler TYPE (default=null)
	Profiler type: simple, advanced or pytorch

--loader_num_workers NUMBER (default=0)
	Number of workers in dataset loader

--info BOOLEAN (default=True)
	Whether print out info
```

Example command run of **resume** mode (minimal required args):
```
python3 Program/main_terminal_gnn.py
--mode resume
--model_type max
--model_file_path TrainedModels/BlocksClear/Max/Version_0/Checkpoints/epoch=169-validation_loss=0.035.ckpt
--problem_name BlocksClear
--train_dataset_folder_path Data/Json/BlocksClear/Train
--validation_dataset_folder_path Data/Json/BlocksClear/Validation
```

#### Usage of **test** mode :
To get some help use
```
Program/main_terminal_gnn.py --mode test --help
```

Arguments of **test** mode:
```
--mode MODE (required: test)
	Planner mode: train, resume, test, predict
	
--model_type TYPE (required: add or max)
	Type of GNN model: add or max
	
--model_file_path PATH (required)
	Full or relative path to the model ".ckpt" file
	
--test_dataset_folder_path PATH (required)
	Full or relative path to test dataset folder consisting of "_cost_state_pairs" ".txt" GNN input files
	
--batch_size NUMBER (default=16)
	Maximum size of batches
	
--accelerator TYPE (default="cpu")
	Type of process unit: cpu, gpu, tpu, ipu or hpu
	
--devices NUMBER (default=1)
	Count of accelerators to be run on
	
--max_samples_per_value NUMBER (default=100)
	Maximum number of states per value per instance in dataset
	
--loader_num_workers NUMBER (default=0)
	Number of workers in dataset loader
	
--info BOOLEAN (default=True)
	Whether print out info
```

Example command run of **test** mode (minimal required args):
```
python3 Program/main_terminal_gnn.py
--mode test
--model_type max
--model_file_path TrainedModels/BlocksClear/Max/Version_0/Checkpoints/epoch=169-validation_loss=0.035.ckpt
--test_dataset_folder_path Data/Json/BlocksClear/Test

```

#### Usage of **predict** mode :
To get some help use
```
Program/main_terminal_gnn.py --mode predict --help
```

Arguments of **predict** mode:
```
--mode MODE (required: predict)
	Planner mode: train, resume, test, predict
	
--model_type TYPE (required: add or max)
	Type of GNN model: add or max
	
--model_file_path PATH (required)
	Full or relative path to the model ".ckpt" file
	
--fastdownward_folder_path PATH (required)
	Full or relative path to folder with compiled Fast Downward Planner
	
--pddl_domain_file_path PATH (required)
	Full or relative path to pddl domain file
	
--pddl_domain_instance_file_path PATH (required)
	Full or relative path to domain instance pddl file
	
--step_limit NUMBER (default=100)
	Max count of steps in plan, writes no plan if exceeded
	
--output_folder_path PATH (default="")
	Full or relative path to output folder
	
--output_file_name NAME (default="")
	Name of output file
	
--fastdownward_pruning BOOLEAN (default=False)
	Whether space should be pruned (see Fast Downward’s option –translate-options –keep-unimportant-variables)
	
--info BOOLEAN (default=True)
	Whether print out information
	
--clean_up BOOLEAN (default=True)
	Whether clean up all temporary files
```

Example command run of **predict** mode (minimal required args):
```
python3 Program/main_terminal_gnn.py
--mode predict
--model_type max
--model_file_path ReferenceModels/Logistics/Max/Version_0/Checkpoints/epoch=93-step=180197.ckpt
--fastdownward_folder_path FastDownwardPlanner
--pddl_domain_file_path Data/Pddl/Logistics/Train/domain.pddl
--pddl_domain_instance_file_path Data/Pddl/Logistics/Test/logistics_37_5.pddl
```

### Args passed through json file config
#### Usage of **train** mode:

```
python3 Program/main_gnn.py --config Program/train_config.json
```

Default **train** mode json
```
{
	"mode": "train",
	"model_type": "max",
	"problem_name": "BlocksClear",
	"train_dataset_folder_path": "Data/Json/BlocksClear/Train",
	"validation_dataset_folder_path": "Data/Json/BlocksClear/Validation",
	"hidden_size": 32,
	"iterations": 30,
	"batch_size": 16,
	"accelerator": "cpu",
	"devices": 1,
	"max_epochs": 1000,
	"learning_rate": 0.0002,
	"l1_factor": 0.0001,
	"weight_decay": 0.0,
	"gradient_accumulation": 1,
	"max_samples_per_value": 100,
	"patience": 200,
	"gradient_clip": 0.01,
	"profiler": null,
	"loader_num_workers": 0,
	"info": true
}
```

#### Usage of **resume** mode:
```
python3 Program/main_gnn.py --config Program/resume_config.json
```

Default **resume** mode json
```
{
	"mode": "resume",
	"model_type": "max",
	"model_file_path": "TrainedModels/BlocksClear/Max/Version_0/Checkpoints/epoch=169validation_loss=0.035.ckpt",
	"problem_name": "BlocksClear",
	"train_dataset_folder_path": "Data/Json/BlocksClear/Train",
	"validation_dataset_folder_path": "Data/Json/BlocksClear/Validation",
	"batch_size": 16,
	"accelerator": "cpu",
	"devices": 1,
	"max_epochs": 1000,
	"gradient_accumulation": 1,
	"max_samples_per_value": 100,
	"patience": 200,
	"gradient_clip": 0.01,
	"profiler": null,
	"loader_num_workers": 0,
	"info": true
}
```

#### Usage of **test** mode:
```
python3 Program/main_gnn.py --config Program/test_config.json
```

Default **test** mode json
```
{
	"mode": "test",
	"model_type": "max",
	"model_file_path": "TrainedModels/BlocksClear/Max/Version_0/Checkpoints/epoch=169-validation_loss=0.035.ckpt",
	"test_dataset_folder_path": "Data/Json/BlocksClear/Test",
	"batch_size": 16,
	"accelerator": "cpu",
	"devices": 1,
	"max_samples_per_value": 100,
	"loader_num_workers": 0,
	"info": true
}
```

#### Usage of **predict** mode:
```
python3 Program/main_gnn.py --config Program/predict_config.json
```

Default **predict** mode json
```
{
	"mode": "predict",
	"model_type": "max",
	"model_file_path": "ReferenceModels/Logistics/Max/Version_0/Checkpoints/epoch=93-step=180197.ckpt",
	"fastdownward_folder_path": "FastDownwardPlanner",
	"pddl_domain_file_path": "Data/Pddl/Logistics/Train/domain.pddl",
	"pddl_domain_instance_file_path": "Data/Pddl/Logistics/Test/logistics_37_5.pddl",
	"step_limit": 100,
	"output_folder_path": "",
	"output_file_name": "",
	"fastdownward_pruning": false,
	"info": true,
	"clean_up": true
}
```
