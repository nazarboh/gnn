# Data Generator for GNN
This script generates GNN (Graph Neural Network) input files (main_gnn_data_generator.py and main_terminal_gnn_data_generator.py).

## Table of Contents
1. [Technologies](#technologies-and-packages)
2. [GitLab](#gitlab)
3. [Usage](#collaboration)

## Technologies And Packages
* Python [https://www.python.org/downloads/](https://www.python.org/downloads/)
* Compiled Fast Downward Planner on your platform [https://www.fast-downward.org/](https://www.fast-downward.org/)
*  ```frozendict``` package is required. See info [https://pypi.org/project/frozendict/](https://pypi.org/project/frozendict/).

## GitLab
Find the project [https://gitlab.fel.cvut.cz/nazarboh/gnn](https://gitlab.fel.cvut.cz/nazarboh/gnn).

## Usage
### Args passed through one by one
#### Usage of **single** mode:
To get help use:
```
Program/main_terminal_gnn_data_generator.py --mode single --help
```

Arguments of **single** mode:
```
--mode MODE (required: single)
	Generator mode: single or multiple
	
--fastdownward_folder_path PATH (required)
	Full or relative path to folder with compiled Fast Downward Planner
	
--pddl_domain_file_path PATH (required)
	Full or relative path to pddl domain file
	
--pdll_domain_instance_file_path PATH (required)
	Full or relative path to pddl domain instance file
	
--output_folder_path PATH (default=.)
	Full or relative path to output folder
	
--output_file_name NAME (default=[pddl_file_name])
	Name of output file
	
--fastdownward_pruning BOOLEAN (default=FALSE)
	Whether space should be pruned (see Fast Downward’s option --translate-options --keep-unimportant-variables)
	
--timer NUMBER (default=10) (in minutes)
	Timer (in minutes) per problem
	
--count NUMBER (default=20000)
	Max count of cost state pairs per problem
	
--length NUMBER (default=100)
	Max length of random walk per problem
	
--patience NUMBER (default=100)
	Max count of tries without discovering a new cost state pair per problem
	
--info BOLLEAN (default=TRUE)
	Whether print out information
	
--clean_up BOOLEAN (default=TRUE)
	Whether clean up all temporary files
```

Example command run of **single** mode (minimal required args):
```
python3 Program/main_terminal_gnn_data_generator.py
--mode single
--fastdownward_folder_path FastDownwardPlanner/
--pddl_domain_file_path Data/Pddl/BlocksClear/Train/domain.pddl
--pddl_domain_instance_file_path Data/Pddl/BlocksClear/Train/blocks_clear_2_0.pddl
```

#### Usage of **multiple** mode:
To get help use:
```
Program/main_terminal_gnn_data_generator.py --mode multiple --help
```

Arguments of **multiple** mode
```
--mode MODE (required: multiple)
	Generator mode: single or multiple
	
--fastdownward_folder_path PATH (required)
	Full or relative path to folder with compiled Fast Downward Planner
	
--pddl_folder_path PATH (required)
	Full or relative path to folder with pddl domain file and pddl domain instance files (domain should be called "domain.pddl")
	
--output_folder_path PATH (default=Output)
	Full or relative path to output folder
	
--fastdownward_pruning BOOLEAN (default=FALSE)
	Whether space should be pruned (see Fast Downward’s option --translate-options --keep-unimportant-variables)
	
--timer NUMBER (default=10) (in minutes)
	Timer (in minutes) per problem
	
--count NUMBER (default=20000)
	Max count of cost state pairs per problem
	
--length NUMBER (default=100)
	Max length of random walk per problem
	
--patience NUBER (default=100)
	Max count of tries without discovering a new cost state pair per problem
	
--info BOOLEAN (default=TRUE)
	Whether print out information

--clean_up BOOLEAN (default=TRUE)
	Whether clean up all temporary files
```

Example command run of **multiple** mode (minimal required args):
```
python3 Program/main_terminal_gnn_data_generator.py
--mode multiple
--fastdownward_folder_path FastDownwardPlanner/
--pddl_folder_path Data/Pddl/BlocksClear/Train
```

### Args passed through json file config
#### Usage of **single** mode:
```
python3 Program/main_gnn_data_generator.py --config Program/single_config.json
```

Default **single** mode json
```
{
	"mode": "single",
	"fastdownward_folder_path": "FastDownwardPlanner",
	"pddl_domain_file_path": "Data/Pddl/BlocksClear/Train/domain.pddl",
	"pddl_domain_instance_file_path": "Data/Pddl/BlocksClear/Train/blocks_clear_2_0.pddl",
	"output_folder_path": "",
	"output_file_name": "",
	"fastdownward_pruning": false,
	"timer": 10,
	"count": 20000,
	"length": 100,
	"patience": 100,
	"info": true,
	"clean_up": true
}
```

#### Usage of **multiple** mode:
```
python3 Program/main_gnn_data_generator.py --config Program/multiple_config.json
```

Default **single** mode json
```
{
	"mode": "multiple",
	"fastdownward_folder_path": "FastDownwardPlanner",
	"pddl_folder_path": "Data/Pddl/Transport/Train",
	"output_folder_path": "Output",
	"fastdownward_pruning": false,
	"timer": 10,
	"count": 20000,
	"length": 100,
	"patience": 100,
	"info": true,
	"clean_up": true
}
```
