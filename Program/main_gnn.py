
from Code.gnn import Gnn
from Code.utils import parse_arguments, read_config_json
from Code.configs import TrainConfig, ResumeConfig, TestConfig, PredictConfig


if __name__ == "__main__":
    args = parse_arguments()
    config = read_config_json(args.config)

    gnn = Gnn()
    if config.mode == 'train':
        config = TrainConfig(**config.__dict__)
        gnn.train(config)
    elif config.mode == 'resume':
        config = ResumeConfig(**config.__dict__)
        gnn.resume(config)
    elif config.mode == 'test':
        config = TestConfig(**config.__dict__)
        gnn.test(config)
    elif config.mode == 'predict':
        config = PredictConfig(**config.__dict__)
        gnn.predict(config)
    else:
        raise ValueError('Unknown type of mode, exiting...')