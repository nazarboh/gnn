
from Code.gnn_data_generator import GnnDataGenerator
from Code.utils import seconds2hours_minutes_seconds, str2bool
from Code.configs import SingleConfig, MultipleConfig

import argparse
from pathlib import Path
import time


def parse_arguments():
    # Create starting parser to choose the mode of generator
    mode_argument_parser = argparse.ArgumentParser(add_help=False)
    mode_argument_parser.add_argument('--mode', required=True, type=str, help='Generator mode: single or multiple')
    mode_argument, _ = mode_argument_parser.parse_known_args()

    # Create the main parser with the knowledge of choosen mode
    parser = argparse.ArgumentParser(parents=[mode_argument_parser])
    parser.add_argument('--fastdownward_folder_path', required=True, type=Path, help='Full or relative path to folder with the Fast Downward planner')
    
    # Different additional required argument for modes
    if mode_argument.mode == 'single':
        parser.add_argument('--pddl_domain_file_path', required=True, type=Path, help='Full or relative path to domain pddl file')
        parser.add_argument('--pddl_domain_instance_file_path', required=True, type=Path, help='Full or relative path to domain instance pddl file')
        parser.add_argument('--output_folder_path', default='', type=Path, help='Full or relative path to output folder (default is active folder in terminal)')
        parser.add_argument('--output_file_name', default='', type=str, help='Name of output file (default is "name of pddl instance file")')
    elif mode_argument.mode == 'multiple':
        parser.add_argument('--pddl_folder_path', required=True, type=Path, help='Full or relative path to folder with domain pddl file and domain instance pddl files')
        parser.add_argument('--output_folder_path', default="Output", type=Path, help='Full or relative path to output folder (default is folder Output)')
    else:
        raise TypeError("Unknown type of generator mode")

    # Optinal arguments for both modes
    parser.add_argument('--fastdownward_pruning', default=False, type=str2bool, help='Whether space should be pruned (default is False)')
    parser.add_argument('--timer', default=10, type=int, help='Timer (in minutes) per problem (default is 10 minutes)')
    parser.add_argument('--count', default=20000, type=int, help='Max count of cost state pairs per problem (default is 20000 cost state pairs)')
    parser.add_argument('--length', default=100, type=int, help='Max length of random walk per problem (default is 100 length walk)')
    parser.add_argument('--patience', default=100, type=int, help='Max count of tries without discovering a new cost state pair per problem (default is 100 tries)')
    parser.add_argument('--info', default=True, type=str2bool, help='Whether print info (default is True)')
    parser.add_argument('--clean_up', default=True, type=str2bool, help='Whether clean up all temporary files (default is True)')

    arguments = parser.parse_args()

    # convert minutes into seconds
    arguments.timer *= 60 # TODO uncomment this

    return arguments


def print_arguments(args):
    print()
    print('Generator parameters:')

    print(f'--mode = {args.mode}')
    print(f'--fastdownward_folder_path = {args.fastdownward_folder_path}')
    
    if args.mode == 'single':
        print(f'--pddl_domain_file_path = {args.pddl_domain_file_path}')
        print(f'--pddl_domain_instance_file_path = {args.pddl_domain_instance_file_path}')
        print(f'--output_folder_path = {args.output_folder_path}')
        print(f'--output_file_name = {("(pddl instance file name)_gnn_data.txt" if args.output_file_name == "" else args.output_file_name)}')

    if args.mode == 'multiple':
        print(f'--pddl_folder_path = {args.pddl_folder_path}')
        print(f'--output_folder_path = {args.output_folder_path}')

    print(f'--fastdownward_pruning = {args.fastdownward_pruning}')
    print(f'--timer = {seconds2hours_minutes_seconds(args.timer)}')
    print(f'--count = {args.count}')
    print(f'--length = {args.length}')
    print(f'--patience = {args.patience}')
    print(f'--info = {args.info}')
    print(f'--clean_up = {args.clean_up}')
    print()


if __name__ == "__main__":
    args = parse_arguments()
    if args.info: print_arguments(args)

    # Start time
    tic = time.perf_counter()

    # Modes of generator
    generator = GnnDataGenerator()
    if args.mode == 'single':
        generator.single(SingleConfig(**args.__dict__))
    elif args.mode == 'multiple':
        generator.multiple(MultipleConfig(**args.__dict__))
    else:
        raise ValueError('Unknown mode of generator')

    # End time
    toc = time.perf_counter()
    print(f"Generated the data in {seconds2hours_minutes_seconds(toc-tic)}.")