
import torch
import pytorch_lightning as pl
from typing import List, Dict, Tuple

from .sas_data import SasData
from .pddl_data import PddlData
from .gnn_data_adapter import GnnDataAdapter
from .gnn_dataset_entry_adapter import GnnDatasetEntryAdapter
from .gnn_dataset import collate_gnn_dataset_state, collate_batch_of_gnn_dataset_states
from .gnn_data import GnnData

from .sas_file_structs import *


class SasStateCostPredictor:
    model: pl.LightningModule
    gnn_data_adapter: GnnDataAdapter
    gnn_dataset_entry_adapter: GnnDatasetEntryAdapter
    sas_data: SasData
    pddl_data: PddlData

    gnn_data_predicates: Dict[int, str]
    gnn_data_objects: Dict[int, str]
    gnn_data_facts: List[Tuple[int, List[int]]]
    gnn_data_goals: List[Tuple[int, List[int]]]

    goal_predicate_offset: int
    gnn_dataset_entry_facts: List[Tuple[int, List[int]]]
    gnn_dataset_entry_goals: List[Tuple[int, List[int]]]

    def __init__(
        self, 
        model: pl.LightningModule,
        gnn_data_adapter: GnnDataAdapter,
        gnn_dataset_entry_adapter: GnnDatasetEntryAdapter,
        sas_data: SasData,
        pddl_data: PddlData,

    ) -> None:
        self.model = model
        self.gnn_data_adapter = gnn_data_adapter
        self.gnn_dataset_entry_adapter = gnn_dataset_entry_adapter
        self.sas_data = sas_data
        self.pddl_data = pddl_data

        self.init()


    def init(self) -> None:
        # Init, calculated only once: gnn data predicates, objects, facts and goals
        self.gnn_data_predicates = self.gnn_data_adapter.adapt_predicates(self.pddl_data.predicates)
        self.gnn_data_objects = self.gnn_data_adapter.adapt_objects(self.pddl_data.objects)
        self.gnn_data_facts = self.gnn_data_adapter.adapt_facts(self.gnn_data_predicates, self.gnn_data_objects, self.pddl_data.init_predicate_states, self.pddl_data.effect_predicates)
        self.gnn_data_goals = self.gnn_data_adapter.adapt_goals(self.gnn_data_predicates, self.gnn_data_objects, self.sas_data.variables, self.sas_data.goal_state)

        # Init, calculated only once: offset, gnn dataset entry facts and goals
        self.goal_predicate_offset = self.gnn_dataset_entry_adapter.get_goal_predicate_offset(self.gnn_data_predicates.keys())
        self.gnn_dataset_entry_facts = self.gnn_dataset_entry_adapter.adapt_facts(self.gnn_data_facts)
        self.gnn_dataset_entry_goals = self.gnn_dataset_entry_adapter.adapt_goals(self.gnn_data_goals, self.goal_predicate_offset)


    def predict_cost(self, sas_state: Dict[int, VariableState]) -> float:
        # Convert to SAS State into GNN State
        gnn_data_state = self.gnn_data_adapter.adapt_state(self.gnn_data_predicates, self.gnn_data_objects, self.sas_data.variables, sas_state)
        
        # Convert to GNN State into GNN Dataset State
        gnn_dataset_entry_state = self.gnn_dataset_entry_adapter.adapt_state(self.gnn_dataset_entry_facts, self.gnn_dataset_entry_goals, gnn_data_state)

        # Convert GNN Dataset State into Collated State
        input_state = collate_gnn_dataset_state(gnn_dataset_entry_state)

        cost_tensor = self.model.forward(input_state)

        return torch.squeeze(cost_tensor).item()
    

    def predict_costs(self, sas_states: List[Dict[int, VariableState]]) -> List[float]:
        # Convert to SAS States into GNN States, then into GNN Dataset States
        gnn_dataset_entry_states = map(lambda x: self.gnn_dataset_entry_adapter.adapt_state(self.gnn_dataset_entry_facts, self.gnn_dataset_entry_goals, self.gnn_data_adapter.adapt_state(self.gnn_data_predicates, self.gnn_data_objects, self.sas_data.variables, x)), sas_states)
        
        # Convert GNN Dataset States into Collated State
        input_states = collate_batch_of_gnn_dataset_states(gnn_dataset_entry_states)

        costs_tensor = self.model.forward(input_states)

        return torch.squeeze(costs_tensor).tolist()
