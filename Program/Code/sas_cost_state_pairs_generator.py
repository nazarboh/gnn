
from .utils import seconds2hours_minutes_seconds
from .sas_file_structs import *
from .sas_data import SasData

import random
import os
import time
from pathlib import Path
from frozendict import frozendict
from typing import Tuple, Dict, List, Union
import uuid
import subprocess

'''
Generates cost state pairs from sas parsed info using the fastdownward planner
'''
class SasCostStatePairsGenerator:
    fastdownward_folder_path: Path
    output_file_name: str
    timer: int
    count: int
    length: int
    patience: int
    info: bool
    unique_number: uuid.UUID

    def __init__(self, fastdownward_folder_path: Path, output_file_name: str, timer: int, count: int, length: int, patience: int, info: bool, unique_number: uuid.UUID):
        self.fastdownward_folder_path = Path(fastdownward_folder_path)
        self.output_file_name = str(output_file_name)
        self.timer = int(timer)
        self.count = int(count)
        self.length = int(length)
        self.patience = int(patience)
        self.info = bool(info)
        self.unique_number = unique_number

    '''
    Create sas state (sas state is a dictionary of VariableStates, describes state of atom in every variable) using random walk
    '''
    def random_walk(self, sas_operators: List[Operator], sas_init_state: Dict[int, VariableState]) -> Dict[int, VariableState]:
        # Init
        #iterations = random.randrange(int(self.length / 2), self.length)
        iterations = random.randrange(1, self.length)
        state = sas_init_state

        # # Go random way 'iterations' times
        # for i in range(iterations):

        #     # Find all unique neighbours
        #     neighbour_states = []
        #     for operator in sas_operators:
        #         neighbour_state = operator.apply(state)

        #         if neighbour_state != None:
        #             neighbour_states.append(neighbour_state)

        #     # no neighbours found
        #     if len(neighbour_states) == 0:
        #         break

        #     # Get index of random neighbour
        #     neighbour_index = random.randrange(len(neighbour_states))

        #     # Update current state
        #     state = neighbour_states[neighbour_index]


        # Go random way 'iterations' times
        for i in range(iterations):

            neighbour_state = None
            for i in range(self.patience):

                # Get index of random operator
                neighbour_sas_operator_index = random.randrange(len(sas_operators))
                neighbour_state = sas_operators[neighbour_sas_operator_index].apply(state)

                if neighbour_state != None:
                    break

            # Update current state
            if neighbour_state != None:
                state = neighbour_state

        return state


    '''
    Splits file content into 3 parts using two indices
    '''
    def read_split_sas_file(self, file: Path, first_index: int, second_index: int) -> Tuple[List[str], List[str], List[str]]:
        with file.open('r') as fs: lines = [line.strip() for line in fs.readlines()]
        
        # Split the content of the sas file
        before = lines[ : first_index]
        between = lines[first_index : second_index + 1]
        after = lines[second_index + 1 : ]

        return before, between, after
    

    '''
    Converts sas state (sas state is a dictionary of VariableStates, describes state of atom in every variable) into string init state representation in the file
    '''
    def create_sas_init_state(self, init_state: Dict[int, VariableState]) -> List[str]:
        sas_init_state = []

        # Add start key word
        sas_init_state.append('begin_state')

        # Add every variable state
        for variable_index, variable_state in init_state.items():
            sas_init_state.append(str(variable_state.atom_index))

        # Add end key word
        sas_init_state.append('end_state')

        return sas_init_state


    '''
    Finds operator in operators list using parsed info in provided line
    '''
    def find_operator(self, operators: List[Operator], line: str) -> Union[Operator, None]:
        line_splited = line.replace('(', '').replace(')', ' ').split()
        operator_name = line_splited[0]
        objects = line_splited[1:]

        for operator in operators:
            if operator.operator_name == operator_name and operator.objects == objects:
                return operator

        return None


    '''
    Parses the plan provided by the planner, creates a dictionary of sas states as a key and their cost as value, sas stete itself is a dictionary so that converted into frozndict to stop mutability
    '''
    def parse_plan(self, file: Path, sas_data: SasData, sas_init_state: Dict[int, VariableState]) -> Dict[Dict[int, VariableState], float]:
        with file.open('r') as fs: lines = [line.strip() for line in fs.readlines()]

        new_state_cost_dictionary : Dict[Dict[int, VariableState], float] = {}

        # Choose the last line
        cost_line = lines[-1]
        
        # Just get the number
        cost = float(cost_line.replace('; cost = ', '').replace(' (unit cost)', ''))

        # Init
        state = sas_init_state
        new_state_cost_dictionary[frozendict(state)] = cost

        # Line for line parses plan and applies it, always decreases the cost by one 
        for i in range(len(lines) - 1):
            operator = self.find_operator(sas_data.operators, lines[i])

            state = operator.apply(state)
            cost -= operator.cost
            new_state_cost_dictionary[frozendict(state)] = cost
        
        assert(int(cost) == 0)

        return new_state_cost_dictionary


    '''
    Creates new sas file with given new init state and runs the planner on it then creates a dictionary of sas states as a key and their cost as value
    '''
    def get_state_cost_dictionary_using_planner(self, sas_init_state: Dict[int, VariableState], sas_data: SasData) -> Dict[Dict[int, VariableState], float]:
        # Create a new input file for the planner
        init_section_start_index, init_section_end_index = sas_data.section_indices['init_state']
        before_init_state_lines, init_state_lines, after_init_state_lines = self.read_split_sas_file(sas_data.file_path, init_section_start_index, init_section_end_index)

        # Create goal state 
        new_init_state_lines = self.create_sas_init_state(sas_init_state)

        # Create content of input file for the planner
        sas_file_lines = '\n'.join(before_init_state_lines + new_init_state_lines + after_init_state_lines)

        # Create input file for the planner
        with open(f'{self.output_file_name}_{self.unique_number}_modified_output.sas', 'w') as f: f.write(sas_file_lines)

        # Run the planner
        with open(f'{self.output_file_name}_{self.unique_number}_modified_fast_downward_log.txt', mode='w') as file:
            subprocess.run(f'python {os.path.join(os.path.abspath(self.fastdownward_folder_path), "fast-downward.py")} --plan-file {self.output_file_name}_{self.unique_number}_modified_sas_plan {self.output_file_name}_{self.unique_number}_modified_output.sas --search "astar(lmcut())"', stdout=file, stderr=file, shell=True)

        new_state_cost_dictionary = None

        # exists if it is not a deadend
        if os.path.exists(Path(f'{self.output_file_name}_{self.unique_number}_modified_sas_plan')):
            # Get states with costs from the parser
            new_state_cost_dictionary = self.parse_plan(Path(f'{self.output_file_name}_{self.unique_number}_modified_sas_plan'), sas_data, sas_init_state)

        return new_state_cost_dictionary


    '''
    Creates up to 'self.count' unique sas states using random walk
    '''
    def n_random_walk(self, sas_data: SasData) -> Dict[Dict[int, VariableState], float]:
        patience_left = self.patience

        # Include Optimal path
        state_cost_dictionary = self.get_state_cost_dictionary_using_planner(sas_data.init_state, sas_data)


        # deadend immediately at the init position
        if state_cost_dictionary == None:
            return {frozendict(sas_data.init_state): 999999999.9}


        # dummy instance - no actions available, goal state from the begining, Fast Downward pruned variables and created 'dummy' predicate
        if len (sas_data.operators) == 0:
            return None


        # Set up breaking timer
        tic = time.perf_counter()


        # Try to find 'count' times unique states, after 'patience' times without update -> leave
        while True:
            toc = time.perf_counter()

            if self.info and len(state_cost_dictionary) % 1000 == 0:
                print('   ' + f'Created {len(state_cost_dictionary)} state cost pairs in {seconds2hours_minutes_seconds(toc-tic)}, continuing...') 

            # Timer is off or no changes (no new states found) or reached 'count' of states
            if (toc - tic) >= self.timer or patience_left == 0 or len(state_cost_dictionary) >= self.count:
                break

            # Go randomly to some state from randomly chosen already known state
            random_sas_init_state = dict(random.choice(list(state_cost_dictionary.keys())))
            random_sas_state = frozendict(self.random_walk(sas_data.operators, random_sas_init_state))


            # The new random state is already in the states, so the planner already created path with costs
            if random_sas_state in state_cost_dictionary.keys():
                patience_left -= 1
                continue


            # Find states with costs using the planner
            new_state_cost_dictionary = self.get_state_cost_dictionary_using_planner(random_sas_state, sas_data)
            
            # deadend
            if new_state_cost_dictionary == None:
                patience_left -= 1
                continue

            # Add new unique states to states dict
            isAtLeastOneNewStateFound = False
            for new_sas_state, new_cost in new_state_cost_dictionary.items():
                if new_sas_state not in state_cost_dictionary.keys():
                    state_cost_dictionary[new_sas_state] = new_cost
                    patience_left = self.patience
                    isAtLeastOneNewStateFound = True


            # Control if any new state have been added to states dict
            if isAtLeastOneNewStateFound:
                patience_left = self.patience
            else:
                patience_left -= 1


        return state_cost_dictionary

    
    '''
    Generates banch of unique sas states with their true cost pairs
    '''
    def generate(self, sas_data: SasData) -> List[Tuple[float, Dict[int, VariableState]]]:
        state_cost_dictionary = self.n_random_walk(sas_data)

        if state_cost_dictionary != None:
            cost_state_list = [(cost, sas_state) for sas_state, cost in state_cost_dictionary.items()]
            return cost_state_list
