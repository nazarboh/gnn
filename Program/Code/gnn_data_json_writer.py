
from .gnn_data import GnnData

from pathlib import Path
import json
import os


class GnnDataJsonWriter:
    def __init__(self) -> None:
        pass

    def write(self, gnn_data: GnnData, output_folder_path: Path, output_file_name: str) -> None:
        output_file_name = output_file_name + '.json'

        json_string = json.dumps(gnn_data.__dict__)
        path = os.path.abspath(os.path.join(output_folder_path, output_file_name))
        os.makedirs(os.path.dirname(path), exist_ok=True)

        with open(path, 'w') as f:
            f.write(json_string)
