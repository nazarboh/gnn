
from .gnn_data import GnnData

from pathlib import Path

class GnnDataReader:
    def read(self, gnn_data_file_path: Path) -> GnnData:
        raise NotImplementedError
