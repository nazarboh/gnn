
from pathlib import Path
import os
from typing import List, Dict, Tuple

from .sas_file_structs import *
from .sas_plan_data import SasPlanData

class SasPlanDataWriter:
    def __init__(self) -> None:
        pass

    def write_plan(self, lines: List[str], plan: Dict[Dict[int, VariableState], Tuple[float, float, Dict[int, VariableState], str, List[str]]]) -> List[str]:
        total_true_cost = 0
        for sas_state, (neighbor_predicted_cost, neighbor_operator_cost, neighbor_sas_state, neighbor_operator_name, neighbor_operator_objects) in plan.items():
            total_true_cost += neighbor_operator_cost

            instruction_string = '(' + neighbor_operator_name + ' ' + ' '.join(neighbor_operator_objects) + ')'
            lines.append(instruction_string)
            #lines.insert(0, instruction_string)

        cost_string = '; cost = ' + str(int(total_true_cost)) + ' (unit cost)'
        
        lines.append(cost_string)
        
        return lines


    def write(self, sas_plan_data: SasPlanData, output_folder_path: Path, output_file_name: str) -> None:
        #output_file_name = output_file_name + '.txt'

        lines = []

        lines = self.write_plan(lines, sas_plan_data.plan)

        path = os.path.abspath(f'{output_folder_path}/{output_file_name}')

        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, 'w') as f:
            for line in lines:
                f.write(f"{line}\n")