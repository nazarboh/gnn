
import pytorch_lightning as pl

from torch.functional import Tensor
import torch
from typing import List, Dict, Tuple
from collections import OrderedDict
from frozendict import frozendict

from .sas_data import SasData
from .pddl_data import PddlData
from .gnn_data_adapter import GnnDataAdapter
from .gnn_dataset_entry_adapter import GnnDatasetEntryAdapter
from .gnn_data import GnnData
from .sas_file_structs import *
from .sas_plan_data import SasPlanData
from .sas_state_cost_predictor import SasStateCostPredictor


class SasPlanPredictor:
    def __init__(self) -> None:
        pass

    
    def get_all_neighbor_info_of_sas_state(self, sas_state_cost_predictor: SasStateCostPredictor, sas_operators: List[Operator], sas_state: Dict[int, VariableState], ) -> List[Tuple[float, float, Dict[int, VariableState], str, List[int]]]:
        neighbour_sas_states_info: List[Tuple[float, float, Dict[int, VariableState], str, List[int]]] = []
        neighbours: List[Dict[int, VariableState]] = []

        # Find all valid neighbors states and their info
        for operator in sas_operators:
            neighbour_sas_state = operator.apply(sas_state)

            if neighbour_sas_state != None:
                neighbours.append(neighbour_sas_state)
                neighbour_sas_states_info.append((operator.cost, neighbour_sas_state, operator.operator_name, operator.objects))

        predicted_costs = sas_state_cost_predictor.predict_costs(neighbours)

        # Merge predicted costs of neighbors into their information
        neighbour_sas_states_info = [(a, *b) for a, b in zip(predicted_costs, neighbour_sas_states_info)]

        return neighbour_sas_states_info
    

    def is_goal_sas_state(self, sas_goal_state: Dict[int, VariableState], sas_state: Dict[int, VariableState]) -> bool:
        for goal_variable_index, goal_variable_state in sas_goal_state.items():
            if goal_variable_index not in sas_state.keys() or goal_variable_state != sas_state[goal_variable_index]:
                return False

        return True


    def predict(self, model: pl.LightningModule, sas_data: SasData, pddl_data: PddlData, step_limit: int, info: bool) -> SasPlanData:
        gnn_data_adapter = GnnDataAdapter()
        gnn_dataset_entry_adapter = GnnDatasetEntryAdapter()
        sas_state_cost_predictor = SasStateCostPredictor(model, gnn_data_adapter, gnn_dataset_entry_adapter, sas_data, pddl_data)

        plan: Dict[Dict[int, VariableState], Tuple[float, float, Dict[int, VariableState], str, List[str]]] = OrderedDict()

        sas_state = frozendict(sas_data.init_state)
        visited: Dict[Dict[int, VariableState]] = {}

        if info: print(f'      {len(sas_data. operators)} SAS operators')

        step = 0
        total_true_cost = 0
        while True:
            # is finished
            if self.is_goal_sas_state(sas_data.goal_state, sas_state):
                if info: print('      ; cost = ' + str(int(total_true_cost)) + ' (unit cost)')
                return SasPlanData(plan)
            
            # Step limit
            if step >= step_limit:
                if info: print('     Step limit')
                return SasPlanData(None)

            # Get all neighbors
            neighbour_sas_states_info = self.get_all_neighbor_info_of_sas_state(sas_state_cost_predictor, sas_data.operators, sas_state)

            # No neighbors found - deadend
            if len(neighbour_sas_states_info) == 0:
                if info: print('     No neighbors found - deadend')
                return SasPlanData(None)
            
            # Sort neighbor states
            neighbour_sas_states_info = sorted(neighbour_sas_states_info, key = lambda x: x[0])

            visited[frozendict(sas_state)] = True
            foundNeighbor = False

            for neighbor_predicted_cost, neighbor_operator_cost, neighbor_sas_state, neighbor_operator_name, neighbor_operator_objects in neighbour_sas_states_info:
                # Go next lowest neighbor cost direction
                if not visited.get(frozendict(neighbor_sas_state), False):
                    if info: print(f'   {step + 1}. ({neighbor_operator_name} {" ".join(neighbor_operator_objects)}) of predicted cost {neighbor_predicted_cost:0.2f} (lowest out of {len(neighbour_sas_states_info)} neighbors)')
                    total_true_cost += neighbor_operator_cost
                    
                    plan[frozendict(sas_state)] = (neighbor_predicted_cost, neighbor_operator_cost, neighbor_sas_state, neighbor_operator_name, neighbor_operator_objects)
                    
                    sas_state = neighbor_sas_state
                    step += 1
                    foundNeighbor = True
                    break
            
            if foundNeighbor:
                continue

            # No not visited neighbors found - deadend
            break
        
        if info: print('     All neighbors of current sas state visited - deadend')
        return SasPlanData(None)
    