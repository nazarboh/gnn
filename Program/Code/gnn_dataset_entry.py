
from typing import List, Dict, Tuple
from torch.functional import Tensor

class GnnDatasetEntry:
    predicate_arity_tuples: List[Tuple[int, int]]
    state_cost_pairs: List[Tuple[Dict[int, Tensor], Tensor]]
    
    def __init__(self, predicate_ariry_tuples: List[Tuple[int, int]], state_cost_pairs: List[Tuple[Dict[int, Tensor], Tensor]]) -> None:
        self.predicate_arity_tuples = predicate_ariry_tuples
        self.state_cost_pairs = state_cost_pairs