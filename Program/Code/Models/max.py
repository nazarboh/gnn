import torch

import torch.nn as nn
import pytorch_lightning as pl
from torch.nn import functional as F
from torch.nn.functional import Tensor

from typing import List, Dict, Tuple


class RelationMessagePassing(pl.LightningModule):
    def __init__(self, relation_arity_tuples: List[Tuple[int, int]], hidden_size: int):
        super().__init__()
        
        self.hidden_size = hidden_size
        self.relation_modules = nn.ModuleList()

        for relation, arity in relation_arity_tuples:
            # assert relation == len(self.relation_modules) what is even this?

            input_size = arity * hidden_size
            output_size = arity * hidden_size

            if (input_size > 0) and (output_size > 0):
                mlp = nn.Sequential(nn.Linear(input_size, input_size, True), nn.ReLU(), nn.Linear(input_size, output_size, True))
            else:
                mlp = None

            self.relation_modules.append(mlp)

        self.update = nn.Sequential(nn.Linear(2 * hidden_size, 2 * hidden_size, True), nn.ReLU(), nn.Linear(2 * hidden_size, hidden_size, True))


    def forward(self, node_states: Tensor, relations: Dict[int, Tensor]) -> Tensor:
        max_outputs = []
        outputs = []

        for relation, module in enumerate(self.relation_modules):
            if (module is not None) and (relation in relations):
                values = relations[relation]
                input = torch.index_select(node_states, 0, values).view(-1, module[0].in_features)
                output = module(input).view(-1, self.hidden_size)
                max_outputs.append(torch.max(output))
                node_indices = values.view(-1, 1).repeat(1, self.hidden_size)
                outputs.append((output, node_indices))

        max_offset = torch.max(torch.stack(max_outputs))
        exps_sum = torch.full_like(node_states, 1E-16, device=self.device)

        for output, node_indices in outputs:
            exps = torch.exp(8.0 * (output - max_offset))
            exps_sum = torch.scatter_add(exps_sum, 0, node_indices, exps)

        max_message = ((1.0 / 8.0) * torch.log(exps_sum)) + max_offset

        node_states = self.update(torch.cat([max_message, node_states], dim=1))

        return node_states


class Readout(pl.LightningModule):
    def __init__(self, input_size: int, output_size: int, bias: bool = True):
        super().__init__()

        self.pre = nn.Sequential(nn.Linear(input_size, input_size, bias), nn.ReLU(), nn.Linear(input_size, input_size, bias))
        self.post = nn.Sequential(nn.Linear(input_size, input_size, bias), nn.ReLU(), nn.Linear(input_size, output_size, bias))


    def forward(self, relation_sizes: List[int], node_states: Tensor) -> Tensor:
        relation_values: List[Tensor] = []
        offset: int = 0
        node_states: Tensor = self.pre(node_states)

        for relation_size in relation_sizes:
            relation_value = self.post(torch.sum(node_states[offset:(offset + relation_size)], dim=0))

            relation_values.append(relation_value)
            offset += relation_size

        relation_values = torch.stack(relation_values)

        return relation_values


class RelationMessagePassingModel(pl.LightningModule):
    def __init__(self, relation_arity_tuples: List[Tuple[int, int]], hidden_size: int, iterations: int):
        super().__init__()

        self.hidden_size = hidden_size
        self.iterations = iterations
        self.relation_network = RelationMessagePassing(relation_arity_tuples, hidden_size)
        self.readout = Readout(hidden_size, 1)


    def forward(self, input: Tuple[Dict[int, Tensor], List[int]]) -> Tensor:
        relations, relation_sizes = input

        node_states = self.initialize_node_states(sum(relation_sizes))
        for _ in range(self.iterations):
            node_states = self.relation_network(node_states, relations)
        relation_values = self.readout(relation_sizes, node_states)

        return relation_values


    def initialize_node_states(self, relation_sizes_sum: int) -> Tensor:
        init_node_states_zeroes = torch.zeros((relation_sizes_sum, (self.hidden_size // 2) + (self.hidden_size % 2)), dtype=torch.float, device=self.device)
        init_node_states_random = torch.randn((relation_sizes_sum, self.hidden_size // 2), dtype=torch.float, device=self.device)
        init_node_states = torch.cat([init_node_states_zeroes, init_node_states_random], dim=1)

        return init_node_states


class MaxModel(pl.LightningModule):
    def __init__(self, predicates: List[Tuple[int, int]], hidden_size: int, iterations: int, learning_rate: float, l1_factor: float, weight_decay: float): # TODO predicates should be called 'predicate_arity_tuples', can't change because old checkpoints won't be valid anymore
        super().__init__()

        self.save_hyperparameters() # By default, every parameter of the __init__ method will be considered a hyperparameter to the LightningModule, saves into hparams.yaml file into version

        self.model = RelationMessagePassingModel(predicates, hidden_size, iterations)

        self.learning_rate = learning_rate
        self.l1_factor = l1_factor
        self.weight_decay = weight_decay

    def forward(self, states: Tuple[Dict[int, Tensor], List[int]]) -> Tensor:
        return self.model(states)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.learning_rate, weight_decay=self.weight_decay)
        return optimizer

    def training_step(self, train_batch, batch_index):
        states, target = train_batch
        output = self(states)
        loss = torch.mean(torch.abs(torch.sub(target, output)))
        self.log('train_loss', loss)

        if self.l1_factor > 0.0:
            l1_loss = 0.0

            for parameter in self.parameters():
                l1_loss += torch.sum(self.l1_factor * torch.abs(parameter))

            self.log('l1_loss', l1_loss)
            loss += l1_loss
        
        self.log('total_loss', loss)

        return loss

    def validation_step(self, validation_batch, batch_index):
        states, target = validation_batch
        output = self(states)
        loss = F.l1_loss(output, target)
        self.log('validation_loss', loss)

    def test_step(self, test_batch, test_batch_idx):
        states, target = test_batch
        output = self(states)

        return [output, target]

    def test_epoch_end(self, test_step_outputs):
        error_sum = torch.tensor(0)
        target_sum = torch.tensor(0)

        for output, target in test_step_outputs:
            
            error = torch.abs(output - target)
            error_sum = torch.add(error_sum, torch.sum(error))
            target_sum = torch.add(target_sum, target.nelement()) # TODO: target_sum = torch.add(target_sum, torch.sum(target))

        print("{} ({} / {})".format(float(error_sum / target_sum), int(error_sum), int(target_sum)))
