
from .gnn_data_reader import GnnData
from .gnn_data_reader import GnnDataReader
from .gnn_dataset_entry_adapter import GnnDatasetEntryAdapter
from .gnn_dataset_entry import GnnDatasetEntry

import torch
from pathlib import Path
from torch.functional import Tensor
from torch.utils.data.dataset import Dataset
from typing import List, Dict, Tuple
import sys
import copy


class GnnDataset(Dataset):
    dataset_folder_path: Path
    gnn_data_reader: GnnDataReader
    min_cost: float
    max_cost: float
    max_samples_per_value: int
    repeat: int

    predicate_arity_tuples: List[Tuple[int, int]] = None
    state_cost_pairs: Tuple[Dict[int, Tensor], Tensor] = []


    def __init__(self, dataset_folder_path: Path, gnn_data_reader: GnnDataReader, min_cost: float = 0.0, max_cost: float = sys.float_info.max, max_samples_per_value: int = sys.maxsize, repeat: int = 1) -> None:
        self.dataset_folder_path = Path(dataset_folder_path)
        self.gnn_data_reader = gnn_data_reader
        self.min_cost = float(min_cost) 
        self.max_cost = float(max_cost)
        self.max_samples_per_value = int(max_samples_per_value)
        self.repeat = int(repeat)

        self.init_dataset()


    def entry(self, gnn_dataset_entry: GnnDatasetEntry) -> None:
        # Init local dataset predicate arities (first entry ever)
        if self.predicate_arity_tuples == None:
            self.predicate_arity_tuples = copy.deepcopy(gnn_dataset_entry.predicate_arity_tuples)
            self.predicate_arity_tuples.sort()

        # Check that predicate arities from next entry are the same
        gnn_dataset_entry.predicate_arity_tuples.sort()
        if not (self.predicate_arity_tuples == gnn_dataset_entry.predicate_arity_tuples):
            print(self.predicate_arity_tuples)
            print(gnn_dataset_entry.predicate_arity_tuples)
            raise Exception('Gnn dataset entry with different predicate atiry tuples')
        
        # Add state cost pairs from current entry to all state cost pairs
        self.state_cost_pairs += gnn_dataset_entry.state_cost_pairs


    def init_dataset(self) -> None:
        gnn_dataset_entry_adapter = GnnDatasetEntryAdapter()

        for gnn_data_file_path in self.dataset_folder_path.glob('*_gnn_data.json'):
            # Read GnnData from a file with provided reader (Json or Txt reader)
            gnn_data: GnnData = self.gnn_data_reader.read(gnn_data_file_path)
            
            # Transform GnnData into dataset entry 
            gnn_dataset_entry: GnnDatasetEntry = gnn_dataset_entry_adapter.adapt(gnn_data, self.min_cost, self.max_cost, self.max_samples_per_value)
            
            # Add from current entry state cost pairs to overall state cost pairs 
            self.entry(gnn_dataset_entry)

    
    def __len__(self) -> int:
        return len(self.state_cost_pairs) * self.repeat


    def __getitem__(self, idx) -> Tuple[Dict[int, Tensor], Tensor]:
        if not (0 <= idx and idx <= len(self) - 1):
            raise Exception('Invalid index')

        return self.state_cost_pairs[idx % len(self.state_cost_pairs)]


def collate_gnn_dataset_state(state: Dict[int, Tensor]) -> Tuple[Dict[int, Tensor] , List[int]]:
    max_size = 0

    # Find in state's predicates upper bound of object value
    for predicate, values in state.items():
        if values.nelement() > 0:
            max_size = max(max_size, int(torch.max(values)) + 1)
        
        # from torch list of lists of objects make list of objects [[a ...] ... [b ...]] -> [a, ..., ..., b, ...] 
        state[predicate] = torch.reshape(values, shape=(-1,))

    return (state, [max_size])


def collate_batch_of_gnn_dataset_states(states: List[Dict[int, Tensor]]) -> Tuple[Dict[int, Tensor] , List[int]]:
    input_state = {}
    input_sizes = []
    offset = 0

    for state in states:
        # Find in state's predicates upper bound of object value
        (state, [size]) = collate_gnn_dataset_state(state)

        # Aggregate current state with offset shift to input state 
        for predicate, values in state.items():
            if predicate not in input_state: 
                input_state[predicate] = torch.tensor([], dtype=torch.long)
            
            # torch vector append torch vector tensor[a, b], tensor[c, d] -> tensor[a, b, c, d]
            input_state[predicate] = torch.cat([input_state[predicate], values + offset])

        input_sizes.append(size)
        offset += size

    return (input_state, input_sizes)


def collate_batch_of_gnn_dataset_state_cost_pairs(state_cost_pairs: List[Tuple[Dict[int, Tensor], float]]) -> Tuple[Tuple[Dict[int, Tensor] , List[int]], Tensor]:
    # Before [(state, cost)]
    #
    # Example: test_clear_probBLOCKS-2-0_states.txt, batch of 5 state cost pairs from dataset
    #
    # [
    #     (
    #         {
    #             7: tensor([[1]]),
    #             1: tensor([[0],[1]]),
    #             2: tensor([[0],[1]]),
    #             3: tensor([], size=(1, 0), dtype=torch.int64)
    #         },
    #         tensor([0.], dtype=torch.float64)
    #     ),
    #     (
    #         {
    #             7: tensor([[1]]),
    #             4: tensor([[1]]),
    #             1: tensor([[0]]),
    #             2: tensor([[0]])
    #         },
    #         tensor([1.], dtype=torch.float64)
    #     ),
    #     (
    #         {
    #             7: tensor([[1]]),
    #             0: tensor([[1, 0]]),
    #             1: tensor([[0]]),
    #             2: tensor([[1]]),
    #             3: tensor([], size=(1, 0), dtype=torch.int64)
    #         },
    #         tensor([0.], dtype=torch.float64)
    #     ),
    #     (
    #         {
    #             7: tensor([[1]]),
    #             4: tensor([[0]]),
    #             1: tensor([[1]]),
    #             2: tensor([[1]])
    #         },
    #         tensor([0.], dtype=torch.float64)
    #     ),
    #     (
    #         {
    #             7: tensor([[1]]),
    #             0: tensor([[0, 1]]),
    #             1: tensor([[1]]),
    #             2: tensor([[0]]),
    #             3: tensor([], size=(1, 0), dtype=torch.int64)
    #         },
    #         tensor([1.], dtype=torch.float64)
    #     )
    # ]


    input_state = {}
    input_sizes = []
    offset = 0
    target = []

    for state, cost in state_cost_pairs:
        # Find in state's predicates upper bound of object value
        (state, [size]) = collate_gnn_dataset_state(state)

        # Aggregate current state with offset shift to input state 
        for predicate, values in state.items():
            if predicate not in input_state.keys(): 
                input_state[predicate] = torch.tensor([], dtype=torch.long)
            
            # torch vector append torch vector tensor[a, b], tensor[c, d] -> tensor[a, b, c, d]
            input_state[predicate] = torch.cat([input_state[predicate], values + offset])

        # update offset, global aggregated size
        offset += size

        input_sizes.append(size)
        target.append(cost)


    # After ((states, sizes), costs)
    #
    # Example: test_clear_probBLOCKS-2-0_states.txt
    #
    # (
    #     (
    #         {
    #             7: tensor([1, 3, 5, 7, 9]),
    #             1: tensor([0, 1, 2, 4, 7, 9]),
    #             2: tensor([0, 1, 2, 5, 7, 8]),
    #             3: tensor([], dtype=torch.int64),
    #             4: tensor([3, 6]),
    #             0: tensor([5, 4, 8, 9])
    #         },
    #         [2, 2, 2, 2, 2]
    #     ),
    #     tensor([[0.],
    #             [1.],
    #             [0.],
    #             [0.],
    #             [1.]], dtype=torch.float64))

    return ((input_state, input_sizes), torch.stack(target))
