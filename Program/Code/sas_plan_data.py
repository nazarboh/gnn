
from typing import List, Dict, Tuple

from .sas_file_structs import*

class SasPlanData:
    plan: Dict[Dict[int, VariableState], Tuple[float, float, Dict[int, VariableState], str, List[str]]]

    def __init__(self, plan: Dict[Dict[int, VariableState], Tuple[float, float, Dict[int, VariableState], str, List[str]]]) -> None:
        self.plan = plan
