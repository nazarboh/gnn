
from typing import List, Tuple


class PddlData():
    predicates: List[str]
    predicate_arity_tuples: List[Tuple[str, int]]
    effect_predicates: List[str]
    objects: List[str]
    init_predicate_states: List[Tuple[str, List[str]]]

    def __init__(
        self, 
        predicates: List[str],
        predicate_arity_tuples: List[Tuple[str, int]],
        effect_predicates: List[str],
        objects: List[str],
        init_predicate_states: List[Tuple[str, List[str]]]
    ) -> None:
        self.predicates = predicates
        self.predicate_arity_tuples = predicate_arity_tuples
        self.effect_predicates = effect_predicates
        self.objects = objects
        self.init_predicate_states = init_predicate_states

    def sort(self) -> None:
        # self.predicates.sort()
        # self.objects.sort()
        # self.effect_predicates.sort()
        self.init_predicate_states.sort()
