
from .gnn_data import GnnData
from .gnn_dataset_entry import GnnDatasetEntry

import torch
from torch.functional import Tensor
from typing import List, Dict, Tuple
import sys

class GnnDatasetEntryAdapter:
    def __init__(self) -> None:
        pass


    def get_goal_predicate_offset(self, gnn_predicates_keys: List[int]) -> int:
        # Finds offset of goal predicate
        #
        # Example: test_clear_probBLOCKS-2-0_states.txt
        # _G
        # 
        # 5
        # 
        # Structure:
        # '_G'
        # 
        # int - amount of predicates in graph
        return len(gnn_predicates_keys)

    
    def adapt_facts(self, gnn_facts: List[Tuple[int, List[int]]]) -> List[Tuple[int, List[int]]]:
        return gnn_facts
    

    def adapt_goals(self, gnn_goals: List[Tuple[int, List[int]]], goal_predicate_offset: int) -> List[Tuple[int, List[int]]]:
        # Convert goals' predicates into predicates with goal offset
        #
        # Example: test_clear_probBLOCKS-2-0_states.txt
        # [('clear_G', ['a'])]
        #
        # [(7, [1])]
        # 
        # Structure:
        # [(predicate + goal_predicate_offset, [objects])]
        return [(predicate + goal_predicate_offset, objects) for predicate, objects in gnn_goals]
    

    def adapt_state(self, gnn_dataset_entry_facts: List[Tuple[int, List[int]]], gnn_dataset_entry_goals: List[Tuple[int, List[int]]], gnn_state: List[Tuple[int, List[int]]]) -> Dict[int, Tensor]:
        # Get all predicate objects pairs
        # 
        # Example: test_clear_probBLOCKS-2-0_states.txt
        # 
        # gnn_dataset_entry_facts: []
        # gnn_dataset_entry_goals: [(7, [1])]
        # gnn_state(from index 0): [(1, [0]), (1, [1]), (2, [0]), (2, [1]), (3, [])]
        #
        # Result:
        # [(7, [1]), (1, [0]), (1, [1]), (2, [0]), (2, [1]), (3, [])]
        #
        # Structure:
        # [(predicate, [objects])]
        all_predicate_objects_pairs = gnn_dataset_entry_facts + gnn_dataset_entry_goals + gnn_state


        # Group predicate objects pairs by predicate (when we have the predicate with the same name but different list of argument objects, we will create here {predicate: list of lists of arguments})
        #
        # [('on', ['a', 'b']), ('on', ['c','a'])] -> {'on': [['a', 'b'], ['c','a']]}
        #
        # Example: test_clear_probBLOCKS-2-0_states.txt
        #
        # (for index 0) ({'clear_G': [['a']], 'ontable': [[b'], ['a']], 'clear': [['b'], ['a']], 'handempty': [[]]}, 0.0)
        # (for index 1) ({'clear_G': [['a']], 'holding': [['a']], 'ontable': [['b']], 'clear': [['b']]}, 1.0)
        # (for index 2) ({'clear_G': [['a']], 'on': [['a', 'b']], 'ontable': [['b']], 'clear': [['a']], 'handempty': [[]]}, 0.0)
        # (for index 3) ({'clear_G': [['a']], 'holding': [['b']], 'ontable': [['a']], 'clear': [['a']]}, 0.0)
        # (for index 4) ({'clear_G': [['a']], 'on': [['b', 'a']], 'ontable': [['a']], 'clear': [['b']], 'handempty': [[]]}, 1.0)
        #
        # (for index 0) ({7: tensor([[1]]), 1: tensor([[0],[1]]), 2: tensor([[0], [1]]), 3: tensor([], size=(1, 0))}, tensor([0.]))
        #
        # Structure:
        # ({predicate: Tensor([[objects]])}, cost)
        grouped = {}
        for predicate, objects in all_predicate_objects_pairs:
            if predicate not in grouped: 
                grouped[predicate] = []

            grouped[predicate].append(objects)

        # convert to tensor
        for predicate in grouped.keys():
            grouped[predicate] = torch.tensor(grouped[predicate], dtype=torch.long)

        return grouped
    

    def adapt_cost(self, gnn_cost: float) -> Tensor:
        return torch.tensor([gnn_cost], dtype=torch.double)
    

    def adapt_state_cost_pairs(self, min_cost: float, max_cost: float, max_samples_per_value: int, gnn_cost_state_pairs: List[Tuple[float, List[Tuple[int, List[int]]]]], gnn_dataset_entry_facts: List[Tuple[int, List[int]]], gnn_dataset_entry_goals: List[Tuple[int, List[int]]]) -> List[Tuple[Dict[int, Tensor], Tensor]]:
        gnn_dataset_entry_state_cost_pairs: List[Tuple[Dict[int, Tensor], Tensor]] = []

        sample_count = {}
        for gnn_cost, gnn_state in gnn_cost_state_pairs:
            gnn_cost_key = int(gnn_cost)
            
            # Init count for current cost if not exists
            if gnn_cost_key not in sample_count.keys():
                sample_count[gnn_cost_key] = 0

            # Add state cost pairs only if it satisfies constrains of costs and count per sample 
            if min_cost <= gnn_cost and gnn_cost <= max_cost and sample_count[gnn_cost_key] <= max_samples_per_value:
                sample_count[gnn_cost_key] += 1

                gnn_dataset_entry_input = self.adapt_state(gnn_dataset_entry_facts, gnn_dataset_entry_goals, gnn_state)
                gnn_dataset_entry_target = self.adapt_cost(gnn_cost)

                gnn_dataset_entry_state_cost_pairs.append((gnn_dataset_entry_input, gnn_dataset_entry_target))

        return gnn_dataset_entry_state_cost_pairs


    def adapt(self, gnn_data: GnnData, min_cost: float = 0, max_cost: float = sys.float_info.max, max_samples_per_value: int = sys.maxsize) -> GnnDatasetEntry:
        goal_predicate_offset = self.get_goal_predicate_offset(gnn_data.predicates.keys())

        gnn_dataset_entry_predicate_arity_tuples = gnn_data.predicate_arity_tuples + [(predicate_value + goal_predicate_offset, predicate_arity) for predicate_value, predicate_arity in gnn_data.predicate_arity_tuples]
        gnn_dataset_entry_facts = self.adapt_facts(gnn_data.facts)
        gnn_dataset_entry_goals = self.adapt_goals(gnn_data.goals, goal_predicate_offset)
        gnn_dataset_entry_state_cost_pairs = self.adapt_state_cost_pairs(min_cost, max_cost, max_samples_per_value, gnn_data.cost_state_pairs, gnn_dataset_entry_facts, gnn_dataset_entry_goals)

        return GnnDatasetEntry(gnn_dataset_entry_predicate_arity_tuples, gnn_dataset_entry_state_cost_pairs)
