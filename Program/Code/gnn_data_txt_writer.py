
from .gnn_data import GnnData

from pathlib import Path
from typing import List, Dict, Tuple
import os

class GnnDataTxtWriter:
    def __init__(self):
        pass


    def write_objects(self, lines: List[str], objects: Dict[int, str]) -> List[str]:
        lines.append('BEGIN_OBJECTS')
        
        for object_index, object_name, in objects.items():
            lines.append(str(object_index) + ' ' + str(object_name))

        lines.append('END_OBJECTS')

        return lines
    

    def write_predicates(self, lines: List[str], predicates: Dict[int, str]) -> List[str]:
        lines.append('BEGIN_PREDICATES')
        
        for predicate_index, predicate_name in predicates.items():
            line = str(predicate_index) + ' ' + str(predicate_name)
            lines.append(line)

        lines.append('END_PREDICATES')

        return lines


    def write_predicate_arity_tuples(self, lines: List[str], predicate_arity_tuples: List[Tuple[int, int]]) -> List[str]:
        lines.append('BEGIN_PREDICATE_ARITY_TUPLES')
        
        for predicate_index, predicate_arity in predicate_arity_tuples:
            line = str(predicate_index) + ' ' + str(predicate_arity)
            lines.append(line)

        lines.append('END_PREDICATE_PREDICATE_ARITY_TUPLES')

        return lines


    def write_facts(self, lines: List[str], facts: List[Tuple[int, List[int]]]) -> List[str]:
        lines.append('BEGIN_FACT_LIST')

        for fact in facts:
            predicate, objects = fact

            line = str(predicate)

            for object in objects:
                line += ' ' + str(object)
            
            lines.append(line)

        lines.append('END_FACT_LIST')
        
        return lines
    

    def write_goals(self, lines: List[str], goals: List[Tuple[int, List[int]]]) -> List[str]:
        lines.append('BEGIN_GOAL_LIST')

        for goal in goals:
            predicate, objects = goal

            line = str(predicate)

            for object in objects:
                line += ' ' + str(object)
            
            lines.append(line)

        lines.append('END_GOAL_LIST')

        return lines
    

    def write_state(self, lines: List[str], state: List[Tuple[int, List[int]]]) -> List[str]:
        lines.append('BEGIN_STATE')

        for predicate_objects_state in state:
            predicate, objects = predicate_objects_state

            line = str(predicate)

            for object in objects:
                line += ' ' + str(object)
            
            lines.append(line)

        lines.append('END_STATE')
        
        return lines


    def write_cost_state_pairs(self, lines: List[str], cost_state_pairs: List[Tuple[float, List[Tuple[int, List[int]]]]]) -> List[str]:
        lines.append('BEGIN_STATE_LIST')

        for cost_state_pair in cost_state_pairs:
            cost, state = cost_state_pair

            lines.append('BEGIN_LABELED_STATE')
            lines.append(str(cost))

            lines = self.write_state(lines, state)

            lines.append('END_LABELED_STATE')

        lines.append('END_STATE_LIST')

        return lines


    def write(self, gnn_data: GnnData, output_folder_path: Path, output_file_name: str) -> None:
        output_file_name = output_file_name + '.txt'

        lines = []

        lines = self.write_objects(lines, gnn_data.objects)
        lines = self.write_predicates(lines, gnn_data.predicates)
        lines = self.write_predicate_arity_tuples(lines, gnn_data.predicate_arity_tuples)
        lines = self.write_facts(lines, gnn_data.facts)
        lines = self.write_goals(lines, gnn_data.goals)
        lines = self.write_cost_state_pairs(lines, gnn_data.cost_state_pairs)

        path = os.path.abspath(os.path.join(output_folder_path, output_file_name))

        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, 'w') as f:
            for line in lines:
                f.write(f"{line}\n")
