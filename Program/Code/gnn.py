
from pathlib import Path
import os
from typing import Any, Dict, List, Union
import uuid
import subprocess
import time

from torch.utils.data.dataloader import DataLoader

import pytorch_lightning as pl
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint, RichProgressBar
from pytorch_lightning.loggers import CSVLogger

from .Models.add import AddModel 
from .Models.max import MaxModel
from .gnn_dataset import GnnDataset, collate_batch_of_gnn_dataset_state_cost_pairs
from .utils import bcolors, get_next_experiment_version, seconds2hours_minutes_seconds
from .configs import TrainConfig, ResumeConfig, TestConfig, PredictConfig
from .sas_file_parser import SasFileParser
from .gnn_data_json_reader import GnnDataJsonReader
from .pddl_file_reader import PddlFileReader
from .sas_data import SasData
from .pddl_data import PddlData
from .sas_plan_predictor import SasPlanPredictor
from .sas_plan_data import SasPlanData
from .sas_plan_data_writer import SasPlanDataWriter


class Gnn():
    def __init__(self):
        pass


    def get_model_class(self, model_type: str) -> Union[AddModel, MaxModel]:
        if model_type == 'add':
            return AddModel
        elif model_type == 'max':
            return MaxModel
        
        raise ValueError('Unknown type of model')


    def load_trainer(self, problem_name: str, model_type: str, patience: int, profiler: Union[str, None], gradient_accumulation: int, gradient_clip: float, max_epochs: int, accelerator: str, devices: int, info: bool) -> pl.Trainer:
        # Setup
        model_type = model_type.capitalize()
        version = 'Version_' + str(get_next_experiment_version(os.path.join('TrainedModels', problem_name, model_type), 'Version_'))


        # 1. callback: stopping, monitor model validation loss result, continue max args.patience times without any change
        early_stopping = EarlyStopping(monitor='validation_loss', patience=patience)


        # 2. callback: checkpoint, save top best 1 models based on validation loss result
        checkpoint = ModelCheckpoint(
            save_top_k=1,
            monitor='validation_loss',
            dirpath=os.path.join('TrainedModels', problem_name, model_type, version, 'Checkpoints'),
            filename="{epoch}_{validation_loss:.3f}",
        )


        # 3. callback: bar
        progress_bar = RichProgressBar()


        # Create specific logger 
        logger = CSVLogger(
            save_dir=os.path.join('TrainedModels', problem_name),
            name=model_type,
            version=version
        )
        
        if info: print('   ' + f'Results will be saved in folder [{bcolors.OKGREEN}{os.path.join("TrainedModels", problem_name, model_type, version)}{bcolors.ENDC}]')

        # Create pytorch lightning trainer
        trainer = pl.Trainer(
            num_sanity_val_steps=0, # dont run n batches of val before starting the training routine
            callbacks=[early_stopping, checkpoint, progress_bar],
            auto_lr_find=True, # will make trainer.tune() run a learning rate finder, trying to optimize initial learning for faster convergence
            profiler=profiler,
            accumulate_grad_batches=gradient_accumulation,
            gradient_clip_val=gradient_clip,
            max_epochs=max_epochs,
            accelerator=accelerator,
            devices=devices,
            logger=logger,
            auto_select_gpus=accelerator == 'gpu', # enable auto selection (will find available GPUs on system), auto_select_gpus: If enabled 'devices' is an integer, pick available
        )

        return trainer


    def train(self, config: TrainConfig) -> None:
        if config.info: print(f'Running GNN {bcolors.OKCYAN}train{bcolors.ENDC} mode')

        reader = GnnDataJsonReader()

        # Create a train dataset
        if config.info: print(f'{bcolors.FAIL}1.{bcolors.ENDC} Loading from [{bcolors.OKGREEN}{config.train_dataset_folder_path}{bcolors.ENDC}] train dataset...')
        train_dataset_loader = DataLoader(
            dataset=GnnDataset(Path(os.path.abspath(config.train_dataset_folder_path)), reader, max_samples_per_value=config.max_samples_per_value),
            batch_size=config.batch_size,
            drop_last=False, # the drop_last argument drops the last non-full batch of each worker’s dataset replica
            collate_fn=collate_batch_of_gnn_dataset_state_cost_pairs,
            pin_memory=True, # recommended using automatic memory pinning (i.e., setting pin_memory=True), which enables fast data transfer to CUDA-enabled GPUs.
            num_workers=config.loader_num_workers,
            shuffle=True
        )

        # Create a validation dataset
        if config.info: print(f'{bcolors.FAIL}2.{bcolors.ENDC} Loading from [{bcolors.OKGREEN}{config.validation_dataset_folder_path}{bcolors.ENDC}] validation dataset...')
        validation_dataset_loader = DataLoader(
            dataset=GnnDataset(Path(os.path.abspath(config.validation_dataset_folder_path)), reader, max_samples_per_value=config.max_samples_per_value),
            batch_size=config.batch_size,
            drop_last=False, # the drop_last argument drops the last non-full batch of each worker’s dataset replica
            collate_fn=collate_batch_of_gnn_dataset_state_cost_pairs,
            pin_memory=True, # recommended using automatic memory pinning (i.e., setting pin_memory=True), which enables fast data transfer to CUDA-enabled GPUs.
            num_workers=config.loader_num_workers,
            shuffle=False
        )

        assert(train_dataset_loader.dataset.predicate_arity_tuples == validation_dataset_loader.dataset.predicate_arity_tuples)

        # Create a model
        if config.info: print(f'{bcolors.FAIL}3.{bcolors.ENDC} Loading [{bcolors.OKGREEN}{config.model_type}{bcolors.ENDC}] model...')
        model = self.get_model_class(config.model_type)(
            predicates=train_dataset_loader.dataset.predicate_arity_tuples,
            hidden_size=config.hidden_size,
            iterations=config.iterations,
            learning_rate=config.learning_rate,
            l1_factor=config.l1_factor,
            weight_decay=config.weight_decay
        )

        # Create trainer for training
        if config.info: print(f'{bcolors.FAIL}4.{bcolors.ENDC} Initializing a trainer...')
        trainer = self.load_trainer(
            problem_name=config.problem_name,
            model_type=config.model_type,
            patience=config.patience,
            profiler=config.profiler,
            gradient_accumulation=config.gradient_accumulation,
            gradient_clip=config.gradient_clip,
            max_epochs=config.max_epochs,
            accelerator=config.accelerator,
            devices=config.devices,
            info=config.info
        )

        # Train the model
        if config.info: print(f'{bcolors.FAIL}5.{bcolors.ENDC} Training the model...')
        trainer.fit(
            model=model, 
            train_dataloaders=train_dataset_loader,
            val_dataloaders=validation_dataset_loader
        )


    def resume(self, config: ResumeConfig) -> None:
        if config.info: print(f'Running GNN {bcolors.OKCYAN}resume{bcolors.ENDC} mode')
    
        reader = GnnDataJsonReader()

        # Create a train dataset
        if config.info: print(f'{bcolors.FAIL}1.{bcolors.ENDC} Loading from [{bcolors.OKGREEN}{config.train_dataset_folder_path}{bcolors.ENDC}] train dataset...')
        train_dataset_loader = DataLoader(
            dataset=GnnDataset(Path(os.path.abspath(config.train_dataset_folder_path)), reader, max_samples_per_value=config.max_samples_per_value),
            batch_size=config.batch_size,
            drop_last=False, # the drop_last argument drops the last non-full batch of each worker’s dataset replica
            collate_fn=collate_batch_of_gnn_dataset_state_cost_pairs,
            pin_memory=True, # recommended using automatic memory pinning (i.e., setting pin_memory=True), which enables fast data transfer to CUDA-enabled GPUs.
            num_workers=config.loader_num_workers,
            shuffle=True
        )

        # Create a validation dataset
        if config.info: print(f'{bcolors.FAIL}2.{bcolors.ENDC} Loading from [{bcolors.OKGREEN}{config.validation_dataset_folder_path}{bcolors.ENDC}] validation dataset...')
        validation_dataset_loader = DataLoader(
            dataset=GnnDataset(Path(os.path.abspath(config.validation_dataset_folder_path)), reader, max_samples_per_value=config.max_samples_per_value),
            batch_size=config.batch_size,
            drop_last=False, # the drop_last argument drops the last non-full batch of each worker’s dataset replica
            collate_fn=collate_batch_of_gnn_dataset_state_cost_pairs,
            pin_memory=True, # recommended using automatic memory pinning (i.e., setting pin_memory=True), which enables fast data transfer to CUDA-enabled GPUs.
            num_workers=config.loader_num_workers,
            shuffle=False
        )

        assert(train_dataset_loader.dataset.predicate_arity_tuples == validation_dataset_loader.dataset.predicate_arity_tuples)

        # Load model from a checkpoint
        if config.info: print(f'{bcolors.FAIL}3.{bcolors.ENDC} Initializing [{bcolors.OKGREEN}{config.model_type}{bcolors.ENDC}] model from [{bcolors.OKGREEN}{config.model_file_path}{bcolors.ENDC}] checkpoint...')
        model = self.get_model_class(config.model_type).load_from_checkpoint(
            checkpoint_path=Path(os.path.abspath(config.model_file_path)),
            strict=False
        )

        # Create trainer for training
        if config.info: print(f'{bcolors.FAIL}4.{bcolors.ENDC} Initializing trainer...')
        trainer = self.load_trainer(
            problem_name=config.problem_name,
            model_type=config.model_type,
            patience=config.patience,
            profiler=config.profiler,
            gradient_accumulation=config.gradient_accumulation,
            gradient_clip=config.gradient_clip,
            max_epochs=config.max_epochs,
            accelerator=config.accelerator,
            devices=config.devices,
            info=config.info
        )

        # Train the model
        if config.info: print(f'{bcolors.FAIL}5.{bcolors.ENDC} Continuing training model...')
        trainer.fit(
            model=model,
            train_dataloaders=train_dataset_loader,
            val_dataloaders=validation_dataset_loader,
            ckpt_path=Path(os.path.abspath(config.model_file_path))
        )


    def test(self, config: TestConfig) -> None:
        if config.info: print(f'Running GNN {bcolors.OKCYAN}test{bcolors.ENDC} mode')

        reader = GnnDataJsonReader()

        # Create a test dataset
        if config.info: print(f'{bcolors.FAIL}1.{bcolors.ENDC} Loading from [{bcolors.OKGREEN}{config.test_dataset_folder_path}{bcolors.ENDC}] test dataset...')
        test_dataset_loader = DataLoader(
            dataset= GnnDataset(Path(os.path.abspath(config.test_dataset_folder_path)), reader, max_samples_per_value=config.max_samples_per_value),
            batch_size=config.batch_size,
            drop_last=False, # the drop_last argument drops the last non-full batch of each worker’s dataset replica
            collate_fn=collate_batch_of_gnn_dataset_state_cost_pairs,
            pin_memory=True, # recommended using automatic memory pinning (i.e., setting pin_memory=True), which enables fast data transfer to CUDA-enabled GPUs.
            num_workers=config.loader_num_workers,
            shuffle=False
        )


        # Load model from a checkpoint
        if config.info: print(f'{bcolors.FAIL}2.{bcolors.ENDC} Initializing [{bcolors.OKGREEN}{config.model_type}{bcolors.ENDC}] model from [{bcolors.OKGREEN}{config.model_file_path}{bcolors.ENDC}] checkpoint...')
        model = self.get_model_class(config.model_type).load_from_checkpoint(
            checkpoint_path=Path(os.path.abspath(config.model_file_path)),
            strict=False
        )


        # Create trainer for testing
        if config.info: print(f'{bcolors.FAIL}3.{bcolors.ENDC} Initializing trainer...')
        trainer = pl.Trainer(
            accelerator=config.accelerator,
            devices=config.devices,
            auto_select_gpus=config.accelerator == 'gpu',
            logger=False,
            callbacks=[RichProgressBar()]
        )


        # Test the model on a test dataset
        if config.info: print(f'{bcolors.FAIL}4.{bcolors.ENDC} Testing the model...')
        trainer.test(
            model=model,
            dataloaders=test_dataset_loader
        )


    def predict(self, config: PredictConfig) -> None:
        if config.info: print(f'Running GNN {bcolors.OKCYAN}predict{bcolors.ENDC} mode')

        unique_number = uuid.uuid4().hex


        # 0. Setup name for output and temporary files
        if config.output_file_name == '': config.output_file_name = os.path.basename(os.path.normpath(config.pddl_domain_instance_file_path)).replace('.pddl', '_sas_plan')


        # 1. Create sas file using Fast Downward planner
        if config.info: print(f'{bcolors.FAIL}1.{bcolors.ENDC} Creating [{config.output_file_name}_{unique_number}_output.sas] from [{config.pddl_domain_file_path}] pddl domain and [{config.pddl_domain_instance_file_path}] pddl domain instance using Fast Downward planner...')
        #os.system(f'python {os.path.join(os.path.abspath(config.fastdownward_folder_path), "fast-downward.py")} --sas-file {config.output_file_name}_{unique_number}_output.sas --translate {os.path.abspath(config.pddl_domain_file_path)} {os.path.abspath(config.pddl_domain_instance_file_path)}{" --translate-options --keep-unimportant-variables " if not config.fastdownward_pruning else ""} > {config.output_file_name}_{unique_number}_fast-downward-log.txt')
        with open(f'{config.output_file_name}_{unique_number}_fast-downward-log.txt', mode='w') as file:
            subprocess.run(f'python {os.path.join(os.path.abspath(config.fastdownward_folder_path), "fast-downward.py")} --sas-file {config.output_file_name}_{unique_number}_output.sas --translate {os.path.abspath(config.pddl_domain_file_path)} {os.path.abspath(config.pddl_domain_instance_file_path)}{" --translate-options --keep-unimportant-variables " if not config.fastdownward_pruning else ""}', stdout=file, stderr=file, shell=True)


        # 2. Parse sas file
        if config.info: print(f'{bcolors.FAIL}2.{bcolors.ENDC} Parsing [{config.output_file_name}_{unique_number}_output.sas] into [sas_data]...')
        parser = SasFileParser()
        sas_data = parser.parse(Path(os.path.abspath(f'{config.output_file_name}_{unique_number}_output.sas')))


        # 3. Read pddl data in pddl domain file and pddl domain instance file
        if config.info: print(f'{bcolors.FAIL}3.{bcolors.ENDC} Reading [{config.pddl_domain_file_path}] pddl domain and [{config.pddl_domain_instance_file_path}] pddl domain instance into [pddl_data]...')
        reader = PddlFileReader()
        pddl_data = reader.read(Path(os.path.abspath(config.pddl_domain_file_path)), Path(os.path.abspath(config.pddl_domain_instance_file_path)))


        # 4. Load model from a checkpoint
        if config.info: print(f'{bcolors.FAIL}4.{bcolors.ENDC} Initializing [{bcolors.OKGREEN}{config.model_type}{bcolors.ENDC}] model from [{bcolors.OKGREEN}{config.model_file_path}{bcolors.ENDC}] checkpoint...')
        model = self.get_model_class(config.model_type).load_from_checkpoint(
            checkpoint_path=Path(os.path.abspath(config.model_file_path)),
            strict=False
        )


        # 5. Predict sas plan
        start = time.time()
        if config.info: print(f'{bcolors.FAIL}5.{bcolors.ENDC} Creating sas plan predictor using model, [sas_data] and [pddl_data] to get [sas_plan_data]...')
        predictor = SasPlanPredictor()
        sas_plan_data = predictor.predict(model, sas_data, pddl_data, config.step_limit, config.info)
        end = time.time()
        if config.info: print(f'   Prediction part took {seconds2hours_minutes_seconds(end - start)}')
        

        # 6. Write sas plan
        if sas_plan_data.plan != None:
            if config.info: print(f'{bcolors.FAIL}6.{bcolors.ENDC} Writing [sas_plan_data] in folder [{config.output_folder_path}] in file [{config.output_file_name}]...')
            writer = SasPlanDataWriter()
            writer.write(sas_plan_data, Path(os.path.abspath(config.output_folder_path)), config.output_file_name)
        else:
            if config.info: print(f'{bcolors.FAIL}6.{bcolors.ENDC} GNN failed to find plan...')


        # 7. Clean up all temporary files 
        if config.clean_up:
            if config.info: print(f'{bcolors.FAIL}7.{bcolors.ENDC} Deleting all temporary files...')
            self.clean_up(config.output_file_name, unique_number)
    

    def clean_up(self, output_file_name: str, unique_number: uuid.UUID)-> None:
        if os.path.exists(f'{output_file_name}_{unique_number}_output.sas'):
            os.remove(f'{output_file_name}_{unique_number}_output.sas')

        if os.path.exists(f'{output_file_name}_{unique_number}_fast-downward-log.txt'):
            os.remove(f'{output_file_name}_{unique_number}_fast-downward-log.txt')
