
from pathlib import Path
from typing import Union 

class Config():
    mode: str

    def __init__(self, mode: str) -> None:
        self.mode = str(mode)


class SingleConfig(Config):
    fastdownward_folder_path: Path
    pddl_domain_file_path: Path
    pddl_domain_instance_file_path: Path
    output_folder_path: Path
    output_file_name: str
    fastdownward_pruning: bool
    timer: int
    count: int
    length: int
    patience: int
    info: bool
    clean_up: bool

    def __init__(
        self,
        mode: str,
        fastdownward_folder_path: Path,
        pddl_domain_file_path: Path,
        pddl_domain_instance_file_path: Path,
        output_folder_path: Path,
        output_file_name: str,
        fastdownward_pruning: bool,
        timer: int,
        count: int,
        length: int,
        patience: int,
        info: bool,
        clean_up: bool
    ) -> None:
        super().__init__(mode)
        self.fastdownward_folder_path = Path(fastdownward_folder_path)
        self.pddl_domain_file_path = Path(pddl_domain_file_path)
        self.pddl_domain_instance_file_path = Path(pddl_domain_instance_file_path)
        self.output_folder_path = Path(output_folder_path)
        self.output_file_name = str(output_file_name)
        self.fastdownward_pruning = bool(fastdownward_pruning)
        self.timer = int(timer)
        self.count = int(count)
        self.length = int(length)
        self.patience = int(patience)
        self.info = bool(info)
        self.clean_up = bool(clean_up)


class MultipleConfig(Config):
    fastdownward_folder_path: Path
    pddl_folder_path: Path
    output_folder_path: Path
    fastdownward_pruning: bool
    timer: int
    count: int
    length: int
    patience: int
    info: bool
    clean_up: bool

    def __init__(
        self,
        mode: str,
        fastdownward_folder_path: Path,
        pddl_folder_path: Path,
        output_folder_path: Path,
        fastdownward_pruning: bool,
        timer: int,
        count: int,
        length: int,
        patience: int,
        info: bool,
        clean_up: bool
    ) -> None:
        super().__init__(mode)
        self.fastdownward_folder_path = Path(fastdownward_folder_path)
        self.pddl_folder_path = Path(pddl_folder_path)
        self.output_folder_path = Path(output_folder_path)
        self.fastdownward_pruning = bool(fastdownward_pruning)
        self.timer = int(timer)
        self.count = int(count)
        self.length = int(length)
        self.patience = int(patience)
        self.info = bool(info)
        self.clean_up = bool(clean_up)


class TrainConfig(Config):
    model_type: str
    problem_name: str
    train_dataset_folder_path: Path
    validation_dataset_folder_path: Path
    hidden_size: int
    iterations: int
    batch_size: int
    accelerator: str
    devices: int
    max_epochs: int 
    learning_rate: float
    l1_factor: float
    weight_decay: float
    gradient_accumulation: int
    max_samples_per_value: int
    patience: int
    gradient_clip: float
    profiler: Union[str, None]
    loader_num_workers: int
    info: bool

    def __init__(
        self,
        mode: str,
        model_type: str,
        problem_name: str,
        train_dataset_folder_path: Path,
        validation_dataset_folder_path: Path,
        hidden_size: int,
        iterations: int,
        batch_size: int,
        accelerator: str,
        devices: int,
        max_epochs: int ,
        learning_rate: float,
        l1_factor: float,
        weight_decay: float,
        gradient_accumulation: int,
        max_samples_per_value: int,
        patience: int,
        gradient_clip: float,
        profiler: Union[str, None],
        loader_num_workers: int,
        info: bool,
    ) -> None:
        super().__init__(mode)
        self.model_type = str(model_type)
        self.problem_name = str(problem_name)
        self.train_dataset_folder_path = Path(train_dataset_folder_path)
        self.validation_dataset_folder_path = Path(validation_dataset_folder_path)
        self.hidden_size = int(hidden_size)
        self.iterations = int(iterations)
        self.batch_size = int(batch_size)
        self.accelerator = str(accelerator)
        self.devices = int(devices)
        self.max_epochs = int(max_epochs)
        self.learning_rate = float(learning_rate)
        self.l1_factor = float(l1_factor)
        self.weight_decay = float(weight_decay)
        self.gradient_accumulation = int(gradient_accumulation)
        self.max_samples_per_value = int(max_samples_per_value)
        self.patience = int(patience)
        self.gradient_clip = float(gradient_clip)
        self.profiler = None if profiler == None else str(profiler)
        self.loader_num_workers = int(loader_num_workers)
        self.info = bool(info)


class ResumeConfig(Config):
    model_type: str
    model_file_path: Path
    problem_name: str
    train_dataset_folder_path: Path
    validation_dataset_folder_path: Path
    batch_size: int
    accelerator: str
    devices: int
    max_epochs: int
    gradient_accumulation: int
    max_samples_per_value: int
    patience: int
    gradient_clip: float
    profiler: Union[str, None]
    loader_num_workers: int
    info: bool

    def __init__(
        self,
        mode: str,
        model_type: str,
        model_file_path: Path,
        problem_name: str,
        train_dataset_folder_path: Path,
        validation_dataset_folder_path: Path,
        batch_size: int,
        accelerator: str,
        devices: int,
        max_epochs: int,
        gradient_accumulation: int,
        max_samples_per_value: int,
        patience: int,
        gradient_clip: float,
        profiler: Union[str, None],
        loader_num_workers: int,
        info: bool
    ) -> None:
        super().__init__(mode)
        self.model_type = str(model_type)
        self.model_file_path = Path(model_file_path)
        self.problem_name = str(problem_name)
        self.train_dataset_folder_path = Path(train_dataset_folder_path)
        self.validation_dataset_folder_path = Path(validation_dataset_folder_path)
        self.batch_size = int(batch_size)
        self.accelerator = str(accelerator)
        self.devices = int(devices)
        self.max_epochs = int(max_epochs)
        self.gradient_accumulation = int(gradient_accumulation)
        self.max_samples_per_value = int(max_samples_per_value)
        self.patience = int(patience)
        self.gradient_clip = float(gradient_clip)
        self.profiler = None if profiler == None else str(profiler)
        self.loader_num_workers = int(loader_num_workers)
        self.info = bool(info)


class TestConfig(Config):
    model_type: str
    model_file_path: Path
    test_dataset_folder_path: Path
    batch_size: int
    accelerator: str
    devices: int
    max_samples_per_value: int
    loader_num_workers: int
    info: bool

    def __init__(
        self,
        mode: str,
        model_type: str,
        model_file_path: Path,
        test_dataset_folder_path: Path,
        batch_size: int,
        accelerator: str,
        devices: int,
        max_samples_per_value: int,
        loader_num_workers: int,
        info: bool
    ) -> None:
        super().__init__(mode)
        self.model_type = str(model_type)
        self.model_file_path = Path(model_file_path)
        self.test_dataset_folder_path = Path(test_dataset_folder_path)
        self.batch_size = int(batch_size)
        self.accelerator = str(accelerator)
        self.devices = int(devices)
        self.max_samples_per_value = int(max_samples_per_value)
        self.loader_num_workers = int(loader_num_workers)
        self.info = bool(info)


class PredictConfig(Config):
    model_type: str
    model_file_path: Path
    fastdownward_folder_path: Path
    pddl_domain_file_path: Path
    pddl_domain_instance_file_path: Path
    step_limit: int
    output_folder_path: Path
    output_file_name: str
    fastdownward_pruning: bool
    info: bool
    clean_up: bool

    def __init__(
        self,
        mode: str,
        model_type: str,
        model_file_path: Path,
        fastdownward_folder_path: Path,
        pddl_domain_file_path: Path,
        pddl_domain_instance_file_path: Path,
        step_limit: int,
        output_folder_path: Path,
        output_file_name: str,
        fastdownward_pruning: bool,
        info: bool,
        clean_up: bool
    ) -> None:
        super().__init__(mode)
        self.model_type = str(model_type)
        self.model_file_path = Path(model_file_path)
        self.fastdownward_folder_path = Path(fastdownward_folder_path)
        self.pddl_domain_file_path = Path(pddl_domain_file_path)
        self.pddl_domain_instance_file_path = Path(pddl_domain_instance_file_path)
        self.step_limit = int(step_limit)
        self.output_folder_path = Path(output_folder_path)
        self.output_file_name = str(output_file_name)
        self.fastdownward_pruning = bool(fastdownward_pruning)
        self.info = bool(info)
        self.clean_up = bool(clean_up)
