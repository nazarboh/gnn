
from typing import List, Tuple
from pathlib import Path
import re

from .pddl_data import PddlData

class PddlFileReader():
    predicate_or_object_name_pattern : str = '(\w|-)+'
    predicate_object_name_pattern: str = '\s*\?\s*'+ predicate_or_object_name_pattern + '\s*'
    left_parenthesis_pattern : str = '\s*\(\s*'
    right_parenthesis_pattern : str = '\s*\)\s*'
    predicate_state_pattern : str = left_parenthesis_pattern + '(' + predicate_or_object_name_pattern + '(\s*\?' + predicate_or_object_name_pattern + '\s*)*' + ')' + right_parenthesis_pattern
    not_pattern : str = left_parenthesis_pattern + '(not|NOT)'+ predicate_state_pattern + right_parenthesis_pattern
    predicates_pattern : str = left_parenthesis_pattern + '(:predicates|:PREDICATES)' + '(' + predicate_state_pattern + ')+' + right_parenthesis_pattern
    effect_pattern : str = '(:effect|:EFFECT)' + left_parenthesis_pattern + 'and' + '(' + not_pattern + '|' + predicate_state_pattern + ')+' + right_parenthesis_pattern
    
    objects_pattern : str = left_parenthesis_pattern + '(:objects|:OBJECTS)' +  '(\s*' + predicate_or_object_name_pattern + '\s*)+'  + right_parenthesis_pattern
    init_predicate_state_pattern : str = left_parenthesis_pattern + '(\s*' + predicate_or_object_name_pattern + '\s*)+' + right_parenthesis_pattern
    init_pattern : str = left_parenthesis_pattern + '(:init|:INIT)' + '(' + init_predicate_state_pattern + ')+' + right_parenthesis_pattern

    def __init__(self):
        pass
    
    
    def match_string(self, string: str, pattern: str) -> str:
        # matches first pattern appearance in provided string
        match = re.search(pattern, string)
        assert(match != None)
        matched_string = match.group()

        return matched_string


    def match_predicate_names(self, predicates_string: str) -> List[str]:
        predicate_names : List[str] = []

        # matches every (predicateG ?objectK ... ?objectN) in given predicate string
        for predicate_match in re.finditer(self.predicate_state_pattern, predicates_string):
            predicate_string = predicate_match.group()
            # matches predicate name in (predicateG ?objectK ... ?objectN)
            predicate_name = self.match_string(predicate_string, self.predicate_or_object_name_pattern).strip().lower()
            predicate_names.append(predicate_name)

        assert(len(predicate_names) > 0)
        
        return predicate_names


    def read_predicates(self, pddl_domain_string: str) -> List[str]:
        # matches predicate string in pddl domain string -> (:predicates (predicateG ?objectK ... ?objectN) ... (predicateM ?objectL ... ?objectO))
        predicates_string = self.match_string(pddl_domain_string, self.predicates_pattern)
        # matches predicates names in matched predicate string -> [predicateG ... predicateM]
        predicate_names = self.match_predicate_names(predicates_string)

        return predicate_names
    

    def match_strings(self, string: str, pattern: str) -> List[str]:
        match_strings : List[str] = []

        # matches every (predicateG ?objectK ... ?objectN) in given predicate string
        for match in re.finditer(pattern, string):
            match_string = match.group()
            match_strings.append(match_string)
        
        return match_strings


    def match_predicate_arity_tuples(self, predicates_string: str) -> List[Tuple[str, int]]:
        predicate_arity_tuples: List[Tuple[str, int]] = []

        # matches every (predicateG ?objectK ... ?objectN) in given predicate string
        for predicate_match in re.finditer(self.predicate_state_pattern, predicates_string):
            predicate_string = predicate_match.group()
            # matches predicate name in (predicateG ?objectK ... ?objectN)
            predicate_name = self.match_string(predicate_string, self.predicate_or_object_name_pattern).strip().lower()
            # matches every ?object in (predicateG ?objectK ... ?objectN)
            predicate_objects = [x.replace('?', '').strip().lower() for x in self.match_strings(predicate_string, self.predicate_object_name_pattern)]
            predicate_arity_tuples.append((predicate_name, len(predicate_objects)))

        assert(len(predicate_arity_tuples) > 0)

        return predicate_arity_tuples


    def read_predicate_arity_tuples(self, pddl_domain_string: str) -> List[Tuple[str, int]]:
        predicate_arity_tuples: List[Tuple[str, int]] = []

        # matches predicate string in pddl domain string -> (:predicates (predicateG ?objectK ... ?objectN) ... (predicateM ?objectL ... ?objectO))
        predicates_string = self.match_string(pddl_domain_string, self.predicates_pattern)
        # matches predicates names and their arities -> [(predicateG, U) ... (predicateM, W)]
        predicate_arity_tuples = self.match_predicate_arity_tuples(predicates_string)

        return predicate_arity_tuples


    def match_unique_effect_predicate_names(self, pddl_domain_string: str) -> List[str]:
        unique_effect_predicate_names : List[str] = []

        # matches every effect string (with operator and; not operator is optinal) in pddl domain string -> :effect (and (not (predicateG ?objectK ... ?objectN)) ... (predicateM ?objectL ... ?objectO))
        for effect_predicate_match in re.finditer(self.effect_pattern, pddl_domain_string):
            effect_predicate_string = effect_predicate_match.group()
            effect_predicate_names = self.match_predicate_names(effect_predicate_string) # reads predicates names in matched predicate string -> [predicateG ... predicateM]

            unique_effect_predicate_names = list(set(unique_effect_predicate_names) | set(effect_predicate_names)) # set union, only unique elements

        assert(len(unique_effect_predicate_names) > 0)

        return unique_effect_predicate_names


    def read_effect_predicates(self, pddl_domain_string: str) -> List[str]:
        return self.match_unique_effect_predicate_names(pddl_domain_string)


    def read_from_pddl_domain(self, pddl_domain_file_path: Path) -> Tuple[List[str], List[Tuple[str, int]], List[str]]:
        pddl_domain_string = self.read_file_string(pddl_domain_file_path)

        # from pddl domain read predicates and unique predicates that are effected in actions
        predicates = self.read_predicates(pddl_domain_string)
        predicate_arity_tuples = self.read_predicate_arity_tuples(pddl_domain_string)
        assert(len(predicates) == len(predicate_arity_tuples))
        effect_predicates = self.read_effect_predicates(pddl_domain_string)

        return predicates, predicate_arity_tuples, effect_predicates


    def match_object_names(self, objects_string: str) -> List[str]:
        objects_names : List[str] = []

        # matches every object name in objects string -> some_word (exept for 'objects')
        for i, object_name_match in enumerate(re.finditer(self.predicate_or_object_name_pattern, objects_string)):
            object_name = object_name_match.group().strip().lower()

            if i == 0:
                assert object_name == 'objects' or object_name == 'OBJECTS'
                continue
                
            objects_names.append(object_name)

        assert len(objects_names) > 0
        
        return objects_names


    def read_objects(self, pddl_domain_instance_string: str) -> List[str]:
        # matches object string in pddl domain instance string -> (:objects objectA .. objectN)
        objects_string = self.match_string(pddl_domain_instance_string, self.objects_pattern)
        # matches object names in matched object string -> [objectA ... objectN]
        objects_names = self.match_object_names(objects_string)

        return objects_names


    def match_init_predicate_states(self, init_predicate_states_string: str) -> List[Tuple[str, List[str]]]:
        init_predicates : List[Tuple[str, List[str]]] = []

        # match every init predicate state -> (predicate objectK ... objectN)
        for init_predicate_state_match in re.finditer(self.init_predicate_state_pattern, init_predicate_states_string):
            init_predicate_state_info = init_predicate_state_match.group().replace('(', ' ').replace(')', ' ').split()

            # create predicate state -> [(predicate, [objects])]
            init_predicate = init_predicate_state_info[0].strip().lower()
            init_objects = [object.strip().lower() for object in init_predicate_state_info[1:]]
            init_predicate_state = (init_predicate, init_objects)

            init_predicates.append(init_predicate_state)

        return init_predicates


    def read_init_predicate_states(self, pddl_domain_instance_string: str) -> List[Tuple[str, List[str]]]:
        # matches init string in pddl domain instance string -> (:init (predicateG objectK ... objectN) ... (predicateM objectL ... objectO))
        init_predicate_states_string = self.match_string(pddl_domain_instance_string, self.init_pattern)
        # matches predicate states in matched init string -> [(predicate, [objects])]
        init_predicate_states = self.match_init_predicate_states(init_predicate_states_string)

        return init_predicate_states


    def read_from_pddl_domain_instance(self, pddl_domain_instance_file_path: Path) -> Tuple[List[str], List[Tuple[str, List[str]]]]:
        pddl_domain_instance_string = self.read_file_string(pddl_domain_instance_file_path)
        
        # from pddl domain instance read objects and init predicates
        objects = self.read_objects(pddl_domain_instance_string)
        init_predicate_states = self.read_init_predicate_states(pddl_domain_instance_string)

        return objects, init_predicate_states


    def read_file_string(self, file_path: Path) -> str:
        with open(file_path, 'r') as file:
            return file.read().replace('\n', ' ')


    def read(self, pddl_domain_file_path: Path, pddl_domain_instance_file_path: Path) -> PddlData: 
        predicates, predicate_arity_tuples, effect_predicates = self.read_from_pddl_domain(pddl_domain_file_path)
        objects, init_predicate_states = self.read_from_pddl_domain_instance(pddl_domain_instance_file_path)

        return PddlData(predicates, predicate_arity_tuples, effect_predicates, objects, init_predicate_states)



if __name__ == "__main__":
    domain_path = Path('Data/Pddl/Logistics/Train/domain.pddl')
    instance_path = Path('Data/Pddl/Logistics/Train/logistics_15_0.pddl')
    
    # ; Automatically converted to only require STRIPS and negative preconditions  (define (domain logistics)   (:requirements :strips :negative-preconditions)   (:predicates     (package ?obj)     (truck ?truck)     (airplane ?airplane)     (airport ?airport)     (location ?loc)     (in-city ?obj ?city)     (city ?city)     (at ?obj ?loc)     (in ?obj ?loc)   )    (:action load-truck     :parameters (?obj ?truck ?loc)     :precondition (and       (package ?obj)       (truck ?truck)       (location ?loc)       (at ?truck ?loc)       (at ?obj ?loc)     )     :effect (and       (not (at ?obj ?loc))       (in ?obj ?truck)     )   )    (:action load-airplane     :parameters (?obj ?airplane ?loc)     :precondition (and       (package ?obj)       (airplane ?airplane)       (location ?loc)       (at ?obj ?loc)       (at ?airplane ?loc)     )     :effect (and       (not (at ?obj ?loc))       (in ?obj ?airplane)     )   )    (:action unload-truck     :parameters (?obj ?truck ?loc)     :precondition (and       (package ?obj)       (truck ?truck)       (location ?loc)       (at ?truck ?loc)       (in ?obj ?truck)     )     :effect (and       (not (in ?obj ?truck))       (at ?obj ?loc)     )   )    (:action unload-airplane     :parameters (?obj ?airplane ?loc)     :precondition (and       (package ?obj)       (airplane ?airplane)       (location ?loc)       (in ?obj ?airplane)       (at ?airplane ?loc)     )     :effect (and       (not (in ?obj ?airplane))       (at ?obj ?loc)     )   )    (:action drive-truck     :parameters (?truck ?loc-from ?loc-to ?city)     :precondition (and       (truck ?truck)       (location ?loc-from)       (location ?loc-to)       (city ?city)       (at ?truck ?loc-from)       (in-city ?loc-from ?city)       (in-city ?loc-to ?city)     )     :effect (and       (not (at ?truck ?loc-from))       (at ?truck ?loc-to)     )   )    (:action fly-airplane     :parameters (?airplane ?loc-from ?loc-to)     :precondition (and       (airplane ?airplane)       (airport ?loc-from)       (airport ?loc-to)       (at ?airplane ?loc-from)     )     :effect (and       (not (at ?airplane ?loc-from))       (at ?airplane ?loc-to)     )   ) )
    # ; Automatically converted to only require STRIPS and negative preconditions  (define (problem logistics-4-0)   (:domain logistics)   (:objects apn1 apt2 pos2      apt1 pos1 cit2 cit1 tru2 tru1 obj23 obj22 obj21 obj13 obj12 obj11)   (:init     (package obj11)     (package obj12)     (package obj13)     (package obj21)     (package obj22)     (package obj23)     (truck tru1)     (truck tru2)     (city cit1)     (city cit2)     (location pos1)     (location apt1)     (location pos2)     (location apt2)     (airport apt1)     (airport apt2)     (airplane apn1)     (at apn1 apt2)     (at tru1 pos1)     (at obj11 pos1)     (at obj12 pos1)     (at obj13 pos1)     (at tru2 pos2)     (at obj21 pos2)     (at obj22 pos2)     (at obj23 pos2)     (in-city pos1 cit1)     (in-city apt1 cit1)     (in-city pos2 cit2)     (in-city apt2 cit2)   )   (:goal     (and       (at obj11 apt1)     )   ) )

    reader = PddlFileReader()
    pddl_data = reader.read(domain_path, instance_path)
    print(pddl_data.predicates)
    print(pddl_data.effect_predicates)
    print(pddl_data.objects)
    print(pddl_data.init_predicate_states)