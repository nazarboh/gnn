
from .gnn_data import GnnData
from .gnn_data_reader import GnnDataReader

from pathlib import Path
from typing import List, Dict, Tuple
import os


class GnnDataTxtReader(GnnDataReader):
    def __init__(self):
        pass


    def read_objects(self, lines: List[str], index: int) -> Tuple[int, Dict[int, str]]:
        # Error
        index += 1
        line = (lines[index]).split()
        if line[0] != 'BEGIN_OBJECTS': 
            raise Exception('BEGIN_OBJECTS')
        
        objects : Dict[str, int] = {}

        while True:
            # read next line
            index += 1
            line = (lines[index]).split()

            # exit
            if line[0] == 'END_OBJECTS': 
                break
            
            # parse object 
            object_number = int(line[0])
            object_name = str(line[1])

            # add object number
            objects[object_number] = object_name

        return index, objects


    def read_predicates(self, lines: List[str], index: int) -> Tuple[int, Dict[int, str]]:
        # Error
        index += 1
        line = (lines[index]).split()
        if line[0] != 'BEGIN_PREDICATES': 
            raise Exception('BEGIN_PREDICATES')
        
        predicates : Dict[str, int] = {}

        while True:
            # read next line
            index += 1
            line = (lines[index]).split()

            # exit
            if line[0] == 'END_PREDICATES': 
                break
            
            # parse predicate 
            predicate_number = int(line[0])
            predicate_name = str(line[1])

            # add predicate number
            predicates[predicate_number] = predicate_name

        return index, predicates


    def read_predicate_arity_tuples(self, lines: List[str], index: int) -> Tuple[int, List[Tuple[int, int]]]:
        # Error
        index += 1
        line = (lines[index]).split()
        if line[0] != 'BEGIN_PREDICATE_ARITY_TUPLES': 
            raise Exception('BEGIN_PREDICATE_ARITY_TUPLES')
        
        predicate_arity_tuples: List[Tuple[int, int]] = []

        while True:
            # read next line
            index += 1
            line = (lines[index]).split()

            # exit
            if line[0] == 'END_PREDICATE_PREDICATE_ARITY_TUPLES': 
                break
            
            # parse predicate 
            predicate_value = int(line[0])
            predicate_arity = int(line[1])

            # add predicate number
            predicate_arity_tuples.append((predicate_value, predicate_arity))

        return index, predicate_arity_tuples


    def read_predicate_objects_state(self, line: str) -> Tuple[int, List[int]]:
        # parse predicate objects state
        predicate = int(line[0])
        objects = [int(i) for i in line[1:]]

        return (predicate, objects)


    def read_facts(self, lines: List[str], index: int) -> Tuple[int, List[Tuple[int, List[int]]]]:
        # Error
        index += 1
        line = (lines[index]).split()
        if line[0] != 'BEGIN_FACT_LIST': 
            raise Exception('BEGIN_FACT_LIST')
        
        facts : List[Tuple[int, List[int]]] = []

        while True:
            # read next line
            index += 1
            line = (lines[index]).split()

            # exit
            if line[0] == 'END_FACT_LIST': 
                break
            
            # parse predicate objects state
            fact = self.read_predicate_objects_state(line)

            # add fact
            facts.append(fact)

        return index, facts
    

    def read_goals(self, lines: List[str], index: int) -> Tuple[int, List[Tuple[int, List[int]]]]:
        # Error
        index += 1
        line = (lines[index]).split()
        if line[0] != 'BEGIN_GOAL_LIST': 
            raise Exception('BEGIN_GOAL_LIST')
        
        goals : List[Tuple[int, List[int]]] = []

        while True:
            # read next line
            index += 1
            line = (lines[index]).split()

            # exit
            if line[0] == 'END_GOAL_LIST': 
                break
            
            # parse predicate objects state
            goal = self.read_predicate_objects_state(line)

            # add goal
            goals.append(goal)

        return index, goals
    

    def read_cost_state_pair(self, lines: List[str], index: int) -> Tuple[int, Tuple[float, List[Tuple[int, List[int]]]]]:
        # Error
        index += 1
        line = (lines[index]).split()
        if line[0] != 'BEGIN_LABELED_STATE': 
            raise Exception('BEGIN_LABELED_STATE')
        
        # parse cost
        index += 1
        line = (lines[index]).split()
        cost = float(line[0])

        # Error
        index += 1
        line = (lines[index]).split()
        if line[0] != 'BEGIN_STATE': 
            raise Exception('BEGIN_STATE')
        
        state : List[Tuple[int, List[int]]] = []

        while True:
            # read next line
            index += 1
            line = (lines[index]).split()

            # exit
            if line[0] == 'END_STATE': 
                break
            
            # parse predicate objects state
            predicate_objects_state = self.read_predicate_objects_state(line)

            # add predicate objects state
            state.append(predicate_objects_state)

        # Error
        index += 1
        line = (lines[index]).split()
        if line[0] != 'END_LABELED_STATE': 
            raise Exception('END_LABELED_STATE')

        # create cost_state_pair
        cost_state_pair = (cost, state)

        return index, cost_state_pair


    def read_cost_state_pairs(self, lines: List[str], index: int) -> Tuple[int, List[Tuple[float, List[Tuple[int, List[int]]]]]]:
        # Error
        index += 1
        line = (lines[index]).split()
        if line[0] != 'BEGIN_STATE_LIST': 
            raise Exception('BEGIN_STATE_LIST')
        
        cost_state_pairs : List[Tuple[float, List[Tuple[int, List[int]]]]] = []

        while True:
            # read next line
            line = (lines[index + 1]).split()

            # exit
            if line[0] == 'END_STATE_LIST': 
                break
            
            # read cost state pair
            index, cost_state_pair = self.read_cost_state_pair(lines, index)

            # add cost state pair
            cost_state_pairs.append(cost_state_pair)

        return index, cost_state_pairs


    def read(self, gnn_data_file_path: Path) -> GnnData:
        gnn_data_file_path = Path(os.path.abspath(gnn_data_file_path))
        
        with gnn_data_file_path.open('r') as f: 
            lines = f.readlines()

        index = -1
        index, gnn_objects = self.read_objects(lines, index)
        index, gnn_predicates = self.read_predicates(lines, index)
        index, gnn_predicate_arity_tuples = self.read_predicate_arity_tuples(lines, index)
        index, gnn_facts = self.read_facts(lines, index)
        index, gnn_goals = self.read_goals(lines, index)
        index, gnn_cost_state_pairs = self.read_cost_state_pairs(lines, index)

        return GnnData(
            predicates=gnn_predicates,
            predicate_arity_tuples=gnn_predicate_arity_tuples,
            objects=gnn_objects,
            facts=gnn_facts,
            goals=gnn_goals,
            cost_state_pairs=gnn_cost_state_pairs
        )


if __name__ == "__main__":
    #path = Path('data/train/blocks-clear/test_clear_probBLOCKS-2-0_states.txt')
    path = Path('data/train/logistics-atomic/probLOGISTICS-4-0_0_states.txt')
    #path = Path('gnn-generator-output.txt')

    reader = GnnDataTxtReader()
    gnn_data = reader.read(path)
    print(gnn_data)