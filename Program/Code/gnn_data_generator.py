
from .sas_file_parser import SasFileParser
from .sas_cost_state_pairs_generator import SasCostStatePairsGenerator
from .gnn_data_adapter import GnnDataAdapter
from .pddl_file_reader import PddlFileReader
from .gnn_data_json_writer import GnnDataJsonWriter
from .utils import bcolors, seconds2hours_minutes_seconds
from .configs import SingleConfig, MultipleConfig

from pathlib import Path
import os
import time
import uuid
import subprocess


class GnnDataGenerator():
    def __init__(self) -> None:
        pass


    def single(self, config: SingleConfig) -> None:
        if config.info: print(f'Generating cost state pairs from [{bcolors.OKGREEN}{config.pddl_domain_instance_file_path}{bcolors.ENDC}]')

        unique_number = uuid.uuid4().hex

        # 0. Setup name for output and temporary files
        if config.output_file_name == '': config.output_file_name = os.path.basename(os.path.normpath(config.pddl_domain_instance_file_path)).replace('.pddl', '_gnn_data')
        

        # 1. Create sas file using Fast Downward planner
        if config.info: print(f'{bcolors.FAIL}1.{bcolors.ENDC} Creating [{config.output_file_name}_{unique_number}_output.sas] from [{config.pddl_domain_file_path}] pddl domain and [{config.pddl_domain_instance_file_path}] pddl domain instance using Fast Downward planner...')
        with open(f'{config.output_file_name}_{unique_number}_fast-downward-log.txt', mode='w') as file:
            subprocess.run(f'python {os.path.join(os.path.abspath(config.fastdownward_folder_path), "fast-downward.py")} --sas-file {config.output_file_name}_{unique_number}_output.sas --translate {os.path.abspath(config.pddl_domain_file_path)} {os.path.abspath(config.pddl_domain_instance_file_path)}{" --translate-options --keep-unimportant-variables " if not config.fastdownward_pruning else ""}', stdout=file, stderr=file, shell=True)


        # 2. Parse sas file
        if config.info: print(f'{bcolors.FAIL}2.{bcolors.ENDC} Parsing [{config.output_file_name}_{unique_number}_output.sas] into [sas_data]...')
        parser = SasFileParser()
        sas_data = parser.parse(Path(os.path.abspath(f'{config.output_file_name}_{unique_number}_output.sas')))


        # 3. Read pddl data in pddl domain file and pddl domain instance file
        if config.info: print(f'{bcolors.FAIL}3.{bcolors.ENDC} Reading [{config.pddl_domain_file_path}] pddl domain and [{config.pddl_domain_instance_file_path}] pddl domain instance into [pddl_data]...')
        reader = PddlFileReader()
        pddl_data = reader.read(Path(os.path.abspath(config.pddl_domain_file_path)), Path(os.path.abspath(config.pddl_domain_instance_file_path)))


        # 4. Generate new sas state cost pairs
        tic = time.perf_counter()
        if config.info: print(f'{bcolors.FAIL}4.{bcolors.ENDC} Creating cost state pairs for [sas_data] using Fast Downward planner...')
        generator = SasCostStatePairsGenerator(Path(os.path.abspath(config.fastdownward_folder_path)), config.output_file_name, config.timer, config.count, config.length, config.patience, config.info, unique_number)
        sas_data.cost_state_pairs = generator.generate(sas_data)

        if sas_data.cost_state_pairs != None:
            toc = time.perf_counter()
            if config.info: print('   ' + f'Created totally {bcolors.OKBLUE}{len(sas_data.cost_state_pairs)}{bcolors.ENDC} cost state pairs in {seconds2hours_minutes_seconds(toc-tic)}.')


            # 5. Adapt sas data into gnn form
            if config.info: print(f'{bcolors.FAIL}5.{bcolors.ENDC} Adapting [sas_data] and [pddl_data] into [gnn_data]...')
            adapter = GnnDataAdapter()
            gnn_data = adapter.adapt(sas_data, pddl_data)


            # 6. Write it into gnn form .json file
            if config.info: print(f'{bcolors.FAIL}6.{bcolors.ENDC} Writing [gnn_data] in folder [{config.output_folder_path}] in file [{config.output_file_name}]...')
            writer = GnnDataJsonWriter()
            writer.write(gnn_data, Path(os.path.abspath(config.output_folder_path)), config.output_file_name)
        else:
            if config.info: print(f'{bcolors.FAIL}5.{bcolors.ENDC} Dummy instance found, aborting (Fast Downward pruned variables and created dummy predicate, the init state equals the goal state, consider setting "fastdownward_pruning=false")...')

        # 7. Clean up
        # Clean up all temporary files 
        if config.clean_up:
            if config.info: print(f'{bcolors.FAIL}7.{bcolors.ENDC} Deleting all temporary files...')
            self.clean_up(config.output_file_name, unique_number)


    def multiple(self, config: MultipleConfig) -> None:

        # Find all pddl files in folder
        pddl_pathes = list(config.pddl_folder_path.glob('*.pddl'))
        
        # Pddl domain file
        pddl_domain_file_path = Path(os.path.join(config.pddl_folder_path, 'domain.pddl'))

        # Check whether domain file is in folder
        if pddl_domain_file_path not in pddl_pathes:
            raise ValueError('Provided pddl folder must contain domain file named [domain.pddl]')
        
        # Filter only pddl domain instances
        pddl_domain_instance_file_pathes = filter(lambda x: x != pddl_domain_file_path, pddl_pathes)
        
        # Run generate single for every problem
        for file_index, pddl_domain_instance_file_path in enumerate(pddl_domain_instance_file_pathes):
            if config.info: print(f'File number {file_index + 1}/{len(pddl_pathes) - 1}.')

            single_config = SingleConfig(
                mode='single', 
                fastdownward_folder_path=config.fastdownward_folder_path, 
                pddl_domain_file_path=pddl_domain_file_path, 
                pddl_domain_instance_file_path=pddl_domain_instance_file_path,
                output_folder_path=config.output_folder_path, 
                output_file_name='', 
                fastdownward_pruning=config.fastdownward_pruning, 
                timer=config.timer, 
                count=config.count,
                length=config.length,
                patience=config.patience,
                info=config.info,
                clean_up=config.clean_up
            )

            self.single(single_config)
            if config.info: print()


    def clean_up(self, output_file_name: str, unique_number: uuid.UUID) -> None:
        if os.path.exists(f'{output_file_name}_{unique_number}_output.sas'):
            os.remove(f'{output_file_name}_{unique_number}_output.sas')

        if os.path.exists(f'{output_file_name}_{unique_number}_fast-downward-log.txt'):
            os.remove(f'{output_file_name}_{unique_number}_fast-downward-log.txt')

        if os.path.exists(f'{output_file_name}_{unique_number}_modified_output.sas'):
            os.remove(f'{output_file_name}_{unique_number}_modified_output.sas')

        if os.path.exists(f'{output_file_name}_{unique_number}_modified_fast_downward_log.txt'):
            os.remove(f'{output_file_name}_{unique_number}_modified_fast_downward_log.txt')

        if os.path.exists(f'{output_file_name}_{unique_number}_modified_sas_plan'):
            os.remove(f'{output_file_name}_{unique_number}_modified_sas_plan')
