
from typing import List, Dict, Tuple
from .sas_file_structs import *

from pathlib import Path

class SasData:
    file_path: Path
    section_indices: Dict[str, Tuple[int, int]]
    version: int
    metric: int
    variables: List[Variable]
    mutex_groups: List[List[VariableState]]
    init_state: Dict[int, VariableState]
    goal_state: Dict[int, VariableState]
    operators: List[Operator]
    rules: List[Rule]
    cost_state_pairs: List[Tuple[float, Dict[int, VariableState]]]

    def __init__(
        self,
        file_path: Path,
        section_indices: Dict[str, Tuple[int, int]],   
        version: int,
        metric: int,
        variables: List[Variable],
        mutex_groups: List[List[VariableState]],
        init_state: Dict[int, VariableState],
        goal_state: Dict[int, VariableState],
        operators: List[Operator],
        rules: List[Rule],
        cost_state_pairs: List[Tuple[float, Dict[int, VariableState]]]
    ):
        self.file_path = file_path
        self.section_indices = section_indices
        self.version = version
        self.metric = metric
        self.variables = variables
        self.mutex_groups = mutex_groups
        self.init_state = init_state
        self.goal_state = goal_state
        self.operators = operators
        self.rules = rules
        self.cost_state_pairs = cost_state_pairs