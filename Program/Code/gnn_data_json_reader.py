
from .gnn_data import GnnData
from .gnn_data_reader import GnnDataReader

from pathlib import Path
import json
import os
from typing import List, Dict, Tuple, Any


class GnnDataJsonDecoder:
    def __init__(self) -> None:
        pass


    def decode_json_items_to_gnn_items(self, json_items: Dict[str, str]) -> Dict[int, str]:
        gnn_items: Dict[int, str] = {}

        for json_item_index, json_item_name in json_items.items():
            gnn_items[int(json_item_index)] = json_item_name

        return gnn_items
    
    
    def decode_json_objects_to_gnn_objects(self, json_objects: Dict[str, str]) -> Dict[int, str]:
        return self.decode_json_items_to_gnn_items(json_objects)
    

    def decode_json_predicates_to_gnn_predicates(self, json_predicates: Dict[str, str]) -> Dict[int, str]:
        return self.decode_json_items_to_gnn_items(json_predicates)
    

    def decode_json_predicate_arity_tuples_to_gnn_predicate_arity_tuples(self, json_predicate_arity_tuples: List[List[str]]) -> List[Tuple[int, int]]:
        gnn_predicate_arity_tuples: List[Tuple[int, int]] = []

        for predicate_value, predicate_arity in json_predicate_arity_tuples:
            gnn_predicate_arity_tuples.append((int(predicate_value), int(predicate_arity)))

        return gnn_predicate_arity_tuples

    def decode_json_predicate_states_to_gnn_predicate_states(self, json_predicate_states: List[Any]) -> List[Tuple[int, List[int]]]:
        gnn_predicate_states: List[Tuple[int, List[int]]] = []

        for json_predicate_index, json_predicate_objects in json_predicate_states:
            gnn_predicate_states.append((json_predicate_index, json_predicate_objects))

        return gnn_predicate_states
    

    def decode_json_facts_to_gnn_facts(self, json_facts: List[Any]) -> List[Tuple[int, List[int]]]:
        return self.decode_json_predicate_states_to_gnn_predicate_states(json_facts)
    

    def decode_json_goals_to_gnn_goals(self, json_goals: List[Any]) -> List[Tuple[int, List[int]]]:
        return self.decode_json_predicate_states_to_gnn_predicate_states(json_goals)


    def decode_json_cost_state_pairs_to_gnn_cost_state_pairs(self, json_cost_state_pairs: List[Any]) -> List[Tuple[float, List[Tuple[int, List[int]]]]]:
        gnn_cost_state_pairs: List[Tuple[float, List[Tuple[int, List[int]]]]] = []
        
        for json_cost, json_predicate_states in json_cost_state_pairs:
            gnn_predicate_states = self.decode_json_predicate_states_to_gnn_predicate_states(json_predicate_states)
            gnn_cost_state_pairs.append((json_cost, gnn_predicate_states))

        return gnn_cost_state_pairs


    def decode(self, json_gnn_data: Dict[str, Any]) -> GnnData:
        assert(len(json_gnn_data.keys()) == 6)
    
        gnn_objects = self.decode_json_objects_to_gnn_objects(json_gnn_data['objects'])
        gnn_predicates = self.decode_json_predicates_to_gnn_predicates(json_gnn_data['predicates'])
        gnn_predicate_arity_tuples = self.decode_json_predicate_arity_tuples_to_gnn_predicate_arity_tuples(json_gnn_data['predicate_arity_tuples'])
        gnn_facts = self.decode_json_facts_to_gnn_facts(json_gnn_data['facts'])
        gnn_goals = self.decode_json_goals_to_gnn_goals(json_gnn_data['goals'])
        gnn_cost_state_pairs = self.decode_json_cost_state_pairs_to_gnn_cost_state_pairs(json_gnn_data['cost_state_pairs'])
        
        return GnnData(
            predicates=gnn_predicates,
            predicate_arity_tuples=gnn_predicate_arity_tuples,
            objects=gnn_objects,
            facts=gnn_facts,
            goals=gnn_goals,
            cost_state_pairs=gnn_cost_state_pairs
        )


class GnnDataJsonReader(GnnDataReader):
    def __init__(self) -> None:
        pass
    
    
    def read(self, gnn_data_file_path: Path) -> GnnData:
        gnn_data_file_path = Path(os.path.abspath(gnn_data_file_path))

        with open(gnn_data_file_path, 'r') as f:
            json_gnn_data = json.load(f)
            gnn_data = GnnDataJsonDecoder().decode(json_gnn_data)

            return gnn_data