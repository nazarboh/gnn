
from .sas_file_structs import *
from .pddl_data import PddlData
from .sas_data import SasData
from .gnn_data import GnnData

from typing import List, Dict, Tuple

'''
Adapts sas data and pddl data into gnn data
'''
class GnnDataAdapter:
    def __init__(self):
        pass
    
    
    def adapt_items(self, items: List[str]) -> Dict[int, str]:
        gnn_items: Dict[int, str] = {}

        for index, item in enumerate(items):
            gnn_items[index] = item

        return gnn_items

    def adapt_predicates(self, pddl_predicates: List[str]) -> Dict[int, str]:
        return self.adapt_items(pddl_predicates)
    
    def adapt_predicate_arity_tuples(self, gnn_predicates: Dict[int, str], pddl_predicate_arity_tuples: List[Tuple[str, int]]) -> List[Tuple[int, int]]:
        gnn_predicate_arity_tuples: List[Tuple[int, int]] = []

        gnn_predicates_mapper: Dict[str, int] = {value: key for key, value in gnn_predicates.items()}

        for predicate_name, predicate_arity in pddl_predicate_arity_tuples:
            gnn_predicate_arity_tuples.append((gnn_predicates_mapper[predicate_name], predicate_arity))

        return gnn_predicate_arity_tuples
    

    def adapt_objects(self, pddl_objects: List[str]) -> Dict[int, str]:
        return self.adapt_items(pddl_objects)


    def adapt_facts(self, gnn_predicates: Dict[int, str], gnn_objects: Dict[int, str], pddl_init_predicate_states: List[Tuple[str, List[str]]], pddl_effect_predicates: List[str]) -> List[Tuple[int, List[int]]]:
        gnn_facts: List[Tuple[int, List[int]]] = []

        gnn_predicates_mapper: Dict[str, int] = {value: key for key, value in gnn_predicates.items()}
        gnn_objects_mapper: Dict[str, int] = {value: key for key, value in gnn_objects.items()}
        
        for i in range(len(pddl_init_predicate_states)):
            pddl_predicate, pddl_objects = pddl_init_predicate_states[i]
            
            if pddl_predicate not in pddl_effect_predicates:
                gnn_facts.append((gnn_predicates_mapper[pddl_predicate], [gnn_objects_mapper[pddl_object] for pddl_object in pddl_objects]))

        return gnn_facts
    

    def adapt_goals(self, gnn_predicates: Dict[int, str], gnn_objects: Dict[int, str], sas_variables: List[Variable], sas_goal_state: Dict[int, VariableState]) -> List[Tuple[int, List[int]]]:
        return self.adapt_state(gnn_predicates, gnn_objects, sas_variables, sas_goal_state)


    def adapt_state(self, gnn_predicates: Dict[int, str], gnn_objects: Dict[int, str], sas_variables: List[Variable], sas_state: Dict[int, VariableState]) -> List[Tuple[int, List[int]]]:
        gnn_state: List[Tuple[int, List[int]]] = []

        gnn_predicates_mapper: Dict[str, int] = {value: key for key, value in gnn_predicates.items()}
        gnn_objects_mapper: Dict[str, int] = {value: key for key, value in gnn_objects.items()}

        for variable_index, variable_state in sas_state.items():
            sas_atom = sas_variables[variable_state.variable_index].atoms[variable_state.atom_index]

            # get index of predicate from dictionary of unique predicates names
            if not sas_atom.none_of_those and not sas_atom.negated:
                gnn_state_predicate = gnn_predicates_mapper[sas_atom.predicate_name]

                # get indexes of objects from dictionary of unique object names
                gnn_state_objects = [gnn_objects_mapper[object] for object in sas_atom.predicate_objects]

                gnn_state.append((gnn_state_predicate, gnn_state_objects))

        return gnn_state


    def adapt_cost_state_pairs(self, gnn_predicates: Dict[int, str], gnn_objects: Dict[int, str], sas_variables: List[Variable],  sas_cost_state_pairs: List[Tuple[float, Dict[int, VariableState]]]) -> List[Tuple[float, List[Tuple[int, List[int]]]]]:
        gnn_cost_state_pairs: List[Tuple[float, List[Tuple[int, List[int]]]]] = []

        for cost, sas_state in sas_cost_state_pairs:
            gnn_state = self.adapt_state(gnn_predicates, gnn_objects, sas_variables, sas_state)
            gnn_cost_state_pairs.append((cost, gnn_state))

        return gnn_cost_state_pairs


    def adapt(self, sas_data: SasData, pddl_data: PddlData) -> GnnData:
        pddl_data.sort()
        
        gnn_predicates = self.adapt_predicates(pddl_data.predicates)
        gnn_predicate_arity_tuples = self.adapt_predicate_arity_tuples(gnn_predicates, pddl_data.predicate_arity_tuples)
        gnn_objects = self.adapt_objects(pddl_data.objects)
        gnn_facts = self.adapt_facts(gnn_predicates, gnn_objects, pddl_data.init_predicate_states, pddl_data.effect_predicates)
        gnn_goals = self.adapt_goals(gnn_predicates, gnn_objects, sas_data.variables, sas_data.goal_state)
        gnn_cost_state_pairs = self.adapt_cost_state_pairs(gnn_predicates, gnn_objects, sas_data.variables, sas_data.cost_state_pairs)

        return GnnData(
            predicates=gnn_predicates,
            predicate_arity_tuples=gnn_predicate_arity_tuples,
            objects=gnn_objects,
            facts=gnn_facts,
            goals=gnn_goals,
            cost_state_pairs=gnn_cost_state_pairs
        )
