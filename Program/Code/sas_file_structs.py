
from typing import List, Dict, Union


'''
    Describes Atom functionality from sas ouput file (output of fast downward planner)

    For more information: https://www.fast-downward.org/TranslatorOutputFormat
'''
class Atom:
    predicate_name : str
    predicate_objects : List[str]
    negated : bool
    none_of_those: bool

    def __init__(self, predicate_name: str, predicate_objects: List[str], negated: bool, none_of_those: bool) -> None:
        self.predicate_name = predicate_name
        self.predicate_objects = predicate_objects
        self.negated = negated
        self.none_of_those = none_of_those

    def __repr__(self) -> str:
        string = ""

        string += "{"
        string += " Atom: "

        if self.none_of_those:
            string += "<none of those>"    
            string += "}"
            return string

        string += "negated " if self.negated else ""
        string += f"{self.predicate_name}"
        string += "("

        for object in self.predicate_objects:
            string += object

        string += ")"
        string += "}"

        return string


'''
    Describes Variable functionality from sas ouput file (output of fast downward planner)

    For more information: https://www.fast-downward.org/TranslatorOutputFormat
'''
class Variable:
    variable_name: str
    variable_index: int
    axiom_layer: int
    range: int
    atoms: List[Atom]

    def __init__(self, variable_name: str, variable_index: int, axiom_layer: int, range: int, atoms: List[Atom]) -> None:
        self.variable_name = variable_name
        self.variable_index = variable_index
        self.axiom_layer = axiom_layer
        self.range = range
        self.atoms = atoms
    
    def __repr__(self) -> str:
        string = ""

        string += "{"
        string += " Variable:\n"
        
        for i in range(self.range):
            string += f"        {(self.atoms[i]).__repr__()}\n"
        
        string += "}"

        return string


'''
    Is helper class for Variable class:
    Variable contains bunch of atoms such that only one atom can be as state, so VariablesState do this

    For more information: https://www.fast-downward.org/TranslatorOutputFormat
'''
class VariableState:
    variable_index: int
    atom_index: int

    def __init__(self, variable_index: int, atom_index: int) -> None:
        self.variable_index = variable_index
        self.atom_index = atom_index

    def __repr__(self) -> str:
        string = ""

        string += "{"
        string += f" VariableState: Var {self.variable_index} is " + (f"Atom {self.atom_index}" if self.atom_index != None else "None")
        string += "}"

        return string

    def __eq__(self, other) -> bool:
        if isinstance(other, VariableState):
            return self.variable_index == other.variable_index and self.atom_index == other.atom_index
        return False

    def __hash__(self) -> int:
        return hash((self.variable_index, self.atom_index))


'''
    Helper class to describe "effects" of operator or rule 

    For more information: https://www.fast-downward.org/TranslatorOutputFormat
'''
class Effect:
    effect_conditions: List[VariableState]
    variable_index: int
    pre_condition_atom_index: int
    post_condition_atom_index: int

    def __init__(self, effect_conditions: List[VariableState], variable_index: int, pre_condition_atom_index: int, post_condition_atom_index: int) -> None:
        self.effect_conditions = effect_conditions
        self.variable_index = variable_index
        self.pre_condition_atom_index = pre_condition_atom_index
        self.post_condition_atom_index = post_condition_atom_index

    def __repr__(self) -> str:
        string = ""

        string += "{"
        string += f" Effect:"
        string += f" {self.effect_conditions}"
        string += f" Var {self.variable_index}: Atom {self.pre_condition_atom_index:2} ---> Atom {self.post_condition_atom_index:2}"

        string += "}"

        return string


'''
    Describes Operator functionality from sas ouput file (output of fast downward planner)

    For more information: https://www.fast-downward.org/TranslatorOutputFormat
'''
class Operator:
    operator_name: str
    objects: List[str]
    prevail_conditions: List[VariableState]
    effects: List[Effect]
    operator_cost: float

    def __init__(self, operator_name: str, objects: List[str], prevail_conditions: List[VariableState], effects: List[Effect], operator_cost: float) -> None:
        self.operator_name = operator_name
        self.objects = objects
        self.prevail_conditions = prevail_conditions
        self.effects = effects
        self.cost = operator_cost

    def __repr__(self) -> str:
        string = ""

        string += "{"
        string += f" Operator:\n"
        string += f"        {self.operator_name} {self.objects}\n"
        string += f"        {self.prevail_conditions}\n"

        for effect in self.effects:
            string += f"        {effect}\n"

        string += f"        {self.cost}\n"

        string += "}"

        return string

    def apply(self, state : Dict[int, VariableState]) -> Union[Dict[int, VariableState], None]:
        # make a copy of state and modify it later
        new_state : Dict[int, VariableState] = {}
        for variable_index, variable_state in state.items():
            new_state[variable_index] = VariableState(variable_state.variable_index, variable_state.atom_index)

        # check safisfaction of prevail conditions
        for prevail_condition in self.prevail_conditions:
            if prevail_condition not in state.values():
                return None

        # effects
        for effect in self.effects:
            # check effect conditions
            for effect_condition in effect.effect_conditions:
                # TODO, ignoring for now
                raise NotImplementedError("Error in apply method inside of Operator class, effect_condition functionality is not implemented")

            # check precondition
            if effect.pre_condition_atom_index == state[effect.variable_index].atom_index or effect.pre_condition_atom_index == -1:
                new_state[effect.variable_index].atom_index = effect.post_condition_atom_index
            else:
                return None

        return new_state


'''
    Describes Rule functionality from sas ouput file (output of fast downward planner)

    For more information: https://www.fast-downward.org/TranslatorOutputFormat
'''
class Rule:
    conditions: List[VariableState]
    variable_index: int
    pre_condition_atom_index: int
    post_condition_atom_index: int

    def __init__(self, conditions: List[VariableState], variable_index: int, pre_condition_atom_index: int, post_condition_atom_index: int) -> None:
        self.conditions = conditions
        self.variable_index = variable_index
        self.pre_condition_atom_index = pre_condition_atom_index
        self.post_condition_atom_index = post_condition_atom_index

    def __repr__(self) -> str:
        string = ""

        string += "{"
        string += f" Rule:"
        string += f" {self.conditions}"
        string += f" Var {self.variable_index}: Atom {self.pre_condition_atom_index:2} ---> Atom {self.post_condition_atom_index:2}"

        string += "}"

        return string
