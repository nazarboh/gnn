
import os
from argparse import Namespace, ArgumentParser
from types import SimpleNamespace
from pathlib import Path
import json

from typing import List, Dict, Tuple


def get_problem_names() -> List[str]:
    return [
        'BlocksClear',
        'BlocksOn',
        'Gripper',
        'Logistics',
        
        'Miconic',
        'ParkingBehind',
        'ParkingCurb',
        'Pathing',

        'Rover',
        'Satellite',
        'Transport',
        'Visitall'
    ]


def get_tuple_problem_names() -> List[Tuple[str, str]]:
    return [
        ('BlocksClear', 'blocks_clear'),
        ('BlocksOn', 'blocks_on'),
        ('Gripper', 'gripper'),
        ('Logistics', 'logistics'),

        ('Miconic', 'miconic'),
        ('ParkingBehind', 'parking_behind'),
        ('ParkingCurb', 'parking_curb'),
        ('Pathing', 'pathing'),
        
        ('Rover', 'rover'),
        ('Satellite', 'satellite'),
        ('Transport', 'transport'),
        ('Visitall', 'visitall')
    ]


def get_problem_types() -> List[str]:
    return ['Test', 'Train', 'Validation']


def get_model_types() -> List[str]:
    return ['add', 'max']


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def str2bool(v) -> bool:
    if isinstance(v, bool):
        return v
    if v == 'True' or v == 'true':
        return True
    elif v == 'False' or v == 'false':
        return False
    else:
        raise ValueError('Boolean value expected')
    

def seconds2hours_minutes_seconds(seconds: int) -> str:
    seconds = seconds % (24 * 3600)
    hours = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
     
    return f'{int(hours)} hour(s) {int(minutes)} minute(s) {int(seconds)} second(s)'


def get_next_experiment_version(experiment_folder_path: str, prefix_name: str) -> int:
    if not os.path.isdir(experiment_folder_path):
        print(f'Missing expirement folder: {experiment_folder_path}')
        return 0

    existing_versions = []
    for d in os.listdir(experiment_folder_path):
        if os.path.isdir(os.path.join(experiment_folder_path, d)) and d.startswith(str(prefix_name)):
            existing_versions.append(int(d.split("_")[1]))

    if len(existing_versions) == 0:
        return 0

    return max(existing_versions) + 1


def parse_arguments() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument('--config', required=True, type=Path, help='Full or relative path to train, resume, test or predict config JSON file')
    arguments = parser.parse_args()

    return arguments


def read_config_json(config_json_file_path: Path) -> SimpleNamespace:
    config_json_file_path = Path(os.path.abspath(config_json_file_path))

    with open(config_json_file_path, 'r') as f:
        return SimpleNamespace(**json.load(f))
