
from .sas_file_structs import *
from .sas_data import SasData

from typing import Dict, Tuple, List
from pathlib import Path

'''
Parses all sections of fast downward planner's sas file output 

    1. Version section
    2. Metric section
    3. Variables section
    4. Mutex section
    5. Initial state section
    6. Goal section
    7. Operator section
    8. Axiom section

Uses structs from sas_file_structs.py

For more information: https://www.fast-downward.org/TranslatorOutputFormat
'''
class SasFileParser:
    def __init__(self) -> None:
        pass
    
    
    '''
    Parses version section in fast downward planner's sas file output 
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            begin_version
            3
            end_version
        "
        Output: (new_index, 3)
    '''
    def parse_version(self, index: int, file: List[str]) -> Tuple[int, int]:
        # Error
        index += 1
        line = file[index]
        if line != "begin_version": raise Exception("begin_version")

        # Parse version
        index += 1
        version = int(file[index])

        # Error
        index += 1
        line = file[index]
        if line != "end_version": raise Exception("end_version")

        return index, version


    '''
    Parses metric section in fast downward planner's sas file output 
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            begin_metric
            0
            end_metric
        "
        Output: (new_index, 0)
    '''
    def parse_metric(self, index: int, file: List[str]) -> Tuple[int, int]:
        # Error
        index += 1
        line = file[index]
        if line != "begin_metric": raise Exception("begin_metric")

        # Parse metric
        index += 1
        metric = int(file[index])

        # Error
        index += 1
        line = file[index]
        if line != "end_metric": raise Exception("end_metric")

        return index, metric

    
    '''
    Parses predicate name and objects in atom
        Input:
        string (the part after Atom or NegatedAtom)

        Will be parsed:
        "
            carry(ball1, right)
        "
        Output: ('carry', ['ball1', 'right'])
    '''
    def parse_predicate(self, string: str) -> Tuple[str, List[str]]:
        string_splitted = string.replace('(', ' ').replace(')', ' ').replace(',', ' ').split()

        predicate_name = string_splitted[0]
        predicate_objects = string_splitted[1:]

        return predicate_name, predicate_objects


    '''
    Parses one atom in variable
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            Atom at(ball4, roomb)
        "
        OR
        "
            NegatedAtom at(ball4, roomb)
        "
        OR
        "
            <none of those>
        "

        Output: (new_index, Atom) (see sas_file_structs.py for Atom definition)
    '''
    def parse_atom(self, index: int, file: List[str]) -> Tuple[int, Atom]:
        index += 1
        line = file[index]

        # Parse no type of atom
        if line == '<none of those>':
            # Create a Atom instance using parsed information
            return (index, Atom(None, None, None, none_of_those=True))

        splitted = line.split()

        # Parse type of Atom
        atom_type = splitted[0]
        predicate_line = ' '.join(splitted[1:])

        # Choose type of atom
        if atom_type == 'Atom':
            predicate_name, predicate_objects = self.parse_predicate(predicate_line)

            # Create a Atom instance using parsed information
            return (index, Atom(predicate_name, predicate_objects, negated=False, none_of_those=False))
        elif atom_type == 'NegatedAtom':
            predicate_name, predicate_objects = self.parse_predicate(predicate_line)

            # Create a Atom instance using parsed information
            return (index, Atom(predicate_name, predicate_objects, negated=True, none_of_those=False))

        raise Exception("Unknown type of atom in parse_atom function")


    '''
    Parses one variable in variable section
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            begin_variable
            var0
            -1
            5
            Atom carry(ball1, right)
            Atom carry(ball2, right)
            Atom carry(ball3, right)
            Atom free(right)
            Atom carry(ball4, right)
            end_variable
        "
        Output: (new_index, Variable) (see sas_file_structs.py for Variable definition)
    '''
    def parse_variable(self, index: int, file: List[str]) -> Tuple[int, Variable]:
        # Error
        index += 1
        line = file[index]
        if line != "begin_variable": raise Exception("begin_variable")

        # Parse variable name and get variable index from there
        index += 1
        variable_name = str(file[index])
        variable_index = int(variable_name[3:])
        
        # Parse axiom layer
        index += 1
        axiom_layer = int(file[index])

        # Parse count of atoms
        index += 1
        variable_range = int(file[index])
        
        # Parse every atom
        atoms = []
        for i in range(variable_range):
            index, atom = self.parse_atom(index, file)
            atoms.append(atom)

        # Create a Variable instance using parsed information
        variable = Variable(variable_name, variable_index, axiom_layer, variable_range, atoms)

        # Error
        index += 1
        line = file[index]
        if line != "end_variable": raise Exception("end_variable")

        return index, variable


    '''
    Parses variable section in fast downward planner's sas file output
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            2
            begin_variable
            var0
            -1
            5
            Atom carry(ball1, right)
            Atom carry(ball2, right)
            Atom carry(ball3, right)
            Atom free(right)
            Atom carry(ball4, right)
            end_variable
            begin_variable
            var1
            -1
            5
            Atom carry(ball3, left)
            Atom free(left)
            Atom carry(ball2, left)
            Atom carry(ball1, left)
            Atom carry(ball4, left)
            end_variable
        "
        Output: (new_index, [Variable]) (see sas_file_structs.py for Variable definition)
    '''
    def parse_variables(self, index: int, file: List[str]) -> Tuple[int, List[Variable]]:
        # Parse count of variables
        index += 1
        variable_count = int(file[index])
        
        # Parse every variable
        variables = []
        for i in range(variable_count):
            index, variable = self.parse_variable(index, file)
            variables.append(variable)

        return index, variables


    '''
    Parses variable states 
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            4
            1 4
            0 4
            2 0
            2 1
        "
        Output: (new_index, [VariableState]) (see sas_file_structs.py for VariableState definition)
    '''
    def parse_variables_states(self, index: int, file: List[str]) -> Tuple[int, List[VariableState]]:
        # Parse count of variable states
        index += 1
        number_of_facts = int(file[index])

        # Parse every variable state
        variables_states = []
        for i in range(number_of_facts):
            index += 1
            line = file[index]
            splitted_line = line.split(" ")

            # Parse variable index 
            variable_index = int(splitted_line[0])

            # Parse atom index
            atom_index = int(splitted_line[1])

            # Create a VariableState instance using parsed information and fill a list
            variables_states.append(VariableState(variable_index, atom_index))

        return index, variables_states


    '''
    Parses one mutex group in mutex section
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            begin_mutex_group
            4
            1 4
            0 4
            2 0
            2 1
            end_mutex_group
        "
        Output: (new_index, [VariableState]) (see sas_file_structs.py for VariableState definition)
    '''
    def parse_mutex_group(self, index: int, file: List[str]) -> Tuple[int, List[VariableState]]:
        # Error
        index += 1
        line = file[index]
        if line != "begin_mutex_group": raise Exception("begin_mutex_group")

        # Parse mutex group
        index, mutex_group = self.parse_variables_states(index, file)

        # Error
        index += 1
        line = file[index]
        if line != "end_mutex_group": raise Exception("end_mutex_group")

        return index, mutex_group
        

    '''
    Parses mutex section in fast downward planner's sas file output
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            2
            begin_mutex_group
            4
            1 4
            0 4
            2 0
            2 1
            end_mutex_group
            begin_mutex_group
            4
            1 0
            0 2
            3 0
            3 1
            end_mutex_group
        "
        Output: (new_index, [[VariableState]]) (see sas_file_structs.py for VariableState definition)
    '''
    def parse_mutex_groups(self, index: int, file: List[str]) -> Tuple[int, List[List[VariableState]]]:
        # Parse count of mutex groups
        index += 1
        mutex_group_count = int(file[index])

        # Parse every mutex group
        mutex_groups = []
        for i in range(mutex_group_count):
            index, mutex_group = self.parse_mutex_group(index, file)
            mutex_groups.append(mutex_group)

        return index, mutex_groups


    '''
    Parses initial state section in fast downward planner's sas file output 
        Input:
        index,
        file (list of lines from file),
        variable_count

        Will be parsed:
        "
            begin_state
            3
            1
            0
            0
            0
            0
            1
            end_state
        "
        Output: (new_index, {int: VariableState}) (see sas_file_structs.py for VariableState definition)
    '''
    def parse_begin_state(self, index: int, file: List[str], variable_count: int) -> Tuple[int, Dict[int, VariableState]]:
        # Error
        index += 1
        line = file[index]
        if line != "begin_state": raise Exception("begin_state")

        # Parse every init variable state
        init_state = {}
        for i in range(variable_count):
            # Parse atom index
            index += 1
            atom_index = int(file[index])

            # Fill the dictionary with key as variable index and using parsed information VariableState instance as value
            init_state[i] = VariableState(i, atom_index)

        # Error
        index += 1
        line = file[index]
        if line != "end_state": raise Exception("end_state")

        return index, init_state


    '''
    Parses goal section in fast downward planner's sas file output 
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            begin_goal
            4
            2 1
            3 1
            4 1
            5 1
            end_goal
        "
        Output: (new_index, {int: VariableState}) (see sas_file_structs.py for VariableState definition)
    '''
    def parse_goal_state(self, index: int, file: List[str]) -> Tuple[int, Dict[int, VariableState]]:
        # Error
        index += 1
        line = file[index]
        if line != "begin_goal": raise Exception("begin_goal")
        
        # Parse variable states
        index, goal_variables_states = self.parse_variables_states(index, file)

        # Convert into dictionary
        goal_state = {}
        for i in range(len(goal_variables_states)):
            goal_state[goal_variables_states[i].variable_index] = goal_variables_states[i]

        # Error
        index += 1
        line = file[index]
        if line != "end_goal": raise Exception("end_goal")

        return index, goal_state


    '''
    Parses the header of the operator
        Input:
        string

        Will be parsed:
        "
            move rooma roomb
        "
        Output: (new_index, string, [string]) (index, 'move, ['rooma', 'roomb'])
    '''
    def parse_operator_header(self, index: int, file: List[str]) -> Tuple[int, str, List[str]]:
        index += 1
        line = file[index]
        splitted_line = line.split(' ')

        # Parse operator name
        operator_name = splitted_line[0]

        # Parse objects of the operator
        objects = splitted_line[1:]

        return index, operator_name, objects


    '''
    Parses one effect in effects
        Input:
        string

        Will be parsed:
        "
            1 29 1 29 -1 0
        "
        Output: ([VarialbeState], str) ('1 29 1' will be parsed and '29 -1 0' wont, also see sas_file_structs.py for VarialbeState definition)
    '''
    def parse_effect_conditions(self, line: str) -> Tuple[List[VariableState], str]:
        line_splitted = line.split()

        # Parse count of effect conditions
        effect_conditions_count = int(line_splitted[0])

        # Delete parsed count of effect conditions
        line_splitted = line_splitted[1:]

        # Parse every effect condition
        effect_conditions = [] 
        for i in range(effect_conditions_count):
            # Parse variable index
            variable_index = int(line_splitted[0])

            # Parse atom index
            atom_index = int(line_splitted[1])

            # Delete parsed variable index and atom index
            line_splitted = line_splitted[2:]

            # Create a VariableState instance using parsed information and fill a list
            effect_condition = VariableState(variable_index, atom_index)
            effect_conditions.append(effect_condition)
        
        # Rest of the line
        new_line = ' '.join(line_splitted)

        return effect_conditions, new_line


    '''
    Parses effected variable index, effected pre condition atom index and result (post) condition atom index
        Input:
        string

        Will be parsed:
        "
            29 -1 0
        "
        Output: (int, int, int, str) (str should be empty after)
    '''
    def parse_effect_variable_state(self, line: str) -> Tuple[int, int, int, str]:
        line_splitted = line.split()

        # Parse effected variable index
        variable_index = int(line_splitted[0])

        # Parse pre condition atom index
        pre_condition_atom_index = int(line_splitted[1])
        line_splitted = line_splitted[2:]

        # Parse post result (post) condition atom index
        post_condition_atom_index = int(line_splitted[0])
        line_splitted = line_splitted[1:]

        # Rest of the line
        new_line = ' '.join(line_splitted)

        return variable_index, pre_condition_atom_index, post_condition_atom_index, new_line


    '''
    Parses one effect in effects
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            1 29 1 29 -1 0
        "
        Output: (new_index, Effect) (see sas_file_structs.py for Effect definition)
    '''
    def parse_effect(self, index: int, file: List[str]) -> Tuple[int, Effect]:
        index += 1
        line = file[index]
        
        # Parse effect conditions in effect
        effect_conditions, line = self.parse_effect_conditions(line)

        # Parse effected variable index, effected pre condition atom index and result (post) condition atom index
        variable_index, pre_condition_atom_index, post_condition_atom_index, line = self.parse_effect_variable_state(line)

        assert(len(line) == 0)

        # Create a Effect instance using parsed information
        effect = Effect(effect_conditions, variable_index, pre_condition_atom_index, post_condition_atom_index)

        return index, effect


    '''
    Parses effects in one operator
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            4
            0 24 1 0
            0 3 -1 0
            1 29 1 29 -1 0
            0 22 1 0
        "
        Output: (new_index, [Effect]) (see sas_file_structs.py for Effect definition)
    '''
    def parse_effects(self, index: int, file: List[str]) -> Tuple[int, List[Effect]]:
        # Parse count of effects in operator
        index += 1
        effect_count = int(file[index])

        # Parse every effect
        effects = []
        for i in range(effect_count):
            index, effect = self.parse_effect(index, file)
            effects.append(effect)

        return index, effects


    '''
    Parses one operator in operator section
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            begin_operator
            do-polish a0
            1
            7 0
            4
            0 24 1 0
            0 3 -1 0
            1 29 1 29 -1 0
            0 22 1 0
            7
            end_operator
        "
        Output: (new_index, Operator) (see sas_file_structs.py for Operator definition)
    '''
    def parse_operator(self, index: int, file: List[str]) -> Tuple[int, Operator]:
        # Error
        index += 1
        line = file[index]
        if line != "begin_operator": raise Exception("begin_operator")

        # Parse the header of the operator
        index, operator_name, objects = self.parse_operator_header(index, file)

        # Parse prevail conditions of the operator
        index, prevail_conditions = self.parse_variables_states(index, file)

        # Parse effects of the operator
        index, effects = self.parse_effects(index, file)

        # Parse cost of the operator
        index += 1
        operator_cost = float(file[index])

        # Create a Operator instance using parsed information
        operator = Operator(operator_name, objects, prevail_conditions, effects, operator_cost) 

        # Error
        index += 1
        line = file[index]
        if line != "end_operator": raise Exception("end_operator")

        return index, operator


    '''
    Parses operator section in fast downward planner's sas file output 
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            2
            begin_operator
            move rooma roomb
            0
            1
            0 6 1 0
            0
            end_operator
            begin_operator
            pick ball4 rooma left
            1
            6 1
            2
            0 1 1 4
            0 2 0 2
            0
            end_operator
        "
        Output: (new_index, [Operator]) (see sas_file_structs.py for Operator definition)
    '''
    def parse_operators(self, index: int, file: List[str]) -> Tuple[int, List[Operator]]:
        # Parse count of operators
        index += 1
        operator_count = int(file[index])

        # Parse every operator
        operators = []
        for i in range(operator_count):
            index, operator = self.parse_operator(index, file)
            operators.append(operator)

        return index, operators


    '''
    Parses one rule in rule section
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            begin_rule
            2
            1 0
            3 0
            5 0 1
            end_rule
        "
        Output: (new_index, Rule) (see sas_file_structs.py for Rule definition)
    '''
    def parse_rule(self, index: int, file: List[str]) -> Tuple[int, Rule]:
        # Error
        index += 1
        line = file[index]
        if line != "begin_rule": raise Exception("begin_rule")

        # Parse variable states
        index, conditions = self.parse_variables_states(index, file)

        # Parse rule's effect
        index += 1
        line = file[index]
        variable_index, pre_condition_atom_index, post_condition_atom_index, new_line = self.parse_effect_variable_state(line)

        assert(len(new_line) == 0)

        # Create a Rule instance using parsed information 
        rule = Rule(conditions, variable_index, pre_condition_atom_index, post_condition_atom_index)

        # Error
        index += 1
        line = file[index]
        if line != "end_rule": raise Exception("end_rule")

        return index, rule


    '''
    Parses rule section in fast downward planner's sas file output 
        Input:
        index,
        file (list of lines from file)

        Will be parsed:
        "
            1
            begin_rule
            2
            1 0
            3 0
            5 0 1
            end_rule
        "
        Output: (new_index, [Rule]) (see sas_file_structs.py for Rule definition)
    '''
    def parse_rules(self, index: int, file: List[str]) -> Tuple[int, List[Rule]]:
        # Parse count of rules
        index += 1
        rule_count = int(file[index])

        # Parse every rule
        rules = []
        for i in range(rule_count):
            index, rule = self.parse_rule(index, file)
            rules.append(rule)

        return index, rules


    '''
    Parses fast downward planner's output sas file 
        Input:
        file path to fast downward sas output file

        Output: dictionary with all 8 sections of sas file
    '''
    def parse(self, sas_file_path: Path) -> SasData:
        with sas_file_path.open('r') as fs: lines = [line.strip() for line in fs.readlines()]

        index = -1

        # save indices of sections
        section_indices = {}

        previous_index = index
        index, version = self.parse_version(index, lines)
        section_indices['version'] = (previous_index + 1, index)

        previous_index = index
        index, metric = self.parse_metric(index, lines)
        section_indices['metric'] = (previous_index + 1, index)

        previous_index = index
        index, variables = self.parse_variables(index, lines)
        section_indices['variables'] = (previous_index + 1, index)

        previous_index = index
        index, mutex_groups = self.parse_mutex_groups(index, lines)
        section_indices['mutex_groups'] = (previous_index + 1, index)

        previous_index = index
        index, init_state = self.parse_begin_state(index, lines, len(variables))
        section_indices['init_state'] = (previous_index + 1, index)

        previous_index = index
        index, goal_state = self.parse_goal_state(index, lines)
        section_indices['goal_state'] = (previous_index + 1, index)

        previous_index = index
        index, operators = self.parse_operators(index, lines)
        section_indices['operators'] = (previous_index + 1, index)

        previous_index = index
        index, rules = self.parse_rules(index, lines)
        section_indices['rules'] = (previous_index + 1, index)

        return SasData(
            file_path=sas_file_path,
            section_indices=section_indices,
            version=version,
            metric=metric,
            variables=variables,
            mutex_groups=mutex_groups,
            init_state=init_state,
            goal_state=goal_state,
            operators=operators,
            rules=rules,
            cost_state_pairs=[]
        )
    

if __name__ == "__main__":
    path = Path('output.sas')

    parser = SasFileParser()
    sas_data = parser.parse(path)
    print(sas_data)
