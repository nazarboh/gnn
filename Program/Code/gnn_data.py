
from typing import List, Dict, Tuple

class GnnData:
    predicates: Dict[int, str]
    predicate_arity_tuples: List[Tuple[int, int]]
    objects: Dict[int, str]
    facts: List[Tuple[int, List[int]]]
    goals: List[Tuple[int, List[int]]]
    cost_state_pairs: List[Tuple[float, List[Tuple[int, List[int]]]]]

    def __init__(
        self,
        predicates: Dict[int, str],
        predicate_arity_tuples: List[Tuple[int, int]],
        objects: Dict[int, str],
        facts: List[Tuple[int, List[int]]],
        goals: List[Tuple[int, List[int]]],
        cost_state_pairs: List[Tuple[float, List[Tuple[int, List[int]]]]]
    ):
        self.predicates = predicates
        self.predicate_arity_tuples = predicate_arity_tuples
        self.objects = objects
        self.facts = facts
        self.goals = goals
        self.cost_state_pairs = cost_state_pairs
