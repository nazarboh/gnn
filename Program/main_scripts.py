
from Code.gnn_data import GnnData
from Code.gnn_data_txt_reader import GnnDataTxtReader
from Code.gnn_data_txt_writer import GnnDataTxtWriter
from Code.gnn_data_json_reader import GnnDataJsonReader
from Code.gnn_data_json_writer import GnnDataJsonWriter
from Code.pddl_file_reader import PddlFileReader
from Code.utils import *
from Code.pddl_data import PddlData


from pathlib import Path
import json
import os
import uuid

from Code.gnn_dataset import GnnDataset, collate_batch_of_gnn_dataset_state_cost_pairs, collate_batch_of_gnn_dataset_states
import sys
import torch
from Code.utils import *
import os
import zipfile
import uuid


# def create_domain_folders(folder_path: str):
#     problem_names = get_problem_names()
#     problem_folder_types = get_problem_folder_types()

#     for problem_name in problem_names:
#         for problem_folder_type in problem_folder_types:
            
#             path = Path(folder_path + '/' + problem_name + '/' + problem_folder_type)

#             if not os.path.exists(path):
#                 print('Creating ' + str(path))
#                 os.makedirs(path)


# def unzip_in_doimain_folders(folder_path: str):
#     problem_names = get_problem_names()
#     problem_folder_types = get_problem_folder_types()


#     for problem_name in problem_names:
#         for problem_folder_type in problem_folder_types:

#             path = Path(folder_path + '/' + problem_name + '/' + problem_folder_type)
#             os.chdir(path) # change directory from working dir to dir with files

#             for item in os.listdir(path): # loop through items in dir
#                 if item.endswith('.zip'): # check for ".zip" extension
#                     print(item)
#                     file_name = os.path.abspath(item) # get full path of files
#                     zip_ref = zipfile.ZipFile(file_name) # create zipfile object
#                     zip_ref.extractall(path) # extract file to dir
#                     zip_ref.close() # close file
#                     os.remove(file_name) # delete zipped file
            

# def rename_pddls_in_domain_folders(folder_path: str):
#     reader = PddlFileReader()

#     tuple_problem_names = [('Rover', 'rover')] # get_tuple_problem_names()
#     folder_types = get_problem_folder_types()

#     for problem_name, problem_name_for_file in tuple_problem_names:
#         for folder_type in folder_types:
#             print(f'{problem_name} {folder_type}')

#             path = Path(folder_path + '/'+ problem_name + '/' + folder_type)
#             pddl_domain_file_path = Path(str(path) + '/' + 'domain.pddl')

#             same_name_counts = {}

#             for file_name in path.glob('*.pddl'):
#                 new_file_name = ''
#                 count_of_objects = -1
#                 pddl_domain_instance_file_path = Path(file_name)

#                 if pddl_domain_file_path == pddl_domain_instance_file_path:
#                     continue

#                 pddl_data: PddlData = reader.read(pddl_domain_file_path, pddl_domain_instance_file_path)
#                 count_of_objects = len(pddl_data.objects)
                
#                 if count_of_objects <= 0:
#                     raise Exception('Something went wrong')


#                 if count_of_objects in same_name_counts.keys():
#                     same_name_counts[count_of_objects] += 1
#                 else:
#                     same_name_counts[count_of_objects] = 0

#                 new_file_name = str(path) + '/' + problem_name_for_file + '_' + str(count_of_objects) + '_' + str(same_name_counts[count_of_objects]) + '.pddl'


#                 if str(file_name) != str(new_file_name):
#                     if os.path.exists(Path(new_file_name)):
#                         raise Exception('Trying to rename file to existiting file name')
#                     print(f'{file_name} -> {new_file_name}')
#                     os.rename(file_name, new_file_name)
#         print()
#     print('Done.')


# def rename_txts_in_domain_folders(folder_path: str):
#     reader = GnnDataTxtReader()

#     tuple_problem_names = get_tuple_problem_names()
#     folder_types = get_problem_folder_types()

#     for problem_name, problem_name_for_file in tuple_problem_names:
#         for folder_type in folder_types:
#             print(f'{problem_name} {folder_type}')

#             path = Path(folder_path + '/'+ problem_name + '/' + folder_type)
#             same_name_counts = {}

#             for file_name in path.glob('*_states.txt'):
#                 new_file_name = ''
#                 count_of_objects = -1
#                 gnn_data_file_path = Path(file_name)


#                 gnn_data: GnnData = reader.read(gnn_data_file_path)
#                 count_of_objects = len(gnn_data.objects)
                
#                 if count_of_objects <= 0:
#                     raise Exception('Something went wrong')


#                 if count_of_objects in same_name_counts.keys():
#                     same_name_counts[count_of_objects] += 1
#                 else:
#                     same_name_counts[count_of_objects] = 0

#                 new_file_name = str(path) + '/' + problem_name_for_file + '_' + str(count_of_objects) + '_' + str(same_name_counts[count_of_objects]) + '_gnn_data.txt'


#                 if str(file_name) != str(new_file_name):
#                     if os.path.exists(Path(new_file_name)):
#                         raise Exception('Trying to rename file to existiting file name')
#                     print(f'{file_name} -> {new_file_name}')
#                     os.rename(file_name, new_file_name)
#         print()
#     print('Done.')


def create_jsons_of_txts(problem_names: List[str], problem_types: List[str]):
    reader = GnnDataTxtReader()
    writer = GnnDataJsonWriter()

    for problem_name in problem_names:
        for problem_type in problem_types:
            print(f'{problem_name} {problem_type}')

            path_read_from = Path(os.path.join('Data', 'Txt' , problem_name, problem_type))

            for file_name_read in path_read_from.glob('*_gnn_data.txt'):
                gnn_data = reader.read(file_name_read)

                write_folder_path = Path(os.path.join('Data', 'Json' , problem_name, problem_type))
                write_file_name = os.path.basename(os.path.normpath(file_name_read)).replace('.txt', '')
                print('     ' + os.path.join(write_folder_path, write_file_name))
                writer.write(gnn_data, write_folder_path, write_file_name)
        print()
    print('Done.')


def create_txts_of_jsons(problem_names: List[str], problem_types: List[str]):
    reader = GnnDataJsonReader()
    writer = GnnDataTxtWriter()

    for problem_name in problem_names:
        for problem_type in problem_types:
            print(f'{problem_name} {problem_type}')

            path_read_from = Path(os.path.join('Data', 'Json' , problem_name, problem_type))

            for file_name_read in path_read_from.glob('*_gnn_data.json'):
                gnn_data = reader.read(file_name_read)

                write_folder_path = Path(os.path.join('Data', 'Txt' , problem_name, problem_type))
                write_file_name = os.path.basename(os.path.normpath(file_name_read)).replace('.json', '')
                print('     ' + os.path.join(write_folder_path, write_file_name))
                writer.write(gnn_data, write_folder_path, write_file_name)
        print()
    print('Done.')


# from Code.gnn_data_txt_reader_old import GnnDataTxtReader as GnnDataTxtReaderOld
# from Code.gnn_data_txt_writer_old import GnnDataTxtWriter as GnnDataTxtWriterOld
# from Code.gnn_data_json_reader_old import GnnDataJsonReader as GnnDataJsonReaderOld
# from Code.gnn_data_old import GnnData as GnnDataOld
# from Code.gnn_data_adapter import GnnDataAdapter

# def convert_old_jsons_into_new_jsons(problem_names: List[str], problem_types: List[str]):
#     reader = GnnDataJsonReaderOld()
#     writer = GnnDataTxtWriter()
#     adapter = GnnDataAdapter()

#     for problem_name in problem_names:
#         pddl_folder_path = Path(os.path.join('Data', 'Pddl', problem_name, 'Test'))
#         pddl_domain_file_path = Path(os.path.join(pddl_folder_path, 'domain.pddl'))
#         pddl_domain_instance_file_path = Path(list(pddl_folder_path.glob('*0.pddl'))[0])

#         pddl_data: PddlData = PddlFileReader().read(pddl_domain_file_path, pddl_domain_instance_file_path)

#         for problem_type in problem_types:
#             print(f'{problem_name} {problem_type}')

#             path_read_from = Path(os.path.join('Data', 'Json' , problem_name, problem_type))

#             for file_name_read in path_read_from.glob('*_gnn_data.json'):
#                 gnn_data: GnnDataOld = reader.read(file_name_read)

#                 new_gnn_data = GnnData(
#                     predicates=gnn_data.predicates,
#                     predicate_arity_tuples=adapter.adapt_predicate_arity_tuples(gnn_data.predicates, pddl_data.predicate_arity_tuples),
#                     objects=gnn_data.objects,
#                     facts=gnn_data.facts,
#                     goals=gnn_data.goals,
#                     cost_state_pairs=gnn_data.cost_state_pairs
#                 )

#                 write_folder_path = Path(os.path.join('Data', 'Txt' , problem_name, problem_type))
#                 write_file_name = os.path.basename(os.path.normpath(file_name_read)).replace('.json', '')
#                 print('     ' + os.path.join(write_folder_path, write_file_name))
#                 writer.write(new_gnn_data, write_folder_path, write_file_name)
#         print()
#     print('Done.')


if __name__ == "__main__":
    pass

