
from Code.gnn_data_generator import GnnDataGenerator
from Code.utils import seconds2hours_minutes_seconds, parse_arguments, read_config_json
from Code.configs import SingleConfig, MultipleConfig

import time


if __name__ == "__main__":
    args = parse_arguments()
    config = read_config_json(args.config)

    # convert time minutes to seconds
    config.timer *= 60      # TODO uncomment this

    # Start time
    tic = time.perf_counter()

    generator = GnnDataGenerator()
    if config.mode == 'single':
        config = SingleConfig(**config.__dict__)
        generator.single(config)
    elif config.mode == 'multiple':
        config = MultipleConfig(**config.__dict__)
        generator.multiple(config)
    else:
        raise ValueError('Unknown type of mode, exiting...')

    # End time
    toc = time.perf_counter()
    print(f"Generated the data in {seconds2hours_minutes_seconds(toc-tic)}.")
