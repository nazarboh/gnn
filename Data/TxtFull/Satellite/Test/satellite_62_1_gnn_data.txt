BEGIN_OBJECTS
0 satellite0
1 instrument0
2 instrument1
3 instrument2
4 satellite1
5 instrument3
6 instrument4
7 instrument5
8 satellite2
9 instrument6
10 instrument7
11 satellite3
12 instrument8
13 satellite4
14 instrument9
15 instrument10
16 image1
17 thermograph0
18 spectrograph2
19 groundstation1
20 groundstation2
21 star0
22 phenomenon3
23 phenomenon4
24 star5
25 phenomenon6
26 planet7
27 planet8
28 star9
29 star10
30 planet11
31 phenomenon12
32 planet13
33 planet14
34 star15
35 phenomenon16
36 phenomenon17
37 phenomenon18
38 planet19
39 phenomenon20
40 star21
41 planet22
42 planet23
43 planet24
44 phenomenon25
45 planet26
46 planet27
47 phenomenon28
48 phenomenon29
49 star30
50 planet31
51 phenomenon32
52 star33
53 planet34
54 phenomenon35
55 star36
56 planet37
57 planet38
58 planet39
59 phenomenon40
60 star41
61 planet42
END_OBJECTS
BEGIN_PREDICATES
0 on_board
1 supports
2 pointing
3 power_avail
4 power_on
5 calibrated
6 have_image
7 calibration_target
8 satellite
9 direction
10 instrument
11 mode
END_PREDICATES
BEGIN_PREDICATE_ARITY_TUPLES
0 2
1 2
2 2
3 1
4 1
5 1
6 2
7 2
8 1
9 1
10 1
11 1
END_PREDICATE_PREDICATE_ARITY_TUPLES
BEGIN_FACT_LIST
0 1 0
0 2 0
0 3 0
0 5 4
0 6 4
0 7 4
0 9 8
0 10 8
0 12 11
0 14 13
0 15 13
1 1 18
1 2 16
1 2 17
1 2 18
1 3 17
1 3 18
1 5 16
1 6 16
1 6 17
1 6 18
1 7 17
1 7 18
1 9 16
1 10 16
1 10 17
1 10 18
1 12 16
1 12 17
1 12 18
1 14 16
1 14 17
1 14 18
1 15 16
1 15 17
1 15 18
7 1 19
7 2 21
7 3 19
7 5 19
7 6 20
7 7 19
7 9 20
7 10 20
7 12 19
7 14 20
7 15 21
8 0
8 4
8 8
8 11
8 13
9 19
9 20
9 21
9 22
9 23
9 24
9 25
9 26
9 27
9 28
9 29
9 30
9 31
9 32
9 33
9 34
9 35
9 36
9 37
9 38
9 39
9 40
9 41
9 42
9 43
9 44
9 45
9 46
9 47
9 48
9 49
9 50
9 51
9 52
9 53
9 54
9 55
9 56
9 57
9 58
9 59
9 60
9 61
10 1
10 2
10 3
10 5
10 6
10 7
10 9
10 10
10 12
10 14
10 15
11 16
11 17
11 18
END_FACT_LIST
BEGIN_GOAL_LIST
6 56 17
END_GOAL_LIST
BEGIN_STATE_LIST
BEGIN_LABELED_STATE
5.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
3 13
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
3 13
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 19
3 0
3 4
3 8
3 13
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 19
3 0
3 4
3 8
3 13
4 12
5 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 56
3 0
3 4
3 8
3 13
4 12
5 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 56
3 0
3 4
3 8
3 13
4 12
5 12
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 15
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 21
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 15
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 21
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 15
4 12
5 15
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 15
4 12
5 15
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 15
4 12
5 15
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 15
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 21
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 15
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 21
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 15
3 11
5 15
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 15
3 11
5 15
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 15
3 11
5 15
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 15
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 21
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 15
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 21
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 15
3 11
5 15
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 15
3 11
5 15
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 15
3 11
5 15
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 15
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 21
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 15
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 21
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 15
4 12
5 15
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 15
4 12
5 15
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 15
4 12
5 15
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
3 13
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 20
2 11 60
3 0
3 4
4 10
3 13
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 20
2 11 60
3 0
3 4
4 10
3 13
4 12
5 10
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 56
2 11 60
3 0
3 4
4 10
3 13
4 12
5 10
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 56
2 11 60
3 0
3 4
4 10
3 13
4 12
5 10
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
3 13
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 20
2 11 60
3 0
3 4
4 10
3 13
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 20
2 11 60
3 0
3 4
4 10
3 13
3 11
5 10
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 56
2 11 60
3 0
3 4
4 10
3 13
3 11
5 10
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 56
2 11 60
3 0
3 4
4 10
3 13
3 11
5 10
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 14
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 14
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 14
3 11
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 14
3 11
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 14
3 11
5 14
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 14
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 14
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 14
4 12
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 14
4 12
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 10
4 14
4 12
5 14
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 14
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 14
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 14
4 12
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 14
4 12
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 14
4 12
5 14
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 14
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 14
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 14
3 11
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 14
3 11
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
3 8
4 14
3 11
5 14
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
4 9
4 14
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
4 9
4 14
3 11
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
4 9
4 14
3 11
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 9
4 14
3 11
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 9
4 14
3 11
5 14
6 56 17
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
4.0
BEGIN_STATE
2 0 20
2 13 38
2 4 30
2 8 40
2 11 60
3 0
3 4
4 9
4 14
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
3.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
4 9
4 14
4 12
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
2.0
BEGIN_STATE
2 0 20
2 13 20
2 4 30
2 8 40
2 11 60
3 0
3 4
4 9
4 14
4 12
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
1.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 9
4 14
4 12
5 14
END_STATE
END_LABELED_STATE
BEGIN_LABELED_STATE
0.0
BEGIN_STATE
2 0 20
2 13 56
2 4 30
2 8 40
2 11 60
3 0
3 4
4 9
4 14
4 12
5 14
6 56 17
END_STATE
END_LABELED_STATE
END_STATE_LIST
